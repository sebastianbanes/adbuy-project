
<form method="post" action="index.php?page=users" id="filtru_users_form">
    <table cellspacing="0" cellpadding="2" border="0">
        <tr>
            <td>Email:</td>
            <td><input type="text" name="email" value="{$filtre.email}" /> </td>
        </tr>
		<br/>
        <tr>
            <td><br/>
                <a href="#" onclick="document.getElementById('filtru_users_form').submit()" class="button">Filter</a>
            </td>
        </tr>
    </table>
</form>

<br />
<table cellpadding="2" cellspacing="0" border="0" width="100%">
	<tr><td style="height:5px;"></td></tr>
	<tr>
		<td style="padding-right:20px;" align="right">
		<a href="index.php?page=users&amp;action=add" class="button">Add new user</a>
		</td>
	</tr>
</table>
<br />
{if ($users)}
<table cellpadding="2" cellspacing="0" border="0" width="90%" align="center">
	<tr><td colspan="4" style="height:5px;"></td></tr>
	<tr>
		<td><strong>#</strong></td>
		<td><strong>Social network</strong></td>
        <td><strong>Email</strong></td>
		<td colspan="7" align="center"><strong>Actions</strong></td>
	</tr>
	{foreach from=$users item=user name=nr}
	<tr class="object" onmouseover="this.className='object_hover'" onmouseout="this.className='object'">
		<td>{$smarty.foreach.nr.index+1}</td>
		<td>{$user->socialnetwork}</td>
		<td>{$user->email}</td>
		<td width="30"><a href="index.php?page=users&amp;action=edit&amp;id={$user->id}">Edit</a></td>
	</tr>
	{/foreach}
    <tr>
        <td colspan="4" align="center">{$paginare}</td>
    </tr>
</table>
{else}
<div align="center">No users yet</div>
{/if}