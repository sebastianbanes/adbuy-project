<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
  "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

		<title>::Administration Panel::</title>
		<meta name="LANGUAGE" content="EN">

		<meta name="COPYRIGHT" content="cupew.com">
		<meta name="AUTHOR" content="qubiz.com">

		<link href="/admin/css/index.css" type="text/css" rel="stylesheet">
	</head>
<body>
    <div class="main">
        {include file="header.tpl"}
        {include file="content.tpl"}
        {include file="footer.tpl"}
        <br class="clear"/>
    </div>
</body>
</html>
