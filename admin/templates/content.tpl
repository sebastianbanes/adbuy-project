<div style="width: 100%; display: block; float: left; background-color: #FFFFFF;">
    {if isset($ADMIN)}
    <div style="float:left; width: 20%; min-width: 200px;">
        {include file="left.tpl"}
    </div>
    {/if}
    <div class="content" style="width:{if isset($ADMIN)} 78%{else} 98%{/if};">
        {include file = $CONTENT}
    </div>
</div>