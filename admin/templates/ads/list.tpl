
<form method="post" action="index.php?page=ads" id="filtru_ads_form">
    <table cellspacing="0" cellpadding="2" border="0">
        <tr>
            <td>Title:</td>
            <td><input type="text" name="title" value="{$filtre.title}" /> </td>
            <td>Date from:</td>
            <td><input name="date_from" id="calendar1" value="{if isset($filtre.de_la)}{$filtre.de_la}{/if}" /></td>
            <td>Date to:</td>
            <td><input name="date_to" id="calendar2" value="{if isset($filtre.pana_la)}{$filtre.pana_la}{/if}" /></td>
            <td>Added by user:</td>
            <td>
                <select name="user_id">
                    <option value="">...</option>
                    {foreach from=$users item=user name=nr}
                        <option value="{$user->id}" {if ($filtre.user_id == $user->id)} selected="selected"{/if} >{$user->email}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
		<br/>
        <tr>
            <td colspan="8"><br/>
                <a href="#" onclick="document.getElementById('filtru_ads_form').submit()" class="button">Filter</a>
            </td>
        </tr>
    </table>
</form>

<br />

{if isset($ads)}
<table cellpadding="2" cellspacing="0" border="0" width="90%" align="center">
	<tr><td colspan="6" style="height:5px;"></td></tr>
	<tr>
		<td width="7"><strong>#</strong></td>
        <td><strong>Title</strong></td>
        <td><strong>User</strong></td>
        <td><strong>Budget</strong></td>
        <td><strong>Start Date</strong></td>
        <td><strong>End Date</strong></td>
	</tr>
	{foreach from=$ads item=ad name=nr}
	<tr class="object" onmouseover="this.className='object_hover'" onmouseout="this.className='object'">
		<td>{$smarty.foreach.nr.index+1}</td>
		<td>{$ad->title}</td>
        <td>{$ad->getUserEmail()}</td>
        <td>{$ad->budget}</td>
        <td>{$ad->start_date}</td>
        <td>{$ad->end_date}</td>
	</tr>
	{/foreach}
    <tr>
        <td colspan="6" align="center">{$paginare}</td>
    </tr>
</table>
{else}
<div align="center">No ads yet</div>
{/if}
