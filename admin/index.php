<?php

	#init:
	include "../init.php";

    $config->auto_include_directories[] = 'include/lib/tags';
	#smarty:
	//$smarty = new Smarty;
    $smarty = Application::getSmarty('/admin/templates/', '/admin/templates_c/');


	session_start();

	Admin::config($config->admin_auth);

	if(Admin::isLogged()) {
		$page = empty($_GET['page']) ? 'home' : $_GET['page'];
		$smarty->assign('ADMIN', Admin::getLoggedUser());
	} else {
		$page = 'login';
	}

	$controller = $config->absolute_path . '/admin/pages/' . $page . '.php';
	$template = $config->absolute_path . '/admin/templates/' . $page . '.tpl';
	$subpage = @empty($_GET['subpage']) ? '' : $_GET['subpage'];
	if ($subpage != '') {
		$controller = $config->absolute_path . '/admin/pages/' . $page . '/' . $subpage . '.php';
		$template = $config->absolute_path . '/admin/templates/' . $page . '/' . $subpage . '.tpl';
		$page .= '/' . $subpage;
	}

    require $config->absolute_path . '/include/lib/classes/Messages.php';
    $msg = new Messages();
    if ($msg->getMessages() !== false) {
        $smarty->assign('msg', $msg->getMessages());
        $msg->deleteMessages();
    }
	
	if(file_exists($controller)) {

		require_once $controller;

		$content_var = $smarty->getTemplateVars('CONTENT');
		if(empty($content_var)) {
			$content_var = $page . '.tpl';
		}

	} else if(file_exists($template)) {
		$content_var = $page . '.tpl';
	} else {
		$content_var = 'error_not_found.tpl';
	}

	$smarty->assign('CONTENT', $content_var);

	$index = $smarty->getTemplateVars('INDEX');
	if(empty($index)) {
		$index = 'index.tpl';
	}

	$smarty->assign('page', $page);
	$smarty->assign('config', $config);

	$smarty->display($index);