<?php

	if (Admin::isLogged())
	{
		jump('index.php?'.$config->url_page_var.'=home');
	}

	if ($config->isPost())
	{
		// vine din form

		if(Admin::login($_POST['email'], $_POST['password']))
		{
			jump('index.php?'.$config->url_page_var.'=home');
		} else {
			$smarty->assign('error_login', 'Login failed!');
		}
	}
