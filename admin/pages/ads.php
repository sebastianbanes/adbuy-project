<?php

	$action = isset($_GET['action']) ? $_GET['action'] : 'list';
	$id = isset($_GET['id']) ? $_GET['id'] : null;

	$ads = new Ads($id);
	$err = '';
	
	switch ($action){
		default:
            case 'list':


            if ($config->isPOST()){
                $_SESSION['filtru_ads'] = $_POST;
                jump('index.php?page=ads');
            }

            $append = "";

            if (!empty($_SESSION['filtru_ads']['de_la'])){
                $append .= "AND `payment_date` >= '" . addslashes($_SESSION['filtru_ads']['de_la']) . "' ";
            }
            if (!empty($_SESSION['filtru_ads']['pana_la'])){
                $append .= "AND `payment_date` <= '" . addslashes($_SESSION['filtru_ads']['pana_la']) . "' ";
            }

            if (!empty($_SESSION['filtru_ads']['user_id'])){
                $append .= "AND `user_id` = " . $_SESSION['filtru_ads']['user_id'] . ' ';
            }

            if (!empty($_SESSION['filtru_ads']['title'])){
                $append .= "AND `title` LIKE '%" . addslashes($_SESSION['filtru_ads']['title']) . "%' ";
            }


            $append = ltrim($append, 'AND');
            $append = !empty($append) ? ' WHERE ' . $append : '' ;

            $count = $ads->getCount($append);


            $paginator = new Paginator($count);
            $paginator->setRecordsPerPage(20);

            $ads = $ads->getAll($append . ' ORDER BY `id` DESC LIMIT ' . $paginator->getLimit());

            $users = new Users();
            $smarty->assign('users', $users->getAll(' ORDER BY `email` ASC '));



            $smarty->assign('ads', $ads);
            $smarty->assign('paginare', $paginator->getHtml());
            $smarty->assign('filtre', @$_SESSION['filtru_ads']);


            $action = 'list';

            break;

	}
	
	$smarty->assign('err', $err);
	$smarty->assign('action', $action);
