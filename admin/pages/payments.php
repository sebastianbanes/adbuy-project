<?php

	$action = isset($_GET['action']) ? $_GET['action'] : 'list';
	$id = isset($_GET['id']) ? $_GET['id'] : null;

	$payments = new Payments($id);
	$err = '';
	
	switch ($action){
		default:
            case 'list':


            if ($config->isPOST()){
                $_SESSION['filtru_payments'] = $_POST;
                jump('index.php?page=payments');
            }

            $append = '';

            if (!empty($_SESSION['filtru_payments']['de_la'])){
                $append .= "AND `payment_date` >= '" . addslashes($_SESSION['filtru_payments']['de_la']) . "' ";
            }
            if (!empty($_SESSION['filtru_payments']['pana_la'])){
                $append .= "AND `payment_date` <= '" . addslashes($_SESSION['filtru_payments']['pana_la']) . "' ";
            }

            if (!empty($_SESSION['filtru_payments']['user_id'])){
                $append .= "AND `user_id` = " . $_SESSION['filtru_payments']['user_id'] . ' ';
            }


            $append = ltrim($append, 'AND');
            $append = !empty($append) ? ' WHERE ' . $append : '' ;

            $count = $payments->getCount($append);


            $paginator = new Paginator($count);
            $paginator->setRecordsPerPage(20);

            $payments = $payments->getAll($append . ' ORDER BY id DESC LIMIT ' . $paginator->getLimit());

            $users = new Users();
            $smarty->assign('users', $users->getAll(' ORDER BY `email` ASC '));



            $smarty->assign('payments', $payments);
            $smarty->assign('paginare', $paginator->getHtml());
            $smarty->assign('filtre', @$_SESSION['filtru_payments']);


            $action = 'list';

            break;

			


	}
	
	$smarty->assign('err', $err);
	$smarty->assign('action', $action);
