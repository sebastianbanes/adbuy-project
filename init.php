<?php

	# decomenteaza linia urmatoare doar daca apar probleme de compatibilitate intre PHP-uri:
	//include "include/lib/php_compat.php";

	define('APPLICATION_PATH', dirname(__FILE__));

	#core Application class:
	include APPLICATION_PATH . "/include/lib/core/Application.php";
	spl_autoload_register(array('Application', 'loadClass'));

	#application instance:
	$app = Application::getInstance();

	#config:
	$config = $app->getConfig();

	#maintenance mode:
	if(!empty($config->maintenance_mode)) {
		$app->maintenanceMode();
	}

	#debug mode, include core, error handling and auto-include directories:
	$app->applyConfig($config);

	#init session handler
	SiteSessionHandler::init();

	// Facebook
	require_once APPLICATION_PATH . '/vendor/autoload.php';

	// Twitter

	require APPLICATION_PATH . '/vendor/abraham/twitteroauth/autoload.php';