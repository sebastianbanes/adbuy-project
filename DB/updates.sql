#NEW
ALTER TABLE `ads` ADD `geo_cities` TEXT NULL DEFAULT NULL AFTER `geo`;

#OK
ALTER TABLE `ads` CHANGE `geo` `geo` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `users` CHANGE `last_login` `last_login` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `payments` ADD `status` ENUM('pending','payed') NOT NULL DEFAULT 'pending' ;
ALTER TABLE `ads` DROP `description`;
ALTER TABLE `ads` CHANGE `gender` `gender` TINYINT NOT NULL DEFAULT '0';
ALTER TABLE `payments` ADD `hash` VARCHAR(64) NULL AFTER `id`;

ALTER TABLE `ads` ADD `page_id` VARCHAR(255) NOT NULL AFTER `user_id`, ADD `page_name` VARCHAR(255) NOT NULL AFTER `page_id`;
ALTER TABLE `ads` ADD `age_min` TINYINT NOT NULL AFTER `page_id`, ADD `age_max` TINYINT NOT NULL AFTER `age_min`;
ALTER TABLE `ads` DROP `page_name`;
ALTER TABLE `ads` DROP `demo`;
ALTER TABLE `ads` ADD `gender` TEXT NOT NULL AFTER `age_max`;
ALTER TABLE `ads` ADD `bid` INT NOT NULL AFTER `budget`;
-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 25, 2016 at 10:41 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `adbuy`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(30) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL DEFAULT 'ACTIVE',
  `date_created` datetime NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `username` (`username`),
  KEY `email` (`email`),
  KEY `status` (`status`),
  KEY `access` (`access`),
  KEY `password` (`password`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `email`, `password`, `status`, `date_created`, `last_login`, `access`) VALUES
(1, 'ovi', 'ovi', '07bc1df8c6cc6e78c6d0b970db74b5b4', 'ACTIVE', '0000-00-00 00:00:00', '2016-03-22 00:05:26', 0);

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE IF NOT EXISTS `admin_login` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` tinyint(4) DEFAULT NULL,
  `user_agent` text,
  `ip` varchar(12) DEFAULT NULL,
  `login_date` datetime DEFAULT NULL,
  `type` enum('success','failed') NOT NULL DEFAULT 'success',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;



ALTER TABLE `payments` ADD `user_id` INT NOT NULL AFTER `ad_id`;

ALTER TABLE `ads` ADD `title` VARCHAR(255) NOT NULL AFTER `end_date`;

ALTER TABLE `ads` CHANGE `socialnetwork` `socialnetwork` ENUM('facebook','twitter') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'facebook';

ALTER TABLE `payments` CHANGE `earnings` `earnings` DECIMAL(8,2) NOT NULL;