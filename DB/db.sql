-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 22, 2016 at 04:54 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `adbuy`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE IF NOT EXISTS `ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `budget` int(11) NOT NULL,
  `demo` varchar(255) NOT NULL,
  `geo` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `description` varchar(140) NOT NULL,
  `tracking_url` varchar(255) NOT NULL,
  `socialnetwork` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ad_id` int(11) NOT NULL,
  `earnings` int(11) NOT NULL,
  `payment_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `series_tokens`
--

CREATE TABLE IF NOT EXISTS `series_tokens` (
  `email` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `series_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `session_id` varchar(128) NOT NULL,
  `user_id` int(11) NOT NULL,
  `data` text NOT NULL,
  `expired` tinyint(4) NOT NULL,
  UNIQUE KEY `session_id` (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `user_id`, `data`, `expired`) VALUES
('8qttb7fc5g2m4pss17d9bn4m23', 1, 'fb_access_token|s:195:"CAAXZBIf0oWhcBAAobpWVgyy2SnqkYdhjVRB6lMq0XNJzViZCWGyWLsCx0wiHk8gEX3h6nZBv6WeRp3BZA59l1Reigl3RRhJHHi2KRhmpvZCL6Py4frCAWdblathJ2BOO44G07uTRLACBa6VwOUfBYVb0eopkNJ5yGcffJjzQFwN1nmnBocKu65PxH2TR0iBUZD";user_front|O:5:"Users":5:{s:2:"id";s:1:"1";s:5:"email";s:24:"ovidiu.maritan@qubiz.com";s:21:"socialnetwork_user_id";s:15:"149592365440757";s:10:"last_login";s:19:"2016-04-22 17:46:10";s:13:"socialnetwork";s:8:"facebook";}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `socialnetwork_user_id` varchar(32) NOT NULL,
  `last_login` datetime NOT NULL,
  `socialnetwork` enum('facebook','twitter') NOT NULL DEFAULT 'facebook',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `socialnetwork_user_id`, `last_login`, `socialnetwork`) VALUES
(1, 'ovidiu.maritan@qubiz.com', '149592365440757', '2016-04-22 17:53:32', 'facebook');
