<?php

    Ajax::requireLoggedInToPost();

    $user = Users::getLoggedUser();

//
//$locations = $_POST['location'];
//deg($locations);
//
//$dbLocations = json_encode($locations);
//deg($dbLocations);
//
//$reLocations = json_decode($dbLocations);
//foreach($reLocations as $location){
//    deg($location);
//}
//
//out('gata');

    $validTypes = array('jpg', 'gif');
    $type = file_extension($_FILES['form-ad-photo']['name']);

    if (!in_array($type, $validTypes)) {
        Ajax::outputError(Messages::IMAGE_WRONG_FORMAT);
    }

    global $config;

    if( $_FILES['form-ad-photo']['size'] / (1024*1024) > $config->image_file->max_file_size) {
        Ajax::outputError(Messages::IMAGE_FILE_SIZE_BIG);
    }

    $warningMessage = Ads::validateFields($_POST);

    if (empty($warningMessage)){

        $ad = Ads::createFromAjax($_POST, $user);
        $ad->saveImageFromAjax($_FILES);

        Ajax::output($ad->getPaymentHash());
    } else {
        Ajax::outputError($warningMessage);
    }
