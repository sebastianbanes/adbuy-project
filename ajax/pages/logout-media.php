<?php

    global $config;

    $socialToLogout = isset($_POST['socialMedia']) ? $_POST['socialMedia'] : false;

    $staticFunctionToCall = 'Users::' . $socialToLogout . 'Logout';

    call_user_func($staticFunctionToCall);

    Ajax::output('Ok');

