<?php

    Ajax::requireLoggedInToPost();

    $dates = explode(' - ', $_POST['timeSpan']);

    $cmps = Ads::getCPMs(getMySqlDateFromDatePickerFormat($dates[0]), getMySqlDateFromDatePickerFormat($dates[1]), $_POST['budget']);

    Ajax::output(json_encode($cmps));