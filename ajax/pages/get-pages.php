<?php 

if (!Users::isLogged()) {
   Ajax::outputError(Messages::USER_HAS_NO_PAGES);
}

$pages = FBAdPlacement::getPages();

if (isset($pages['data']) && count($pages['data']) > 0){
    Ajax::output(FBAdPlacement::createJsonFromPages($pages['data']));
} else {
    Ajax::outputError(Messages::USER_HAS_NO_PAGES);
}