<?php
//if (Users::isLogged()) {
//    Ajax::output(Link::AFTER_LOGIN_PAGE);
//}

global $config;
//deg(APPLICATION_PATH . '/include/lib/faceBook/autoload.php');exit;

require_once APPLICATION_PATH . '/vendor/autoload.php';

$loginForFacebook   = isset($_POST['doFaceBookLogin']) && ($_POST['doFaceBookLogin'] == 1);
$loginForInstagram  = isset($_POST['doInstagramLogin']) && ($_POST['doInstagramLogin'] == 1);

$user = new Users();

if(Users::isFacebookLogged() || Users::isInstagramLogged()){

	$user = Users::getLoggedUser();

	$user = $user->getUserFaceBookId($user->socialnetwork_user_id);

	$user->loginForFb($loginForFacebook, $loginForInstagram);


	Ajax::output('loggedIn');
}

$fb = new Facebook\Facebook([
	'app_id' => $config->facebook->appId,
	'app_secret' => $config->facebook->secret,
	'default_graph_version' => $config->facebook->graphVersion,
	]);


$helper = $fb->getRedirectLoginHelper();

$accessToken = new Facebook\Authentication\AccessToken($_POST['accessToken'], $_POST['expiresIn']);


// The OAuth 2.0 client handler helps us manage access tokens
$oAuth2Client = $fb->getOAuth2Client();

// Get the access token metadata from /debug_token
$tokenMetadata = $oAuth2Client->debugToken($accessToken);

// Validation (these will throw FacebookSDKException's when they fail)
$tokenMetadata->validateAppId($config->facebook->appId);
// If you know the user ID this access token belongs to, you can validate it here
// $tokenMetadata->validateUserId('123');
$tokenMetadata->validateExpiration();

if (! $accessToken->isLongLived()) {
    // Exchanges a short-lived access token for a long-lived one
	try {
		$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
	} catch (Facebook\Exceptions\FacebookSDKException $e) {
		Ajax::outputError("Error getting long-lived access token: " . $e->getMessage());
	}
}

$_SESSION['fb_access_token'] = (string) $accessToken;

try {
    // Returns a `Facebook\FacebookResponse` object
	$response = $fb->get('/me?fields=id,name,email,first_name,last_name,picture.type(large),accounts', $accessToken);
} catch(Facebook\Exceptions\FacebookResponseException $e) {
	Ajax::outputError('Graph returned an error: ' . $e->getMessage());
} catch(Facebook\Exceptions\FacebookSDKException $e) {
	Ajax::outputError('Facebook SDK returned an error: ' . $e->getMessage());
}

if (!isset($response)) {
	Ajax::outputError('Facebook login error');
}


$fbUser = $response->getGraphUser();
//deg($fbUser);
$user = $user->getUserFaceBookId($fbUser['id']);

if ($user->id > 0) {
	$user->loginForFb($loginForFacebook, $loginForInstagram);
} else {
	$user->createFaceBookUser($fbUser);
}

$pages = FBAdPlacement::getPages();


if (isset($pages['data']) && count($pages['data']) > 0){
	Ajax::output(FBAdPlacement::createJsonFromPages($pages['data']));
} else {
	Ajax::outputError(Messages::USER_HAS_NO_PAGES);
}

