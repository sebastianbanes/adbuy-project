<?php

global $config;

use Abraham\TwitterOAuth\TwitterOAuth;

$connection = new TwitterOAuth($config->twitter->consumer_key, $config->twitter->consumer_secret);

$request_token = $connection->oauth('oauth/request_token', array('oauth_callback' => $config->twitter->oauth_callback_url));

$_SESSION['oauth_token'] = $request_token['oauth_token'];
$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];

$url = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));

Ajax::output($url);

