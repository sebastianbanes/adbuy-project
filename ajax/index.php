<?php

	define('SKIP_SESSION_ALLOW_CHECK', true);
	require_once "../init.php";

	if (isset($_GET['SESS_ID']))
	{
		session_id($_GET['SESS_ID']);
	}

	SiteSessionHandler::init();
	
	session_start();

	#get the page::
	$page = isset($_GET['page']) ? safeFileName($_GET['page']) : null;
	
	if (empty($page) && isset($_POST['page'])){
		$page = safeFileName($_POST['page']);
	}
	
	$controller = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'pages' . DIRECTORY_SEPARATOR . $page . '.php';
	
	if(file_exists($controller)) {

		require_once $controller;
		Ajax::output('Success');
		
	} else {
		Ajax::outputError('Bad Request');
	}