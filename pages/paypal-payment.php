<?php

    $user = Users::getLoggedUser();

    $paypal_email = $config->mail->payment;

    if (!isset($_GET['hashId'])){
        jump('/');
    }

    $payments = new Payments();
    $payments = $payments->getByHash($_GET['hashId']);
//out($payments);
    // PayPal settings
    // TODO MOVE them to config file
    $return_url = $config->absolute_url . '/payment-successful?hash=' . $payments->hash;
    $cancel_url = $config->absolute_url . '/payment-cancelled';
    $notify_url = $config->absolute_url . '/paypal-payment';


    $ad = new Ads($payments->ad_id);

    $item_name = $ad->title;
    $item_amount = $payments->earnings;


    // Check if paypal request or response
    if (!isset($_POST["txn_id"]) && !isset($_POST["txn_type"])){
        $querystring = '';

        // Firstly Append paypal account to querystring
        $querystring .= "?business=".urlencode($paypal_email)."&";

        // Append amount& currency (£) to quersytring so it cannot be edited in html

        //The item name and amount can be brought in dynamically by querying the $_POST['item_number'] variable.
        $querystring .= "cmd=_xclick&";
        $querystring .= "no_note=1&";
        $querystring .= "bn=" . stripslashes('PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest') . "&";
        $querystring .= "currency_code=USD&";
        $querystring .= "item_name=".urlencode($item_name)."&";
        $querystring .= "amount=".urlencode($item_amount)."&";
        $querystring .= "payer_email=".urlencode($user->email)."&";
        $querystring .= "item_number=".urlencode($ad->tracking_url)."&";

        // Append paypal return addresses
        $querystring .= "return=".urlencode(stripslashes($return_url))."&";
        $querystring .= "cancel_return=".urlencode(stripslashes($cancel_url))."&";
        $querystring .= "notify_url=".urlencode($notify_url);

        // Append querystring with custom field
        //$querystring .= "&custom=".USERID;

        // Redirect to paypal IPN
        header('location:https://www.sandbox.paypal.com/cgi-bin/webscr'.$querystring);
        exit();
    } else {

        // Response from Paypal
        // read the post from PayPal system and add 'cmd'
        $req = 'cmd=_notify-validate';
        foreach ($_POST as $key => $value) {
            $value = urlencode(stripslashes($value));
            $value = preg_replace('/(.*[^%^0^D])(%0A)(.*)/i','${1}%0D%0A${3}',$value);// IPN fix
            $req .= "&$key=$value";
        }

        // assign posted variables to local variables
        $data['txn_id']				= $_POST['txn_id'];
        $data['receiver_email'] 	= $_POST['receiver_email'];
        $data['payer_email'] 		= $_POST['payer_email'];
        $data['custom'] 			= $_POST['custom'];

        // post back to PayPal system to validate
        $header = "POST /cgi-bin/webscr HTTP/1.0\r\n";
        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $header .= "Content-Length: " . strlen($req) . "\r\n\r\n";

        $fp = fsockopen ('ssl://www.sandbox.paypal.com', 443, $errno, $errstr, 30);

        if (!$fp) {
            // HTTP ERROR

        } else {
            fputs($fp, $header . $req);
            while (!feof($fp)) {
                $res = fgets ($fp, 1024);
                if (strcmp($res, "VERIFIED") == 0) {

                    // Used for debugging
                    // mail('user@domain.com', 'PAYPAL POST - VERIFIED RESPONSE', print_r($post, true));

                    // Validate payment (Check unique txnid & correct price)
                    //TODO check if payment received == payment ammount from ad with id received
                    $valid_txnid = true;
                    $valid_price = true;
                    // PAYMENT VALIDATED & VERIFIED!
                    if ($valid_txnid && $valid_price) {
                        //TODO update payment status
                    } else {
                        // Payment made but data has been changed
                        // E-mail admin or alert user
                    }

                } else if (strcmp ($res, "INVALID") == 0) {

                    // PAYMENT INVALID & INVESTIGATE MANUALY!
                    // E-mail admin or alert user

                    // Used for debugging
                    //@mail("user@domain.com", "PAYPAL DEBUGGING", "Invalid Response<br />data = <pre>".print_r($post, true)."</pre>");
                }
            }
            fclose ($fp);
        }
    }


