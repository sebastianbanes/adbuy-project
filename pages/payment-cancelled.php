<?php

    $paymentHash = isset($_GET['hash']) ? $_GET['hash'] : null;

    $message = '';

    if (!empty($paymentHash)){
        $payment = new Payments();
        $payment = $payment->getByHash($paymentHash);

        $payment->setStatusCancelled();

    }


    $smarty->assign('message', Messages::PAYMENT_CANCELLED);
