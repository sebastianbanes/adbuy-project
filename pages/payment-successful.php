<?php

    $paymentHash = isset($_GET['hash']) ? $_GET['hash'] : null;

    $message = '';

    if (!empty($paymentHash)){
        $payment = new Payments();
        $payment = $payment->getByHash($paymentHash);

        $payment->postPayPal = $_POST;

        if ($payment->id > 0 && $payment->paymentOk()){
            $ad = new Ads($payment->ad_id);
            $ad->postToFacebook();

            $payment->setStatusPayed();

            $message = Messages::AD_POSTED_SUCCESSFULLY . '<br/>' . Messages::PAYMENT_COMPLETE;
        } else {
            $message = Messages::PAYMENT_HASH_INVALID;
        }

    } else {
        $message = Messages::PAYPAL_RETURN_URL_WRONG;
    }


    $smarty->assign('message', $message);
