  <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                    <div class="logo">
                        <a href="/" title="Cupew"><img src="/assets/img/logo.png" alt="#"></a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>                            
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="#" data-toggle="modal" data-target="#myModal">Help</a></li>
                                    {*<li><a href="#" data-toggle="modal" data-target="#contactModal">Contact</a></li>*}
                                    <li><a href="http://www.123contactform.com/form-1998357/Contact-Lead-Form" target="_blank">Contact</a></li>
                                    {* {if !isset($loggedUser)}
                                    {else}
                                    <li><a href="/logout">Logout</a></li>
                                    {/if}
                                    <li><a href="/logout">Logout</a></li>*}
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container-fluid -->
                    </nav>
                </div>
            </div>
        </div>
    </div>