<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <title>{$config->page_meta->title}</title>
        <script type="text/javascript">
            var isFacebookLogged = {if !isset($facebookLogged)}false{else}true{/if};
            var isInstagramLogged = {if !isset($instagramLogged)}false{else}true{/if};
            var isTwitterLogged = {if !isset($twitterLogged)}false{else}true{/if};
        </script>
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-datetimepicker.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css?v={$config->version}" />
        <link rel="stylesheet" type="text/css" href="/assets/css/main.css?v={$config->version}" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    </head>
    <body>
        <div>
            {include file="header.tpl"}
            {include file="$CONTENT"}
            {include file="footer.tpl"}
        </div>
    </body>
<!--    <script type="text/javascript" src="/assets/js/dropzone.js"></script>-->
    <script type="text/javascript" src="/assets/js/dropzone.js"></script>
    <script type="text/javascript" src="/assets/js/moment.min.js"></script>
    <script type="text/javascript" src="/assets/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="/assets/js/bootstrap.js?v={$config->version}"></script>
    <script type="text/javascript" src="/assets/js/_global.js?v={$config->version}"></script>
    

</html>