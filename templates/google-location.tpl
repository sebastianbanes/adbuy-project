
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
{literal}
<script type="text/javascript">
    function initialize() {
        var options = {
            types: ['geocode']
        };
        var address = (document.getElementById('my-address'));
        var autocomplete = new google.maps.places.Autocomplete(address, options);
        autocomplete.setTypes(['geocode']);
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                return;
            }

            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }
        });
    }
    function codeAddress() {
        geocoder = new google.maps.Geocoder();
        var address = document.getElementById("my-address").value;
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                $type = results[0].address_components[0].types[0];
                console.log($type);
                if ($.inArray($type, ['country','locality','administrative_area_level_1']) !== -1){
                    alert("Shortname: " + results[0].address_components[0].short_name);
                } else {
                    alert("You can only select a country, a county, or a city");
                }

            }

            else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);

</script>
{/literal}
<input type="text" id="my-address">
<button id="getCords" onClick="codeAddress();">Get Short name</button>

