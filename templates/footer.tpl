<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Cupew consulting HELP</h4>
            </div>
            <div class="modal-body">
                <h3>Important:</h3>
                <strong>You need to have a valid PayPal account, in order to use our platform . All of payments are managed through Paypal.</strong>
                <h3>How to Login on website</h3>
                <p>Before posting an Ad on one of the social platforms, you need to connect with the platform you want to use. You can post an Ad on Facebook , Instagram or Twitter. </p>
                <p>After you complete the <strong>Ad title</strong> , <strong>Destination URL</strong> and <strong>Image attachment</strong> fields , you can connect with Facebook , Twitter or Instagram . Click on social network button.</p>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    How to create a Business Manager account ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                               <p>One of the prerequisites is a Business Manager account. You need one in order to post an Ad on Instagram. Business Manager is a Facebook service. <a href="https://business.facebook.com/" title="Create an account on Business Manager">Create an account</a> , if you don’t have one .</p>
                                <p>Then , you need to add a payment method :</p>
                                <strong>Step 1</strong> - Go to Payments tab on your Business Manager account.
                                <img src="assets/img/payments-tab.png" alt="">
                                <strong>Step 2</strong> - Click on “Add Paymenth Method”
                                <img src="assets/img/AddPaymenth.png" alt="">
                                <p>Next step is to create an Ad Account (you need to assign an Ad Account to post an Ad on Instagram).</p>
                                <p>Under the “People and Assets” TAB , click on <strong>Ad Account</strong> and then hit the <strong>Add New Ad Account</strong> button. Follow the instructions to create the new ad account.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    How to post an Ad on Facebook ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <p>Before posting an ad, make sure you have a valid Facebook account with a valid payment method attached . The payment for Facebook Ad will be managed by Facebook Ads platform.</p>
                                <i>You can add a payment method on Facebook like this:</i>
                                <p>Go to <strong>Settings</strong> -> <strong>Payments</strong> -> <strong>Account Settings</strong> -> <strong>Add Payment Method</strong></p>
                                <p>If you don’t have an account , you need to <a href="https://facebook.com" title="Creat an account">create one </a>.</p>
                                <p>Also , you need to create a Business Manager account (If you want to post an ad on Instagram). You can create a Business Manager account <a href="https://business.facebook.com/">here</a>. </p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    How to post an Ad on Instagram
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                <p>Instagram Ads are managed by Facebook Ads platform. To use Instagram Ads , you need to have a valid Facebook account with a valid payment method attached and a Business Manager account. </p>
                                <p>Also, it’s required to have a valid Instagram account with a <strong>profile picture</strong> .</p>
                                <p>If you don’t have an Instagram account , you can find how to create an account by following <a href="https://help.instagram.com/155940534568753">this steps</a> .</p>
                                <p>After you’ve created a Business Manager account and an Instagram account , you need to claim the Instagram account into your business with <a href="https://developers.facebook.com/docs/marketing-api/businessmanager">Business Manager</a> .</p>
                                <p>Follow this steps to do that:</p>
                                <strong>Step 1</strong> - Go to “People and Assets” tab 
                                <img src="assets/img/menu2.png" alt="">
                                <strong>Step 2</strong> - Find the Instagram button in the left side menu
                                <img src="assets/img/instagram_account.png" alt="">
                                <strong>Step 3</strong> - Click on “Clain New Instagram Account”
                                <img src="assets/img/createinstagram.png" alt="">
                                <strong>Step 4</strong> -Enter your username and password to login on Instagram account, then you need to choose an ad account that you’ve created before.

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Thank you</button>
            </div>
        </div>
    </div>
</div>

<!-- Contact Modal -->
<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Cupew consulting Contact</h4>
            </div>
            <div class="modal-body">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    __FB_APP_ID = '{$config->facebook->appId}';

    {literal}
    // Load the SDK asynchronously
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    {/literal}

</script>

<script type="text/javascript" src="/assets/js/ads.js"></script>
<script type="text/javascript" src="/assets/js/login.js"></script>
<div class="footerContainer">
    <div class="footer">
        &copy;<a href="#">Cupew LLC</a>, All rights reserved, Patent Pending adbuy.com
    </div>
</div>