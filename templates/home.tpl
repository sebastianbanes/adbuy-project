<div class="container-fluid fix-padding">
    <div class="container">
        <div class="row">
            <form id="facebookCreateAd" class="form-vertical websiteForm" action="#" enctype="multipart/form-data">
                <input type="hidden" name="page" value="save-ad">
                <div class="page-content">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 fix-padding">
                        <div class="left-side">
                            <h3>Ad Content</h3>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                        <label for="adContentText">Ad text:</label>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                        <input maxlength="140" type="text" class="form-control" name="adContentText" id="adContentText" required  placeholder="Ad text">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                        <label for="adContentTrackingUrl">Destination URL:</label>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                        <input type="url" class="form-control" name="adContentTrackingUrl" id="adContentTrackingUrl" required placeholder="Destination URL">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <label>Image attachment:</label>
                                </div>
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                    <div id="adImage" class="adImage">
                                        <input type="file" name="form-ad-photo" id="form-ad-photo" class="" required accept="image/gif, image/jpeg">
                                        <p>Drag & drop your image here or <label for="form-ad-photo">Upload</label></p>
                                        <div class="preview_image">
                                            <img id="image_upload_preview">
                                        </div>
                                    </div>
                                    <small>Max upload size is 10MB. Suported file types: jpeg &amp; png</small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <label class="loginIcons">Publish ad on:</label>
                                </div>
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                    <div class="social_media_connect">
                                        <div class="icon facebook-connect" id="btn-facebook"></div>
                                        <div class="icon twitter-connect" id="btn-twitter"></div>
                                        <div class="icon instagram-connect" id="btn-instagram"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 fix-padding">
                        <div class="right-side">
                            <h3>Buy Details</h3>
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <label for="userPageList">
                                        Page:
                                    </label>
                                </div>
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                    <select class="form-control" name="pageId" id="userPageList" required>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                        <label for="buyDetailsCampaignBudget">
                                            Campaign Budget:
                                        </label>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                <input type="number" min="100" class="form-control medium_control" name="buyDetailsCampaignBudget" id="buyDetailsCampaignBudget" required placeholder="Campaign Budget">
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                <span class="input_symbol">&#x24;&nbsp;US Dollar</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <label>Gender:</label>
                                </div>
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gendersOptions" id="AllGenders" checked value="0">
                                            All
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gendersOptions" id="Male" value="1">
                                            Male
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gendersOptions" id="Female" value="2">
                                            Female
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <label for="ageRange">Age:</label>
                                </div>
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                    <div class="form-group" id="ageRange">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <label for="geographySelect">
                                        Geography:
                                    </label>
                                </div>
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                    <div class="row">
                                        <div class="col-xs-10 col-sm-8 col-md-8 col-lg-8">
                                            <input type="text" id="my-address" class="form-control medium_control">
                                        </div>
                                        <div class="col-xs-2 col-sm-4 col-md-4 col-lg-4">
                                            <button id="getCords" onClick="codeAddress(); return false;" class="geolocation_button btn btn-place-order pull-right">Add</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                                    <ul id="geoList" class="geolocation">
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <label for="buyDetailsDateRange">Timeframe:</label>
                                </div>
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <input type="text" class="form-control dateselect" name="start_date" id="start_date" required placeholder="Start date">
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <input type="text" class="form-control dateselect" name="end_date" id="end_date" required placeholder="End date">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <label>Our fee:</label>
                                </div>
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                    <label id="buyDetailsOurFee">$0</label>
                                    <small>Payment by Paypal</small>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-place-order pull-right" id="submitButton">Pay and post ad</button>
                        </div>
                    </div>
                </div>
                </div>
            </form>
    </div>
</div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script type="text/javascript" src="/assets/js/googleGeoLocation.js"></script>
<script type="text/javascript" src="/assets/js/app.js"></script>