<?php

	class Users extends BaseObject
    {

        const
			USER_TYPE_FACEBOOK = 'facebook',
			USER_TYPE_TWITTER = 'twitter';


        public function getTableName()
        {
            return 'users';
        }

        public function getFields()
        {
            return array('id', 'email', 'socialnetwork_user_id', 'last_login', 'socialnetwork');
        }


		public function getUserFaceBookId($faceBookId = 0)
        {
            $u = new Users();
            $u->readField('socialnetwork_user_id', $faceBookId);
            return $u;
        }


		/**
		 * Executa operatia de login pentru un username($email) si o parola($password).
		 * Daca operatia reuseste, userul citit este salvat in sesiune si se returneaza true.
		 * Altfel returneaza false.
		 *
		 * In functie de setari, operatia de login poate actualiza field-ul last_login.
		 *
		 * Se poate apela static.
		 *
		 * @param string $email
		 * @param string $password
		 * @param string $username
		 * @param boolean $remember
		 * @return boolean
		 */
        public static function login($email, $password = false, $remember = false)
		{

			$email = addslashes($email);

			$andPassword = $password === false ? '' : " AND PASSWORD = '" . md5($password) . "'";

            global $config;

			//logare pe baza de email
			$sql = "
				SELECT *
				FROM `{$config->user_auth->table}`
				WHERE `{$config->user_auth->username_field}` = '$email'{$andPassword} AND
				 	`status` = '" . Users::USER_TYPE_ACTIVE . "'";
			$db = new DbMySql($sql);

			if (!$db->nextRecord()) {
				return false;
			}

			// exista user, password
			$user = new User;
			$user->id = $db->f('id');
			$user->email = $db->f('email');
			$user->password = $db->f('password');
			$user->fname = $db->f('fname');
			$user->lname = $db->f('lname');
			if($config->user_auth->save_last_login) {
				$user->last_login = $db->f('last_login');
			}

			if ($remember) {
				self::remember($email);
			}

			if($config->user_auth->save_last_login) {
				$sql = "
					UPDATE `{$config->user_auth->table}`
					SET `{$config->user_auth->last_login_field}` = now()
					WHERE id = " . $user->id;
				$db->query($sql);
			}

			Users::setLoggedUser($user);

            return Users::isLogged();
		}

		private static function remember($email) {
			SeriesTokens::add($email)->store();
		}

		/**
		 * @param $userData \Facebook\GraphNodes\GraphUser
		 */
        public function createFaceBookUser($userData, $loginForFacebook = true, $loginForInstagram = true) {
            $this->email = $userData['email'];
            $this->type = Users::USER_TYPE_FACEBOOK;
            $this->socialnetwork_user_id = $userData['id'];

            $this->save();
            $this->loginForFb($loginForFacebook, $loginForInstagram);

        }


        public function loginForFb($loginForFacebook = true, $loginForInstagram = true)
        {
            Users::setLoggedUser($this);

//            deg($loginForInstagram, $loginForFacebook);

            if($loginForFacebook){
                Users::setFacebookLoggedUser($this);
            }

            if($loginForInstagram){
                Users::setInstagramLoggedUser($this);
            }

            global $config;

            $db = new DbMySql();

            if($config->user_auth->save_last_login) {
                $sql = "UPDATE `{$config->user_auth->table}` SET `{$config->user_auth->last_login_field}` = now() WHERE id = " . $this->id;
                $db->query($sql);
            }

            return Users::isLogged();
        }

		public function isFacebookUser() {
			return $this->type === self::USER_TYPE_FACEBOOK;
		}

		public function isTwitterInUser() {
			return $this->type === self::USER_TYPE_TWITTER;
		}

		/**
		 * Returneaza true daca exista un user logat curent.
		 *
		 * @return boolean
		 */
        public static function isLogged()
		{
            global $config;
			return !empty($_SESSION[$config->user_auth->session_var]);
		}

        public static function isFacebookLogged()
        {
            global $config;
            return !empty($_SESSION[$config->user_auth->facebook_session_var]);
        }

        public static function isInstagramLogged()
        {
            global $config;
            return !empty($_SESSION[$config->user_auth->instagram_session_var]);
        }


        public static function isTwitterLogged()
        {
            global $config;
            return !empty($_SESSION[$config->user_auth->twitter_session_var]);
        }


        /**
		 * Returneaza userul logat, daca exista unul.
		 *
		 * @return User
		 */
		public static function getLoggedUser()
		{
			//$config = Users::config();
            global $config;

			if(empty($_SESSION[$config->user_auth->session_var])) {
				return null;
			}

			$ref = & $_SESSION[$config->user_auth->session_var];
			return $ref;
		}

        public static function getFacebookLoggedUser()
        {
            global $config;

            if(empty($_SESSION[$config->user_auth->facebook_session_var])) {
                return null;
            }

            $ref = & $_SESSION[$config->user_auth->facebook_session_var];
            return $ref;
        }

        public static function getInstagramLoggedUser()
        {
            global $config;

            if(empty($_SESSION[$config->user_auth->instagram_session_var])) {
                return null;
            }

            $ref = & $_SESSION[$config->user_auth->instagram_session_var];
            return $ref;
        }

        public static function getTwitterLoggedUser()
        {
            global $config;

            if(empty($_SESSION[$config->user_auth->twitter_session_var])) {
                return null;
            }

            $ref = & $_SESSION[$config->user_auth->twitter_session_var];
            return $ref;
        }

		/**
		 * Folosit pentru a seta un user ca fiind cel logat curent.
		 * Este apelata automat in functia login().
		 *
		 * Poate fi apelata pentru a re-actualiza modificarile facute unui user, si
		 * a se reflecta si in sesiune.
		 *
		 * @param User $user
		 */
		function setLoggedUser($user)
		{
            global $config;
			SiteSessionHandler::setUserId($user->id);
			SiteSessionHandler::expireUserSessions($user->id, session_id());
			$_SESSION[$config->user_auth->session_var] = $user;
		}


        function setFacebookLoggedUser($user){
            global $config;
            SiteSessionHandler::setUserId($user->id);
            SiteSessionHandler::expireUserSessions($user->id, session_id());
            $_SESSION[$config->user_auth->facebook_session_var] = $user;
//            deg($user);
        }


        function setInstagramLoggedUser($user){
            global $config;
            SiteSessionHandler::setUserId($user->id);
            SiteSessionHandler::expireUserSessions($user->id, session_id());
            $_SESSION[$config->user_auth->instagram_session_var] = $user;
        }

        function setTwitterLoggedUser($user){
            global $config;
            SiteSessionHandler::setUserId($user->id);
            SiteSessionHandler::expireUserSessions($user->id, session_id());
            $_SESSION[$config->user_auth->twitter_session_var] = $user;
        }


		/**
		 * Executa operatia de logout pentru useru curent logat,
		 * daca exista unul.
		 *
		 */
        public static function logout($expireAll = true)
		{
            global $config;
			SeriesTokens::clear();
			SiteSessionHandler::expireUserSessions(Users::getLoggedUser()->id, null, !$expireAll);
			unset($_SESSION[$config->user_auth->session_var]);
            Users::facebookLogout();
            Users::instagramLogout();
            Users::twitterLogout();
		}


        public static function facebookLogout(){
            global $config;
            unset($_SESSION[$config->user_auth->facebook_session_var]);
        }

        public static function instagramLogout(){
            global $config;
            unset($_SESSION[$config->user_auth->instagram_session_var]);
        }

        public static function twitterLogout(){
            global $config;
            unset($_SESSION[$config->user_auth->twitter_session_var]);
        }

        public static function autoLogin() {
            if (!self::isLogged()) {
                $email = SeriesTokens::fromCookie();
                if ($email) {
                    self::login($email);
                }
            }
        }



        public static function create($data, & $errorMessage = '') {

			$email = isset($data['email']) ? $data['email'] : null;
			$password = isset($data['password']) ? $data['password'] : null;
			$repassword = isset($data['password-confirm']) ? $data['password-confirm'] : null;
			$username = isset($data['username']) ? $data['username'] : null;
			$firstname = isset($data['firstname']) ? $data['firstname'] : '';
			$lastname = isset($data['lastname']) ? $data['lastname'] : '';

            //invalid email
            if (!valid_email($email)) {
                $errorMessage .= Messages::INVALID_EMAIL;
            }

			if (!self::validUsername($username)) {
				$errorMessage .= Messages::INVALID_USERNAME;
			}

			//invalid password retype
            if ($password != $repassword) {
                $errorMessage .= Messages::INVALID_PASSWORD_RETYPE . '<br/>';
            }

            //invalid password
            if (strlen($password) < self::MIN_PASSWORD_CHARS) {
                $errorMessage .= Messages::INVALID_PASSWORD . '<br/>';
            }

            //invalid recaptcha
            /*if (!Recaptcha::check($captcha)) {
                $errorMessage = Messages::INVALID_RECAPTCHA;
                return false;
            }*/

            //email already exists
            $u = new User();
            $u->readField('email', $email);
            if ($u->id) {
                $errorMessage .= Messages::EMAIL_EXISTS;
            }

            if ($errorMessage != '') {
                return false;
            }

            //all ok, creeaza-l
            $u->email = $email;
            $u->password = md5($password);
			$u->username = $username;
			$u->fname = $firstname;
			$u->lname = $lastname;
            $u->save();

            //$u->createConfirmation()->send($u);

            return $u;
        }

		public static function validUsername($username) {
			return !empty($username) && strlen($username) < 32;
		}
/*
        public function createConfirmation() {
            $uc = new UserConfirmation();
            $uc->setRecord(array(
                'user_id' => $this->id,
                'key' => UserConfirmation::generateKey($this)
            ));
            $uc->insert();

            return $uc;
        }

        public function confirm() {
            $this->confirmed = self::CONFIRMED;
            $this->update();
        }
*/


		public static function forgotPassword($email) {

			$u = new self();
			$u->readField('email', $email);

			if (empty($u->id)) {
				return false;
			}

			return $u->createPasswordReset();
		}

		public function createPasswordReset() {

			if (!$this->isEmailUser()) {
				return false;
			}

			$this->sendPasswordResetEmail(PasswordReset::reset($this->id)->hash);

			return true;

		}

		public function resetPassword($data) {

			$password = isset($data['new-password']) ? $data['new-password'] : null;
			$repassword = isset($data['new-password-confirm']) ? $data['new-password-confirm'] : null;

			//invalid password retype
			if ($password != $repassword) {
				return Messages::INVALID_PASSWORD_RETYPE;
			}

			//invalid password
			if (strlen($password) < self::MIN_PASSWORD_CHARS) {
				return Messages::INVALID_PASSWORD;
			}

			$this->password = md5($password);
			$this->save();

			return true;

		}

		public function sendPasswordResetEmail($passwordResetHash) {

			$config = Application::getConfig();
			$smarty = Application::getSmarty('templates', 'templates_c');

			$smarty->assign('passwordResetLink', 'http://' . $_SERVER['HTTP_HOST'] . '/resetare-parola.html?prh=' . $passwordResetHash);

			$mail = getMailObject($smarty->fetch('mail/password-reset.tpl'));
			$mail->Subject = $config->mail->passwordReset->subject;
			$mail->AddAddress($this->email);
			$mail->Send();

		}

		public function isActive() {
			return $this->status === self::USER_TYPE_ACTIVE;
		}

		public function getCompany() {
			$c = new Companies();
			$all = array_values($c->getAll(sprintf("WHERE `added_by_user_id` = %u LIMIT 1", $this->id)));
			if (count($all)) {
				return $all[0];
			}
			return null;
		}

	}
