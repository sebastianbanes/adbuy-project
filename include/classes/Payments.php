<?php

	class Payments extends BaseObject
	{

        const STATUS_PENDING = 'pending',
            STATUS_PAYED = 'payed',
            STATUS_CANCELLED = 'cancelled';

        public $postPayPal;

        public function getTableName()
		{
			return 'payments';
		}

		public function getFields()
		{
			return array('id', 'hash', 'ad_id', 'user_id', 'earnings', 'payment_date', 'status');
		}

        public static function getByHash($hash)
        {
            $q = new self;
            $q->readField('hash', $hash);
            return $q;
        }

		public static function getForUser($userId) {

			$res = array();

			$q = sprintf("
				SELECT p.*
				FROM `payments` p
				LEFT JOIN `users` u ON u.`id` = p.`user_id`
			  	WHERE p.`user_id` = %u
				", $userId);

			$db = new DbMySql($q);

			while ($db->nextRecord()) {
				$r = new self;
				$r->setRecord($db->getRecord());
				$res[] = $r;
			}

			return $res;
		}

		public function getDateAdded($format = 'd-m-Y') {
			return date($format, strtotime($this->payment_date));
		}

        public function getAddTitle()
        {
            $add = new Ads($this->ad_id);
            return $add->title;
        }

        public function getUserEmail()
        {
            $user = new Users($this->user_id);
            return $user->email;
        }


        public function paymentOk(){
            $paymentStatus = true;
            if (isset($this->postPayPal['payment_gross']) && ($this->postPayPal['payment_gross'] != $this->earnings)){
                $paymentStatus = false;
            }

            return $paymentStatus;
        }


        public function setStatusPayed(){
            $this->status = Payments::STATUS_PAYED;
            $this->save();
        }

        public function setStatusCancelled(){
            $this->status = Payments::STATUS_C;
            $this->save();

        }

    }
