<?php

    class Link {

        const HOME_PAGE = '/',
            AFTER_REGISTER_PAGE = self::HOME_PAGE,
            AFTER_LOGIN_PAGE = self::HOME_PAGE,
            AFTER_LOGOUT_PAGE = self::HOME_PAGE,
            LOGIN_PAGE = '/login.html';

        public static function getSubpageName($link)
        {
            $page = explode('/', trim($link, '/'));
            if (isset($page[1])){
                return rtrim($page[1], '/');
            }
            trigger_error('Invalid page link', E_USER_WARNING);
            return false;
        }

        public static function getLoginPage($next = '')
        {
            $ret = self::LOGIN_PAGE;
            if (!empty($next)){
                $ret .= '?next=' . urlencode($next);
            }
            return $ret;
        }

        public static function goToLoginPage($next = '')
        {
            jump(self::getLoginPage($next));
            exit;
        }

    }