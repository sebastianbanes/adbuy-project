<?php

	class Ads extends BaseObject
	{

        public function getTableName()
		{
			return 'ads';
		}

		public function getFields()
		{
			return array('id', 'user_id', 'page_id', 'age_min', 'age_max', 'budget', 'bid', 'geo', 'geo_cities', 'start_date', 'end_date', 'title', 'tracking_url', 'gender', 'socialnetwork');
		}


		public static function getForUser($userId) {

			$res = array();

			$q = sprintf("
				SELECT a.*
				FROM `ads` a
				LEFT JOIN `users` u ON u.`id` = a.`user_id`
			  	WHERE a.`user_id` = %u
				", $userId);

			$db = new DbMySql($q);

			while ($db->nextRecord()) {
				$r = new self;
				$r->setRecord($db->getRecord());
				$res[] = $r;
			}

			return $res;
		}

        public function getUserEmail()
        {
            $user = new Users($this->user_id);
            return $user->email;
        }

        public function getPaymentHash(){
            $payment = new Payments();
            $payment->readField('ad_id', $this->id);

            return $payment->hash;
        }


        public static function createFromAjax($postData, $user)
        {
//            $dates = explode(' - ', $postData['buyDetailsDateRange']);

            $ad = new self;

                $ad->user_id = $user->id;
                $ad->budget = $_POST['buyDetailsCampaignBudget'];
                $ad->page_id = $_POST['pageId'];
                $ad->age_min = $_POST['ageMin'];
                $ad->age_max = $_POST['ageMax'];
                $ad->gender = $_POST['gendersOptions'];

                if (isset($_POST['locationCity']) &&  !empty($_POST['locationCity'])){
                    $cityNames = $_POST['locationCity'];
                    $cityFacebookIds = array();
                    foreach($cityNames as $cityName){
                        $cityName = iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE', $cityName);
                        if ($cityFbId = FBAdPlacement::getCityFacebookId($cityName))
                        array_push($cityFacebookIds, $cityFbId);
                    }

                    $ad->geo_cities = json_encode($cityFacebookIds);
                }

                $ad->geo = isset($_POST['location']) ? json_encode($_POST['location']) : null;
                $ad->tracking_url = $_POST['adContentTrackingUrl'];
//                $ad->start_date = getMySqlDateFromDatePickerFormat($dates[0]);
//                $ad->end_date = getMySqlDateFromDatePickerFormat($dates[1]);
                $ad->start_date = getMySqlDateFromDatePickerFormat($_POST['start_date']);
                $ad->end_date = getMySqlDateFromDatePickerFormat($_POST['end_date']);

                $ad->title = $_POST['adContentText'];
                $ad->social_network = Users::USER_TYPE_FACEBOOK;

            $ad->save();
            $ad->savePayment();

            return $ad;
        }

        private function savePayment(){

            $payment = new Payments();

            $payment->ad_id         = $this->id;
            $payment->user_id       = $this->user_id;
            $payment->earnings      = $this->budget * 0.5 / 100;
            $payment->payment_date  = date("Y-m-d H:i:s");
            $payment->status        = Payments::STATUS_PENDING;

            $payment->save();

            $payment->hash          = md5($payment->id);
            $payment->save();

        }

        public function saveImageFromAjax($file)
        {
            global $config;

            $adImageName = $this->id . '.' . file_extension($file['form-ad-photo']['name']);
            $target = $config->absolute_path . '/files/ad_images/' . $adImageName;

            return move_uploaded_file($file['form-ad-photo']['tmp_name'], $target);
        }

        public function postToFacebook(){
            $pages = FBAdPlacement::getPages();

            $fbAds = new FBAdPlacement();

                $fbAds->campaignName    = $this->title;
                $fbAds->adSetName       = $this->title;
                $fbAds->bidAmount       = $this->bid * 100; #cents
                $fbAds->lifeTimeBudget  = $this->budget * 100; #cents
                $fbAds->startTime       = (new \DateTime($this->start_date))->format(DateTime::ISO8601);
                $fbAds->endTime         = (new \DateTime($this->end_date))->format(DateTime::ISO8601);
                $fbAds->imageId         = $this->id;
                $fbAds->link            = $this->tracking_url;
                $fbAds->linkMessage     = $this->title;
                $fbAds->linkCaption     = $this->title;
                $fbAds->geo             = $this->geo;
                $fbAds->geoCities       = $this->geo_cities;
                $fbAds->pageId          = $this->page_id;
                $fbAds->adName          = $this->title;
                $fbAds->maxAge          = $this->age_max;
                $fbAds->minAge          = $this->age_min;
                $fbAds->gendersOptions  = $this->gender;

                global $config;

                if (!empty($_SESSION[$config->user_auth->instagram_session_var])){
                    $fbAds->postOnInstagram = true;
                }
            $fbAds->placeAd();
        }

        public static function getCPMs($startDate, $endDate, $budget){
            $bigCpm = Ads::getDailyBudget($startDate, $endDate, $budget);

            $cpms = array();

            $cpms[$bigCpm]          = $bigCpm . '$';
            $cpms[floor($bigCpm/2)] = floor($bigCpm/2) . '$';
            $cpms[floor($bigCpm/4)] = floor($bigCpm/4) . '$';

            return $cpms;
        }

        public static function getDailyBudget($startDate, $endDate, $budget){
            $daysDiff = floor((abs(strtotime($endDate) - strtotime($startDate))/(60*60*24))) + 1;
            return floor($budget / $daysDiff);
        }

        public function validateFields($fields){

            global $config;

            $message = '';
            if(empty($fields['adContentText'])){
                $message .= Messages::AD_TITLE_EMPTY . "\n";
            }


            if(!empty($fields['adContentText']) && strlen($fields['adContentText']) > 140){
                $message .= Messages::AD_TITLE_LENGHT_TO_HIGH . "\n";
            }


            if (empty($fields['adContentTrackingUrl'])){
                $message .= Messages::AD_URL_EMPTY . "\n";
            } else if (!valid_url($fields['adContentTrackingUrl'])){
                $message .= Messages::AD_URL_NOT_VALID . "\n";
            }

            if (empty($fields['pageId'])){
                $message .= Messages::AD_PAGE_ID_NOT_SET . "\n";
            } else if (!Ads::pageIsInPagesArray($fields['pageId'])){
                $message .= Messages::AD_PAGE_ID_INVALID . "\n";
            }

            if (empty($fields['buyDetailsCampaignBudget'])){
                $message .= Messages::AD_BUDGET_EMPTY . "\n";
            }

            if (!is_numeric($fields['buyDetailsCampaignBudget'])){
                $message .= Messages::AD_BUDGET_SHOULD_BE_NUMERIC . "\n";
            } else if (!empty($fields['buyDetailsCampaignBudget']) && intval($fields['buyDetailsCampaignBudget']) < $config->ad_Settings->min_budget_value){
                $message .= Messages::AD_BUDGET_SHOULD_BE_GREATER_THAN . " " . $config->ad_Settings->min_budget_value . "\n";
            }

            if (empty($fields['ageMin'])){
                $message .= Messages::AD_AGE_MIN_EMPTY . "\n";
            }

            if (empty($fields['ageMin'])){
                $message .= Messages::AD_AGE_MAX_EMPTY . "\n";
            }

            if (!empty($fields['ageMin']) && !empty($fields['ageMax']) && ($fields['ageMin'] > $fields['ageMax'])){
                $message .= Messages::AD_AGE_MIN_HIGHER_THAN_MAX . "\n";
            }

            if (empty($fields['location']) && empty($fields['locationCity'])){
                $message .= Messages::AD_GEO_EMPTY . "\n";
            }


            if (empty($fields['buyDetailsCampaignBudget'])){
                $message .= Messages::AD_TOTAL_BUDGET_EMPTY . "\n";
            }



            if (empty($fields['start_date']) || empty($fields['start_date'])){
                $message .= Messages::AD_DATERANGE_EMPTY . "\n";
            } else{
//                $dates = explode(' - ', $fields['buyDetailsDateRange']);
//
//                $dateEnd    = new DateTime($dates[1]);
//                $dateNow    = new DateTime("now");

                $dateStart  = new DateTime($fields['start_date']);
                $dateEnd    = new DateTime($fields['end_date']);
                $dateNow    = new DateTime("now");


                if(strtotime($fields['end_date']) < strtotime($fields['start_date'])){
                    $message .= Messages::AD_DATERANGE_WRONG . "\n";
                } else if($dateNow > $dateEnd){
                    $message .= Messages::END_DATE_IS_PAST . "\n";
                }
            }

            return $message;
        }

        public static function pageIsInPagesArray($pageId){
            $pages = FBAdPlacement::getPages();


            foreach ($pages['data'] as $page){
                if($page['id'] == $pageId) return true;
            }

            return false;
        }

    }
