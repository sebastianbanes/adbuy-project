<?php

#facebook api
use FacebookAds\Api;
use FacebookAds\Object\AdUser;

#ad
use FacebookAds\Object\Ad;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdFields;

#campain
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Values\AdObjectives;
use FacebookAds\Object\Values\InsightsLevels;

#adSet
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Values\BillingEvents;
use FacebookAds\Object\Values\OptimizationGoals;
use FacebookAds\Object\Fields\TargetingSpecsFields;
use FacebookAds\Object\TargetingSpecs;
use FacebookAds\Object\Values\CallToActionTypes;
use FacebookAds\Object\TargetingSearch;
use FacebookAds\Object\Search\TargetingSearchTypes;

#adCreative
use FacebookAds\Object\AdImage;
use FacebookAds\Object\Fields\AdImageFields;
use FacebookAds\Object\AdCreative;
use FacebookAds\Object\ObjectStory\LinkData;
use FacebookAds\Object\Fields\ObjectStory\LinkDataFields;
use FacebookAds\Object\ObjectStorySpec;
use FacebookAds\Object\Fields\ObjectStorySpecFields;
use FacebookAds\Object\Fields\AdCreativeFields;

use Facebook\FacebookRequest;

use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;


class FBAdPlacement {

    /**
     * @var FaceBook token|null
     */
    protected $FBToken;

    /**
     * @var accountId|null
     */
    private $adAccountId;

    /**
     * @var int|null
     */
    private $campaignId;

    /**
     * @var int|null
     */
    private $adSetId;

    /**
     * @var int|null
     */
    private $adCreativeId;

    /**
     * @var string|null
     */
    private $imageHash;

    /**
     * @var object|null
     */
    private $linkData;

    /**
     * @var object|null
     */
    private $objectStory;

    /**
     * @var object|null
     */
    private $ad;

    /**
     * @var mixed
     */
    public $startTime, $endTime, $geo, $geoCities, $demo, $campaignName,
        $adSetName, $bidAmount, $lifeTimeBudget, $imageId,
        $link, $linkMessage, $linkCaption, $gender, $pageId, $adName, $maxAge, $minAge, $postOnTarget, $postOnInstagram;

    /**
     * @var string|null
     */
    protected $errorMessage;


    /**
     * @param FaceBookToken
     */
    public function __construct() {
        $this->FBToken = $_SESSION['fb_access_token'];
    }

    /**
     * @return ResponseInterface|null
     */
    public function placeAd() {
        self::initApi();

        $this->setAccountId();
        $this->setCampaignId();
        $this->setAdSetId();
        $this->setImageHash();
        $this->setLinkData();
        $this->setObjectStorySpec();
        $this->setAdCreativeId();

        $this->setAd();
    }

    /**
     * initialize facebook api
     */
    private static function initApi() {

        global $config;

        Api::init(
            $config->facebook->appId,
            $config->facebook->secret,
            $_SESSION['fb_access_token']
        );
    }


    private function setAccountId(){
        try {
            $me = new AdUser('me');
            $my_adaccount = $me->getAdAccounts()->current();
            $this->adAccountId = $my_adaccount->getData()['id'];
        }
        catch (Exception $e)
        {
            $_SESSION['api_error'] = $e->getMessage();
            jump('/error.html');
        }
    }

    private function setCampaignId(){
        try {
            $campaign = new Campaign(null, $this->adAccountId);
            $campaign->setData(array(
                CampaignFields::NAME => $this->campaignName,
                CampaignFields::OBJECTIVE => AdObjectives::LINK_CLICKS,
            ));

            $campaign->create(array(
                Campaign::STATUS_PARAM_NAME => Campaign::STATUS_PAUSED,#Campaign::STATUS_ACTIVE,
            ));
            $this->campaignId = $campaign->id;
        }
        catch (Exception $e)
        {
            $_SESSION['api_error'] = $e->getMessage();
            jump('/error.html');
        }

    }

    private function setAdSetId(){

        try{
            $adset = new AdSet(null, $this->adAccountId);

            $countryLocations = array();
            $adLocations = json_decode($this->geo);
            if (!empty($adLocations)){
                foreach($adLocations as $adLocation){
                    array_push($countryLocations, $adLocation);
                }
            }

            $adCityLocations = array();
            $adCityLocationsToParse = json_decode($this->geoCities);
            if (!empty($adCityLocationsToParse)) {
                foreach($adCityLocationsToParse as $city){
                    $cityArray = array('key' => strval($city), 'distance_unit' => 'mile');
                    array_push($adCityLocations, $cityArray);
                }
            }

            if ($this->postOnInstagram){

                $adset->setData(array(
                    AdSetFields::NAME => $this->adSetName,
                    AdSetFields::OPTIMIZATION_GOAL => OptimizationGoals::IMPRESSIONS,
                    AdSetFields::IS_AUTOBID => true,
                    AdSetFields::BILLING_EVENT => BillingEvents::IMPRESSIONS,
                    AdSetFields::LIFETIME_BUDGET => $this->lifeTimeBudget,
                    AdSetFields::CAMPAIGN_ID => $this->campaignId,
                    AdSetFields::START_TIME => $this->startTime,
                    AdSetFields::END_TIME => $this->endTime,
                    AdSetFields::TARGETING => (new TargetingSpecs())->setData(array(
                            TargetingSpecsFields::GEO_LOCATIONS => array(
                                'countries' => $adLocations,
                                'cities'    => $adCityLocations,
                            ),
                            TargetingSpecsFields::GENDERS => array($this->gender,),
                            TargetingSpecsFields::AGE_MIN => $this->minAge,
                            TargetingSpecsFields::AGE_MAX => $this->maxAge,
                            TargetingSpecsFields::PAGE_TYPES => array('instagramstream', 'desktopfeed', 'rightcolumn', 'mobilefeed', 'home'),
                        )),
                ));
            } else {
                $adset->setData(array(
                    AdSetFields::NAME => $this->adSetName,
                    AdSetFields::OPTIMIZATION_GOAL => OptimizationGoals::IMPRESSIONS,
                    AdSetFields::IS_AUTOBID => true,
                    AdSetFields::BILLING_EVENT => BillingEvents::IMPRESSIONS,
                    AdSetFields::LIFETIME_BUDGET => $this->lifeTimeBudget,
                    AdSetFields::CAMPAIGN_ID => $this->campaignId,
                    AdSetFields::START_TIME => $this->startTime,
                    AdSetFields::END_TIME => $this->endTime,
                    AdSetFields::TARGETING => (new TargetingSpecs())->setData(array(
                            TargetingSpecsFields::GEO_LOCATIONS => array(
                                'countries' => $adLocations,
                                'cities'    => $adCityLocations,
                            ),
                            TargetingSpecsFields::GENDERS => array($this->gender,),
                            TargetingSpecsFields::AGE_MIN => $this->minAge,
                            TargetingSpecsFields::AGE_MAX => $this->maxAge,
                        )),
                ));
            }

            $adSetCreated = $adset->create(array(
                AdSet::STATUS_PARAM_NAME => AdSet::STATUS_PAUSED,
            ));

            $this->adSetId = $adSetCreated->id;
            }
        catch (Exception $e)
        {
            $_SESSION['api_error'] = $e->getMessage();
            jump('/error.html');
        }


}

    private function setImageHash(){

        try{
            global $config;
            $image = new AdImage(null, $this->adAccountId);
            $image->{AdImageFields::FILENAME} =  $config->absolute_path . '/files/ad_images/' . $this->imageId . '.jpg';
            $image->create();

            $this->imageHash = $image->{AdImageFields::HASH}.PHP_EOL;

        }
        catch (Exception $e)
        {
            $_SESSION['api_error'] = $e->getMessage();
            jump('/error.html');
        }
    }


    private function setLinkData(){
        try{
            $link_data = new LinkData();
            $link_data->setData(array(
                LinkDataFields::MESSAGE => $this->linkMessage,
                LinkDataFields::LINK => $this->link,
                LinkDataFields::CAPTION => $this->linkCaption,
                LinkDataFields::IMAGE_HASH => $this->imageHash,
                LinkDataFields::CALL_TO_ACTION => array(
                    'type' => CallToActionTypes::OPEN_LINK,
                    'value' => array(
                        'link' => $this->link,
                        'link_caption' => $this->linkCaption,
                    ),
                ),
            ));
            $this->linkData = $link_data;
        }
        catch (Exception $e)
        {
            $_SESSION['api_error'] = $e->getMessage();
            jump('/error.html');
        }
    }


    private function setObjectStorySpec(){
        try{
            $object_story_spec = new ObjectStorySpec();

            $object_story_spec->setData(array(
                ObjectStorySpecFields::PAGE_ID => $this->pageId,
                ObjectStorySpecFields::LINK_DATA => $this->linkData,
            ));

            $this->objectStory = $object_story_spec;
        }
        catch (Exception $e)
        {
            $_SESSION['api_error'] = $e->getMessage();
            jump('/error.html');
        }
    }


    private function setAdCreativeId(){
        try{
            $creative = new AdCreative(null, $this->adAccountId);

            global $config;

            if ($this->postOnInstagram){

                $creative->setData(array(
                    AdCreativeFields::NAME => 'Sample Creative',
                    AdCreativeFields::OBJECT_STORY_SPEC => $this->objectStory,
                    AdCreativeFields::INSTAGRAM_ACTOR_ID => $config->twitter->instagram_actor_id,
                ));
            } else {
                $creative->setData(array(
                    AdCreativeFields::NAME => 'Sample Creative',
                    AdCreativeFields::OBJECT_STORY_SPEC => $this->objectStory,
                ));
            }

            $creativeFinal = $creative->create();

            $this->adCreativeId = $creativeFinal->id;
        }
        catch (Exception $e)
        {
            $_SESSION['api_error'] = $e->getMessage();
            jump('/error.html');
        }
    }


    private function setAd(){
        try{
            $data = array(
                AdFields::NAME => $this->adName,
                AdFields::ADSET_ID => $this->adSetId,
                AdFields::CREATIVE => array(
                    'creative_id' => $this->adCreativeId,
                ),
            );

            $ad = new Ad(null, $this->adAccountId);
            $ad->setData($data);

            $ad->create(array(
                Ad::STATUS_PARAM_NAME => Ad::STATUS_PAUSED,
            ));

            $this->ad = $ad;
        }
        catch (Exception $e)
        {
            $_SESSION['api_error'] = $e->getMessage();
            jump('/error.html');
        }
    }


    public static function getPages()
    {
        if (!isset($_SESSION['fb_access_token'])){

            return false;
        }

        global $config;

        $fb = new Facebook\Facebook([
            'app_id'     => $config->facebook->appId,
            'app_secret' => $config->facebook->secret
        ]);

        $response = $fb->get(
            '/me?fields=accounts,name,email',
            $_SESSION['fb_access_token']
        );

        $graphObject = $response->getDecodedBody();

        return isset($graphObject['accounts']) ? $graphObject['accounts'] : '';
    }


    public static function getCityFacebookId($address = null){

        if (empty($address)){
            return false;
        }

        if (!isset($_SESSION['fb_access_token'])){
            return false;
       }

        global $config;


        Api::init(  $config->facebook->appId,
                    $config->facebook->secret,
                    $_SESSION['fb_access_token']);

        $type = TargetingSearchTypes::GEOLOCATION;
        $query = array($address);


        $api = Api::instance();

        $params['type'] = $type;
        $params = array_merge($params, array_filter(array(
            'class' => null,
            'q' => $query
        )));

        $response = $api->call('/search', 'GET', $params);

        $body = $response->getContent();

        if (isset($body['data'][0]['key'])){
            return $body['data'][0]['key'];
        } else {
            return false;
        }
    }


    public static function createJsonFromPages($pages = array())
    {
        $pagesToReturn = array();
        $index = 0;
        foreach($pages as $page){
            $pagesToReturn[$index]['id']    = $page['id'];
            $pagesToReturn[$index]['name']  = $page['name'];
            $index++;
        }

        return $pagesToReturn;
    }

    /**
     * @return string|null
     */
    public function getError() {
        return $this->$errorMessage;
    }

}
