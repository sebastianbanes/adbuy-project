<?php

	$LIB_DIR = dirname(__FILE__) . '/';

	#include functions:
	include $LIB_DIR . 'functions.php';

	
	#require core:
	require $LIB_DIR . 'core/UrlMapper.php';
	require $LIB_DIR . 'core/UrlParser.php';
	require $LIB_DIR . 'core/UrlRewriter.php';
