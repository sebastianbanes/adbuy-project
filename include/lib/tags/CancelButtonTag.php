<?php

	SmartyTag::get('ButtonTag');

	class CancelButtonTag extends ButtonTag
	{
		const DEFAULT_CSS_CLASS = 'button';
		const DEFAULT_VALUE = ' Cancel ';

		public $url;

		public function setParameters($params)
		{
			parent::setParameters($params);

			#value:
			if(empty($this->value)) {
				$this->value = self::DEFAULT_VALUE;
			}

			#url:
			$this->url = $this->_escapeJsString($this->url);

			#onclick event:
			$this->onclick = " location.href = '{$this->url}' ";
		}

		// public function getHtml() # nu mai trebe suprascrisa functia!!!
	}
