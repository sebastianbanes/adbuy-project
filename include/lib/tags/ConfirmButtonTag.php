<?php

	SmartyTag::get('ButtonTag');

	class ConfirmButtonTag extends ButtonTag
	{
		const DEFAULT_CSS_CLASS = 'button';

		public $message;
		public $url;

		public function setParameters($params)
		{
			parent::setParameters($params);

			#new params:
			$this->message = $this->_escapeJsString($this->message);
			$this->url = $this->_escapeJsString($this->url);

			$this->onclick = " this.blur(); if(confirm('{$this->message}')) location.href='{$this->url}' ";
		}

		// public function getHtml() # nu mai trebe suprascrisa functia!!!
	}
