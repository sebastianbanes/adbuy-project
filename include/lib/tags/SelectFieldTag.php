<?php

	SmartyTag::get('FormFieldTag');

	class SelectFieldTag extends FormFieldTag
	{
		const DEFAULT_CSS_CLASS = 'select-field';
		const DEFAULT_VALUE_PROPERTY = 'id';
		const DEFAULT_LABEL_PROPERTY = 'name';
		const BASE_OBJECT_PARENT_CLASS = 'BaseObject';

		public $options;
		public $key;
		public $field;

		public $novalue;
		public $selected;

		public function setParameters($params)
		{
			parent::setParameters($params);

			#key & field (for BaseObject arrays):
			$this->key = empty($this->key) ? self::DEFAULT_VALUE_PROPERTY : $this->key;
			$this->field = empty($this->field) ? self::DEFAULT_LABEL_PROPERTY : $this->field;

			#options (play safe):
			$this->options = (array)$this->options;

			#unselected option:
			if($this->novalue) {
				$this->novalue = htmlspecialchars($this->novalue);
			}

			#selected option:
			if(empty($this->selected)) {
				$this->selected = $this->_getTemplateVar($this->name);
			}
			$this->selected = (string)$this->selected; // nu va permite selectarea de optiuni multiple... 
			// practic e alt tip de control, care trebuie creeat separat; nerecomandabil sa fie folosit, in general...
		}

		public function getHtml()
		{
			$class = $this->_getComputedClass(self::DEFAULT_CSS_CLASS);

			$selected_attribute_value = $this->xhtml ? ' selected="selected"' : ' selected';

			$html = "\n<li class=\"{$class}\">\n";

			$html .= "<label for=\"{$this->id}\"" . 
			($this->accesskey ? ' accesskey="' . $this->accesskey . '"' : '') . 
			'>' . $this->label . "</label>\n";

			$html .= '<select name="' . $this->name . '" id="' . $this->id . '"' . $this->_getExtraParametersString() . '>';

			if($this->novalue) {
				$html .= '<option value="">' . $this->novalue . '</option>';
			}

			if($this->_useBaseObjects()) {
				$key = $this->key;
				$field = $this->field;
				foreach($this->options as $option) {
					$option_value = isset($option->$key) ? htmlspecialchars($option->$key) : '';
					$option_label = isset($option->$field) ? htmlspecialchars($option->$field) : '&nbsp;';
					$option_selected = ($option_value == $this->selected) ? $selected_attribute_value : '';

					$html .= '<option value="' . $option_value . '"' . $option_selected . '>' . $option_label . "</option>\n";
				}
			} else {
				foreach($this->options as $option_value => $option_label) {
					$option_selected = ($option_value == $this->selected) ? $selected_attribute_value : '';

					$html .= '<option value="' . htmlspecialchars($option_value) . '"' . $option_selected . '>' . 
								htmlspecialchars($option_label) . "</option>\n";
				}
			}

			$html .= "</select>\n";

			$html .= $this->_getErrorHtml() . "\n" . $this->_getHintHtml() . "\n";

			$html .= "</li>";

			return $html;
			#return $this->_showTemplateVars();
		}

		private function _useBaseObjects()
		{
			$first_elem = reset($this->options);

			if(is_object($first_elem) && is_a($first_elem, self::BASE_OBJECT_PARENT_CLASS)) {
				return true;
			}

			return false;
		}
	}
