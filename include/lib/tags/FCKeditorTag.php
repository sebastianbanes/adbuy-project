<?php

	SmartyTag::get('TextareaTag');

	class FCKeditorTag extends TextareaTag
	{
		// DEFAULT_CSS_CLASS = 'text-area', din TextareaTag !!!

		const DEFAULT_SET = 'usual';
		const DEFAULT_WIDTH = '450';
		const DEFAULT_HEIGHT = '250';

		const BASE_SCRIPT_URL = '/script/FCKeditor/';

		const DEFAULT_INPUT_ID_POSTFIX = '___Input';
		const DEFAULT_CONFIG_ID_POSTFIX = '___Config';
		const DEFAULT_IFRAME_ID_POSTFIX = '___Frame';

		public static $validSets = array('basic', 'full', 'usual');

		public $set;
		public $width;
		public $height;
		public $style; // doar pentru a fi exclus din getExtraParameters

		public $hiddeninputid;
		public $configid;
		public $iframeid;

		// TODO: languages, in the future

		public function setParameters($params)
		{
			parent::setParameters($params);

			#set:
			if(!$this->set || !in_array(strtolower($this->set), self::$validSets)) {
				$this->set = self::DEFAULT_SET;
			}
			$this->set = strtolower($this->set);

			#value (avem nevoie de valoarea originala, pt. ca in textarea este 'escapata'):
			#$this->value = isset($params['value']) ? $params['value'] : '';

			#width:
			if(!$this->width) {
				$this->width = self::DEFAULT_WIDTH;
			}

			#height:
			if(!$this->height) {
				$this->height = self::DEFAULT_HEIGHT;
			}

			#config hidden field id:
			if(!$this->configid) {
				$this->configid = $this->id . self::DEFAULT_CONFIG_ID_POSTFIX;
			}

			#iframe id:
			if(!$this->iframeid) {
				$this->iframeid = $this->id . self::DEFAULT_IFRAME_ID_POSTFIX;
			}

			#iframe id:
			if(!$this->hiddeninputid) {
				$this->hiddeninputid = $this->id . self::DEFAULT_INPUT_ID_POSTFIX;
			}
		}

		public function getHtml()
		{
			$class = $this->_getComputedClass(self::DEFAULT_CSS_CLASS);

			$html = "\n<li class=\"{$class}\">\n";

			$html .= "<label for=\"{$this->id}\"" . 
			($this->accesskey ? ' accesskey="' . $this->accesskey . '"' : '') . 
			'>' . $this->label . "</label>\n";

			$widthCSS = ( strpos( $this->width, '%' ) === false ) ? $this->width . 'px' : $this->width;
			$heightCSS = ( strpos( $this->height, '%' ) === false ) ? $this->height . 'px' : $this->height;
			$default_html = '<textarea name="' . $this->name . '" id="' . $this->id . '" cols="' . $this->cols . '" rows="' . $this->rows .  '"'. 
						" style=\"width:{$widthCSS};height:{$heightCSS}\"" . 
						$this->_getExtraParametersString() . '>' . $this->value . "</textarea>\n"; 

			#add script:
			if($this->_isCompatibileBrowser()) {
				$basePath = Application::getMappedPath(self::BASE_SCRIPT_URL);

				$link = "{$basePath}editor/fckeditor.html?InstanceName={$this->name}&amp;Toolbar=" . $this->set;

				$short_tag_ending = $this->_getShortTagCloser();
				$js  = "<input type=\"hidden\" id=\"{$this->hiddeninputid}\" name=\"{$this->name}\" value=\"{$this->value}\"" .
							$short_tag_ending;
				$js .= "<input type=\"hidden\" id=\"{$this->configid}\" value=\"CustomConfigurationsPath=" . 
							$basePath . "fckcustomconfig.js\"" . $short_tag_ending ;
				$js .= "<iframe id=\"{$this->iframeid}\" src=\"{$link}\" frameborder=\"0\" scrolling=\"no\"" . 
							" width=\"{$this->width}\" height=\"{$this->height}\" >{$default_html}</iframe>" ;

				$html .= "<noscript>{$default_html}</noscript>";
				$html .= $this->_getJsBlockBegin();

				$html .= 'document.write(\'' . str_replace(
													array("\r", "\n", '</'), 
													array(' ', ' ', "<' + '/"), 
													$js) . '\')';

				$html .= $this->_getJsBlockEnd();
			} else {
				// not compatibile browsers:
				$html .= $default_html;
			}

			$html .= $this->_getErrorHtml() . "\n" . $this->_getHintHtml() . "\n";

			$html .= "</li>";

			return $html;
			#return $this->_showTemplateVars();
		}

		private function _isCompatibileBrowser()
		{
			//test compatibility:
			$userAgent = $_SERVER['HTTP_USER_AGENT'];
			$is_gecko = (strpos($userAgent, 'Gecko/') !== false);
			$is_opera = (strpos($userAgent, 'Opera/') !== false);
			$is_safari = preg_match( "|AppleWebKit/(\d+)|i", $userAgent, $safari_version_matches);
			$is_IE = (strpos($userAgent, 'MSIE') !== false && strpos($userAgent, 'mac') === false && !$is_opera);

			if($is_IE) {
				$version = (float)substr($userAgent, strpos($userAgent, 'MSIE') + 5, 3);
				return ($version >= 5.5);
			}

			if($is_gecko) {
				$version = (int)substr($userAgent, strpos($userAgent, 'Gecko/') + 6, 8);
				return ($version >= 20030210);
			}

			if($is_opera) {
				$version = (float)substr($userAgent, strpos($userAgent, 'Opera/') + 6, 4);
				return ($version >= 9.5);
			}

			if($is_safari) {
				$version = $safari_version_matches[1];
				return ($version >= 522);
			}

			return false;
		}
	}
