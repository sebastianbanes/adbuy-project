<?php

	SmartyTag::get('FormFieldTag');

	class CheckboxFieldTag extends FormFieldTag
	{
		const DEFAULT_CSS_CLASS = 'checkbox-field'; // with label & hint
		const DEFAULT_CSS_CLASS_NO_HINT = 'checkbox-field-no-hint';
		const DEFAULT_CSS_CLASS_NO_LABEL = 'checkbox-field-no-label';
		const DEFAULT_VALUE = '1';

		public $checked;

		public function setParameters($params)
		{
			parent::setParameters($params);

			if(!$this->value) {
				$this->value = self::DEFAULT_VALUE; // are valoare tot timpu!!!
			}

			if(empty($this->checked)) {
				$this->checked = (bool)$this->_getTemplateVar($this->name);
			} else {
				$this->checked = ($this->checked != 'false');
			}
		}

		public function getHtml()
		{
			$nohint = !$this->hint;
			$nolabel = (!$this->label || $this->label == '&nbsp;');

			if($nohint && $nolabel) {
				$this->_showError("No label or hint specified for this checkbox.");
				return '';
			}

			#din punctu asta, nolabel inseamna ca nohint=false, dar nu si invers:
			if($nohint) {
				$class = self::DEFAULT_CSS_CLASS_NO_HINT;
			} else if($nolabel) {
				$class = self::DEFAULT_CSS_CLASS_NO_LABEL;
			} else {
				$class = self::DEFAULT_CSS_CLASS; // are si label si hint...
			}
			$class = $this->_getComputedClass($class);

			#input-ul este la fel tot timpul:
			$checked = $this->checked ? 
							($this->xhtml ? ' checked="checked"' : ' checked') : 
							'';
			$input_html = '<input type="checkbox" name="' . $this->name . '" id="' . $this->id . '" value="' . $this->value . '"' . 
							$checked . 
							($this->accesskey ? ' accesskey="' . $this->accesskey . '"' : '') . 
							$this->_getExtraParametersString() . 
							$this->_getShortTagCloser() . "\n";

			#return value:
			$html = "\n<li class=\"{$class}\">\n";

			if($nohint) { // inseamna ca are doar label
				$html .= "<label for=\"{$this->id}\">" . $this->label . "</label>\n";
				$html .= $input_html . "\n";
			} else if($nolabel) { // inseamna ca are doar hint
				#show hint in label:
				$html .= "<label for=\"{$this->id}\">" . $input_html . "\n<span>{$this->hint}</span></label>\n";
			} else { // are label si hint:
				$html .= "<label for=\"{$this->id}\">" . $this->label . "</label>\n";
				$html .= $input_html . "\n";
				$html .= $this->_getHintHtml() . "\n";
			}

			$html .= $this->_getErrorHtml() . "\n"; // erorile...

			$html .= "</li>";

			return $html;
			#return $this->_showTemplateVars();
		}
	}
