<?php

	SmartyTag::get('FormFieldTag');

	class RadioFieldsTag extends FormFieldTag
	{
		const DEFAULT_CSS_CLASS = 'radio-fields';
		const DEFAULT_VALUE_PROPERTY = 'id';
		const DEFAULT_LABEL_PROPERTY = 'name';
		const BASE_OBJECT_PARENT_CLASS = 'BaseObject';
		const DEFAULT_COLUMN_NUMBER = 1;

		public $legend;

		public $options;
		public $checked;
		public $key;
		public $field;

		public $novalue;

		public function setParameters($params)
		{
			parent::setParameters($params);

			#legend:
			if($this->legend) {
				$this->legend = htmlspecialchars($this->legend);
				#$this->label = null; //astea 2 sunt exclusive ???
			}

			#key & field (for BaseObject arrays):
			$this->key = empty($this->key) ? self::DEFAULT_VALUE_PROPERTY : $this->key;
			$this->field = empty($this->field) ? self::DEFAULT_LABEL_PROPERTY : $this->field;

			#options (play safe):
			$this->options = (array)$this->options;

			#checked options:
			if(empty($this->checked)) {
				$this->checked = $this->_getTemplateVar($this->name);
			}
			$this->checked = (string)$this->checked; // o singura optiune selectata

			#'no-value' option:
			$this->novalue = htmlspecialchars((string)$this->novalue);
		}

		public function getHtml()
		{
			$class = $this->_getComputedClass(self::DEFAULT_CSS_CLASS);

			$html = "\n<li class=\"{$class}\">\n";

			#if($this->label) { // optional...
			#	$html .= "<label>" . $this->label . "</label>\n";
			#}

			if(!empty($this->options)) {
				$html .= "<div><fieldset>\n";

				if($this->legend) {
					$html .= '<legend><span>' . $this->legend . "</span></legend>\n";
				}

				$html .= "<ul>\n";

				$end_tag = $this->_getShortTagCloser() . "\n";
				$checked_attribute = $xhtml ? ' checked="checked"' : ' checked';
				$extra_params = $this->_getExtraParametersString();

				if($this->novalue) {
					$html .= "<li><label for=\"{$this->name}_\">\n";
					$html .= "<input type=\"radio\" name=\"{$this->name}\" id=\"{$this->name}_\" value=\"\"" . 
								($this->checked ? '' : $checked_attribute) . $extra_params . $end_tag;
					$html .= "<span>{$this->novalue}</span>\n";
					$html .= "</label></li>\n";
				}

				if($this->_useBaseObjects()) {
					$key = $this->key;
					$field = $this->field;

					foreach($this->options as $option) {
						$option_value = isset($option->$key) ? htmlspecialchars($option->$key) : '';
						$option_label = isset($option->$field) ? htmlspecialchars($option->$field) : '&nbsp;';
						$option_checked = ($option_value == $this->checked) ? $checked_attribute : '';
						$option_id = $this->id . '_' . $this->_clearId($option_value);

						$html .= "<li><label for=\"{$option_id}\">\n";
						$html .= "<input type=\"radio\" name=\"{$this->name}\" id=\"{$option_id}\" value=\"{$option_value}\"" . 
									$option_checked . $extra_params . $end_tag;
						$html .= "<span>{$option_label}</span>\n";
						$html .= "</label></li>\n";
					}
				} else {
					foreach($this->options as $option_value => $option_label) {
						$option_checked = ($option_value == $this->checked) ? $checked_attribute : '';
						$option_id = $this->id . '_' . $this->_cleanId($option_value);

						$html .= "<li><label for=\"{$option_id}\">\n";
						$html .= "<input type=\"radio\" name=\"{$this->name}\" id=\"{$option_id}\" value=\"{$option_value}\"" . 
									$option_checked . $extra_params . $end_tag;
						$html .= "<span>{$option_label}</span>\n";
						$html .= "</label></li>\n";
					}
				}

				$html .= "</ul>\n";

				$html .= $this->_getErrorHtml() . "\n";
				$html .= $this->_getHintHtml() . "\n";

				$html .= "</fieldset>\n</div>\n";
			}

			$html .= "</li>";

			return $html;
			#return $this->_showTemplateVars();
		}

		private function _useBaseObjects()
		{
			$first_elem = reset($this->options);

			if(is_object($first_elem) && is_a($first_elem, self::BASE_OBJECT_PARENT_CLASS)) {
				return true;
			}

			return false;
		}
	}
