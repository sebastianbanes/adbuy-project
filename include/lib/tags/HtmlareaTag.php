<?php

	SmartyTag::get('TextareaTag');

	class HtmlareaTag extends TextareaTag
	{
		// DEFAULT_CSS_CLASS = 'text-area', din TextareaTag !!!

		const DEFAULT_SET = 'simple';
		const DEFAULT_WIDTH = '440';
		const DEFAULT_HEIGHT = '170';

		const SCRIPT_URL = '/script/tinymce/tiny_mce.js';

		public static $validSets = array('simple', 'advanced');

		public $set;
		public $width;
		public $height;
		public $style; // doar pentru a fi exclus din getExtraParameters

		// TODO: languages, in the future

		public function setParameters($params)
		{
			parent::setParameters($params);

			#set:
			if(!$this->set || !in_array(strtolower($this->set), self::$validSets)) {
				$this->set = self::DEFAULT_SET;
			}
			$this->set = strtolower($this->set);

			#value (avem nevoie de valoarea originala, pt. ca in textarea este 'escapata'):
			#$this->value = isset($params['value']) ? $params['value'] : '';

			#width:
			if(!$this->width) {
				$this->width = self::DEFAULT_WIDTH;
			}

			#height:
			if(!$this->height) {
				$this->height = self::DEFAULT_HEIGHT;
			}
		}

		public function getHtml()
		{
			$widthCSS = ( strpos( $this->width, '%' ) === false ) ? $this->width . 'px' : $this->width;
			$heightCSS = ( strpos( $this->height, '%' ) === false ) ? $this->height . 'px' : $this->height;

			$class = $this->_getComputedClass(self::DEFAULT_CSS_CLASS);

			$html = "\n<li class=\"{$class}\">\n";

			$html .= "<label for=\"{$this->id}\"" . 
			($this->accesskey ? ' accesskey="' . $this->accesskey . '"' : '') . 
			'>' . $this->label . "</label>\n";

			$html .= '<textarea name="' . $this->name . '" id="' . $this->id . '" cols="' . $this->cols . '" rows="' . $this->rows .  '"' . 
						" style=\"width:{$widthCSS};height:{$heightCSS}\"" . 
						$this->_getExtraParametersString() . '>' . $this->value . "</textarea>\n"; 

			#add js:
			$html .= $this->_getJsBlockBegin();

			$extra = 'a:1'; //dummy(???)
			if($this->set == 'advanced') {
				$extra = ' theme_advanced_toolbar_location:"top",theme_advanced_toolbar_align:"left", theme_advanced_path_location:"bottom" ';
			}

			$html .= "tinyMCE.init({ mode:'exact', elements:'{$this->id}', theme:'{$this->set}', {$extra} });\n";

			$html .= $this->_getJsBlockEnd();

			$html .= $this->_getErrorHtml() . "\n" . $this->_getHintHtml() . "\n";

			$html .= "</li>";

			return $html;
			#return $this->_showTemplateVars();
		}

		public function getRequirements()
		{
			return '<script type="text/javascript" src="' . Application::getMappedPath(self::SCRIPT_URL) . '"></script>';
		}
	}
