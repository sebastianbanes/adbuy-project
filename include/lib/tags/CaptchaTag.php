<?php

	SmartyTag::get('TextFieldTag');

	class CaptchaTag extends TextFieldTag
	{
		const DEFAULT_CSS_CLASS = 'captcha-field';

		const DEFAULT_REFRESH_TEXT_CLASS = 'refresh';

		const DEFAULT_NAME = 'captcha';

		const DEFAULT_IMAGE_SRC = '/internals/captcha/?name=';

		const DEFAULT_REFRESH_TEXT = 'Refresh';
		const DEFAULT_REFRESH_IMG = '/images/refresh_icon.jpg';

		public $src;
		public $refreshtext;
		public $refreshimg;

		public function setParameters($params)
		{
			#name:
			if(!$this->name) {
				$this->name = self::DEFAULT_NAME;
			}

			parent::setParameters($params);

			#image src:
			if(!$this->src) {
				$this->src = self::DEFAULT_IMAGE_SRC;
			}
			$this->src = Application::getMappedPath($this->src);

			#refresh text:
			if(!$this->refreshtext) {
				$this->refreshtext = self::DEFAULT_REFRESH_TEXT;
			}

			#refresh text:
			if(!$this->refreshimg) {
				$this->refreshimg = self::DEFAULT_REFRESH_IMG;
			}
		}

		private function _getRefreshTags()
		{
			if($this->refreshimg) {
				return '<img src="' . Application::getMappedPath($this->refreshimg) . '" alt="' . $this->refreshtext . '">';
			}

			if($this->refreshtext) {
				return '<span>' . $this->refreshtext . '</span>';
			}

			return '';
		}

		public function getHtml()
		{
			$short_tag_end = $this->_getShortTagCloser();

			$class = $this->_getComputedClass(self::DEFAULT_CSS_CLASS);

			$html = "\n<li class=\"{$class}\">\n";

			#label:
			$html .= "<label for=\"{$this->id}\"" . 
			($this->accesskey ? ' accesskey="' . $this->accesskey . '"' : '') . 
			'>' . $this->label . "</label>\n";

			#image:
			$src = $this->src . $this->name;
			$img_html = '<img src="' . $src . '&amp;' . time() . '" alt=""' . $short_tag_end;

			#refresh link:
			$reloadjs = 'this.parentNode.getElementsByTagName(\'IMG\')[0].src=\'' . $src . 
				'&amp;\' + (new Date().getTime()); this.blur(); return false';
			$refresh_link_start_tag = '<a href="' . htmlentities($_SERVER['REQUEST_URI']) . '" onclick="' . $reloadjs . '" class="' . 
											self::DEFAULT_REFRESH_TEXT_CLASS . '">';

			$refresh_tags = $this->_getRefreshTags();
			if(!$refresh_tags) {
				// click pe captcha face refresh
				$html .= $refresh_link_start_tag . $img_html;
			} else {
				// click pe textul specificat face refresh
				$html .= $img_html . $refresh_link_start_tag . $refresh_tags;
			}
			$html .= '</a><br' . $short_tag_end;

			#input:
			$html .= '<input type="text" name="' . $this->name . '" id="' . $this->id . '" value="' . $this->value . '"' . 
							$this->_getExtraParametersString() . 
							$short_tag_end . "\n";

			$html .= $this->_getErrorHtml() . "\n" . $this->_getHintHtml() . "\n";

			$html .= "</li>";

			return $html;
			#return $this->_showTemplateVars();
		}
	}
