<?php

	SmartyTag::get('TextFieldTag');

	class DateFieldTag extends TextFieldTag
	{
		const DEFAULT_CSS_CLASS = 'date-field';

		const SCRIPT_URL = '/script/datepicker/datepicker.js';

		const BASE_LANG_URL = '/script/datepicker/languages/';
		const BASE_THEME_URL = '/script/datepicker/themes/';

		const CSS_THEME = 'dark'; // .css
		const DEFAULT_LANGUAGE = 'en'; // .js

		const TEMPLATE_LANGUAGE_VAR = 'lang';

		const DATE_FORMAT = 'yy-mm-dd'; // corespunde cu formatul "Y-m-d" pentru functia php date() -> compatibil cu formatul pt. mysql
		const DEFAULT_RANGE_YEARS = '1950:2020';

		public $rangeyears;

		public function setParameters($params)
		{
			parent::setParameters($params);

			#range years:
			if(empty($this->rangeyears)) {
				$this->rangeyears = self::DEFAULT_RANGE_YEARS;
			}
		}

		public function getHtml()
		{
			$class = $this->_getComputedClass(self::DEFAULT_CSS_CLASS);
			if (!(isset($this->li) && ($this->li == false))){
				$html = "\n<li class=\"{$class}\">\n";
			} else {
				$html = '';
			}
			
			
			$html .= "<label for=\"{$this->id}\"" . 
				($this->accesskey ? ' accesskey="' . $this->accesskey . '"' : '') . 
				'>' . $this->label . "</label>\n";

			$html .= '<input type="text" name="' . $this->name . '" id="' . $this->id . '" value="' . $this->value . '"' . 
							$this->_getExtraParametersString() . 
							$this->_getShortTagCloser() . "\n";

			$html .= $this->_getJsBlockBegin();

			#options:
			$options  = "{";

			$options .= "dateFormat:'" . self::DATE_FORMAT . "'";

			if($this->rangeyears) {
				$options .= ",yearRange: '" . $this->rangeyears . "'";
			}

			$options .= "}";
			// for more options, see here: http://docs.jquery.com/UI/Datepicker/datepicker

			$html .= "$(document).ready(function(){ $('#{$this->id}').datepicker({$options}); });";

			$html .= $this->_getJsBlockEnd();

			$html .= $this->_getErrorHtml() . "\n" . $this->_getHintHtml() . "\n";

			$html .= "</li>";

			return $html;
			#return $this->_showTemplateVars();
		}

		public function getRequirements()
		{
			#paths to include:
			$script_url = Application::getMappedPath(self::SCRIPT_URL);
			$css_url = Application::getMappedPath(self::BASE_THEME_URL . self::CSS_THEME . '.css');

			$code = "<style type=\"text/css\">@import url({$css_url});</style>\n" . 
					"<script type=\"text/javascript\" src=\"{$script_url}\"></script>\n";

			#includem language pack-ul (daca e necesar) - default e in engleza:
			$lang = $this->_getTemplateVar(self::TEMPLATE_LANGUAGE_VAR);
			$lang = $lang ? strtolower(trim($lang)) : self::DEFAULT_LANGUAGE;
			if($lang != 'en') {
				$lang_url = Application::getMappedPath(self::BASE_LANG_URL) . $lang . '.js';
				$code .= "<script type=\"text/javascript\" src=\"{$lang_url}\"></script>\n";
			}

			return $code;
		}
	}
