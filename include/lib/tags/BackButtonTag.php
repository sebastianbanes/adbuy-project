<?php

	SmartyTag::get('ButtonTag');

	class BackButtonTag extends ButtonTag
	{
		const DEFAULT_CSS_CLASS = 'button';
		const DEFAULT_VALUE = ' Back ';

		public function setParameters($params)
		{
			parent::setParameters($params);

			#value:
			if(empty($this->value)) {
				$this->value = self::DEFAULT_VALUE;
			}

			#onclick event:
			$this->onclick = " history.back() ";
		}

		// public function getHtml() # nu mai trebe suprascrisa functia!!!
	}
