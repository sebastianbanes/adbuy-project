<?php

	class FlashTag extends SmartyTag
	{
		const SCRIPT_URL = '/script/AC_RunActiveContent.js';

		const DEFAULT_CODEBASE = 'http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0';
		const DEFAULT_QUALITY = 'high';
		const DEFAULT_CLASSID = 'clsid:D27CDB6E-AE6D-11cf-96B8-444553540000';
		// pt. .dcr-uri, classid="clsid:166B1BCA-3F9C-11CF-8075-444553540000"
		const DEFAULT_TYPE = 'application/x-shockwave-flash';
		const DEFAULT_PLUGINSPACE = 'http://www.macromedia.com/go/getflashplayer';

		// required
		public $src;
		public $movie; // alterlativa la src...
		public $data; // alta alternativa...
		public $width;
		public $height;

		// optional:
		public $codebase;
		public $classid;
		public $type;
		public $pluginspage;

		public $quality;
		public $swliveconnect;
		public $play;
		public $loop;
		public $menu;
		public $scale;
		public $align;
		public $salign;
		public $wmode;
		public $bgcolor;
		public $base;
		public $flashvars;
		public $devicefont;
		public $allowScriptAccess;
		public $allowFullScreen;

		// continut alternativ:
		public $alt;
		
		public function setParameters($params)
		{
			parent::setParameters($params);

			#src/movie/data:
			if(!$this->src) {
				if($this->movie) {
					$this->src = $this->movie;
				} else {
					$this->src = $this->data; // poate fi null si asta...
				}
			}
			
			$this->movie = $this->data = $this->src; // folosim doar unul singur: src

			#codebase:
			if(!$this->codebase) {
				$this->codebase = self::DEFAULT_CODEBASE;
			}

			#quality:
			if(!$this->quality) {
				$this->quality = self::DEFAULT_QUALITY;
			}

			#classid:
			$this->classid = self::DEFAULT_CLASSID;

			#pluginspage:
			$this->pluginspage = self::DEFAULT_PLUGINSPACE;

			#type:
			$this->type = self::DEFAULT_TYPE;

			#id (nu mai trebe htmlspecialchars):
			$this->id = $this->_cleanId($this->id);


			#flashvars:
			$this->flashvars = $this->flashvars;
		}
		
		public function getSrcWithoutExt()
		{
			return substr($this->src, 0, strrpos($this->src, '.', 1));
		}

		public function getHtml()
		{
			if(!$this->src) {
				$this->_showError("No source specified for FlashTag.");
				return;
			}

			if(!$this->width) {
				$this->_showError("No width specified for FlashTag.");
				return;
			}

			if(!$this->height) {
				$this->_showError("No height specified for FlashTag.");
				return;
			}

			$short_tag_encloser = $this->_getShortTagCloser() . "\n";

			$static_html  = "<object type=\"{$this->type}\" data=\"{$this->src}\" width=\"{$this->width}\" height=\"{$this->height}\"";
			if(self::_isIE()) {  // Firefox fix...
				$static_html .= ' codebase=\"' . htmlentities($this->codebase) . '" classid="' . htmlentities($this->classid) . '"';
			}
			$static_html .= $this->_getExtraParametersString() . ">\n";

			$static_html .= '<param name="movie" value="' . htmlentities($this->src) . '"' . $short_tag_encloser;
			$static_html .= '<param name="quality" value="' . $this->quality . '"' . $short_tag_encloser;

			if($this->menu) {
				$static_html .= '<param name="menu" value="' . $this->menu . '"' . $short_tag_encloser;
			}

			if($this->play) {
				$static_html .= '<param name="play" value="' . $this->play . '"' . $short_tag_encloser;
			}

			if($this->loop) {
				$static_html .= '<param name="loop" value="' . $this->loop . '"' . $short_tag_encloser;
			}

			if($this->devicefont) {
				$static_html .= '<param name="devicefont" value="' . $this->devicefont . '"' . $short_tag_encloser;
			}

			if($this->bgcolor) {
				$static_html .= '<param name="bgcolor" value="' . $this->bgcolor . '"' . $short_tag_encloser;
			}

			if($this->wmode) {
				$static_html .= '<param name="wmode" value="' . $this->wmode . '"' . $short_tag_encloser;
			}

			if($this->flashvars) {
				$static_html .= '<param name="flashvars" value="' . $this->flashvars . '"' . $short_tag_encloser;
			}

			if($this->base) {
				$static_html .= '<param name="base" value="' . $this->base . '"' . $short_tag_encloser;
			}

			if($this->scale) {
				$static_html .= '<param name="scale" value="' . $this->scale . '"' . $short_tag_encloser;
			}

			if($this->align) {
				$static_html .= '<param name="align" value="' . $this->align . '"' . $short_tag_encloser;
			}

			if($this->salign) {
				$static_html .= '<param name="salign" value="' . $this->salign . '"' . $short_tag_encloser;
			}

			if($this->allowScriptAccess) {
				$static_html .= '<param name="allowScriptAccess" value="' . $this->allowScriptAccess . '"' . $short_tag_encloser;
			}

			if($this->allowFullScreen) {
				$static_html .= '<param name="allowFullScreen" value="' . $this->allowFullScreen . '"' . $short_tag_encloser;
			}

			$static_html .= "{$this->alt}\n";

			$static_html .= "</object>";

			$html  = $this->_getJsBlockBegin() . $this->_getJavascript() . $this->_getJsBlockEnd();
			$html .= '<noscript>' . $static_html . '</noscript>';

			return $html;
		}

		private static function _isIE()
		{
			return preg_match("/MSIE/i", ''.getenv("HTTP_USER_AGENT"));
		}

		protected function _getJavascript()
		{
			$js  = " AC_FL_RunContent(" . 
					" 'codebase', '{$this->codebase}'" . 
					", 'pluginspage', '{$this->pluginspage}'" . 
					", 'width', '{$this->width}'" . 
					", 'height', '{$this->height}'" . 
					", 'src', '" . $this->getSrcWithoutExt() . "'" . 
					", 'movie', '" . $this->getSrcWithoutExt() . "'" . 
					", 'quality', '{$this->quality}'" . 
					($this->flashvars ? ", 'FlashVars', '{$this->flashvars}'" : '') . 
					($this->bgcolor ? ", 'bgcolor', '{$this->bgcolor}'" : '') . 
					($this->align ? ", 'align', '{$this->align}'" : '') . 
					($this->salign ? ", 'salign', '{$this->salign}'" : '') . 
					($this->scale ? ", 'scale', '{$this->scale}'" : '') . 
					($this->wmode ? ", 'wmode', '{$this->wmode}'" : '') . 
					($this->id ? ", 'id', '{$this->id}'" : '') . 
					($this->allowScriptAccess ? ", 'allowScriptAccess', '{$this->allowScriptAccess}'" : '') . 
					($this->menu ?  ", 'menu', '{$this->menu}'" : '') .
					($this->play ?  ", 'play', '{$this->play}'" : '') .
					($this->loop ?  ", 'loop', '{$this->loop}'" : '') .
					($this->devicefont ?  ", 'devicefont', '{$this->devicefont}'" : '') . 
					($this->allowFullScreen ?  ", 'allowFullScreen', '{$this->allowFullScreen}'" : '') . 
			") ";

			return $js;
		}

		public function getRequirements()
		{
			$script_url = Application::getMappedPath(self::SCRIPT_URL);

			$code = "<script type=\"text/javascript\" src=\"{$script_url}\"></script>\n";

			return $code;
		}
	}
