<?php

	class FieldsetTag extends SmartyTag
	{
		const DEFAULT_CSS_CLASS = 'fieldset-parent';

		public $content;

		public $legend;
		public $class;
		public $id;

		public function setParameters($params)
		{
			parent::setParameters($params);

			#legend:
			$this->legend = htmlspecialchars($this->legend);

			#id:
			$this->id = $this->_cleanId($this->id);

			#class:
			$this->class = $this->class ? $this->class : self::DEFAULT_CSS_CLASS;
			$this->class = htmlspecialchars($this->class);
		}

		public function getHtml()
		{
			$html  = "<div" . ($this->class ? ' class="' . htmlspecialchars($this->class) . '"' : '') . ">\n<fieldset" . 
				($this->id ? ' class="' . htmlspecialchars($this->id) . '"' : '') . 
				$this->_getExtraParametersString() . 
			">\n";

			if($this->legend) {
				$html .= '<legend><span>' . htmlspecialchars($this->legend) . '</span></legend>';
			}

			$html .= $this->content;

			$html .= "</fieldset>\n</div>";

			return $html;
		}
	}
