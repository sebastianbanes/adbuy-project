<?php

	class FormTag extends SmartyTag
	{
		const DEFAULT_METHOD = 'post';
		const DEFAULT_ACTION = '';	// postback, by default
		const DEFAULT_CSS_CLASS = ''; // no class, by default

		public $content;

		public $method;
		public $action;
		public $enctype; // default, e calculat automat
		public $name;
		public $id;
		public $class;

		#field auto-focused:
		public $autofocus;

		public function setParameters($params)
		{
			parent::setParameters($params);

			#method:
			if(!$this->method) {
				$this->method = self::DEFAULT_METHOD;
			}

			#action:
			if(!$this->action) {
				$this->action = self::DEFAULT_ACTION;
			}
			$this->action = $this->action ? 
								Application::getMappedPath($this->action) : 
								$_SERVER['REQUEST_URI'];
			$this->action = htmlspecialchars($this->action);

			#enctype:
			if(!$this->enctype && $this->_haveUploadField()) {
				$this->enctype = 'multipart/form-data'; // cand e empty, nu va fi specificat deloc
			}

			#class:
			$this->class = $this->class ? $this->class : self::DEFAULT_CSS_CLASS;
			$this->class = htmlspecialchars($this->class);

			#id (nu mai trebe htmlspecialchars):
			$this->id = $this->id ? $this->id : $this->name;
			$this->id = $this->_cleanId($this->id);
		}

		public function getHtml()
		{
			$html  = '<form action="' . $this->action . 
						'" method="' . $this->method . '"' . 
						($this->class ? ' class="' . $this->class . '"' : '') . 
						($this->enctype ? ' enctype="' . $this->enctype . '"' : '') . 
						($this->name ? ' name="' . $this->name . '"' : '') . 
						($this->id ? ' id="' . $this->id . '"' : '') . 
						$this->_getExtraParametersString() . 
						">\n" . 
						$this->_cleanTags($this->content) . 
						#$this->content . 
						"</form>\n";

			if($this->autofocus) {
				$html .= $this->_getJsBlockBegin();
				$html .= '$("#' . htmlspecialchars($this->autofocus) . '").focus()';
				$html .= $this->_getJsBlockEnd();
			}

			return $html;
		}

		private function _haveUploadField()
		{
			return (bool)preg_match('#<input.*\stype=["|\']file["|\'].*>#is', $this->content);
		}

		private function _cleanTags($html)
		{
			$html = preg_replace('#</li>\s*<li#is', '</li><li', $html); // stergem spatiile dintre </li> si <li>...altfel nu merg urmatoarele...

			$html = preg_replace('#(?<!</li>)<li#is', "\n<ul>\n<li", $html); // adaugam <ul> pt. <li>-urile este care primele din lista
			$html = preg_replace('#(<ul[^>]*>)\s*<ul>#is', "$1\n", $html); // stergem <ul> -urile duble...

			$html = preg_replace('#</li>(?!<li)#is', "</li>\n</ul>\n", $html); // adaugam <ul> pt. <li>-urile este care ultimele din lista
			$html = preg_replace('#</ul>\s*</ul>#is', "\n</ul>\n", $html); // stergem </ul> -urile duble...

			$html = str_replace("</li><li", "</li>\n\n<li", $html); // adaugam 2 newline intre <li>-uri(sa fie lizibile)

			return $html;
		}
	}
