<?php

	SmartyTag::get('FormFieldTag');

	class TextFieldTag extends FormFieldTag
	{
		const DEFAULT_CSS_CLASS = 'text-field';

		public function getHtml()
		{
			$class = $this->_getComputedClass(self::DEFAULT_CSS_CLASS);

			$html = "\n<li class=\"{$class}\">\n";

			$html .= "<label for=\"{$this->id}\"" . 
			($this->accesskey ? ' accesskey="' . $this->accesskey . '"' : '') . 
			'>' . $this->label . "</label>\n";

			$html .= '<input type="text" name="' . $this->name . '" id="' . $this->id . '" value="' . $this->value . '"' . 
							$this->_getExtraParametersString() . 
							$this->_getShortTagCloser() . "\n";

			$html .= $this->_getErrorHtml() . "\n" . $this->_getHintHtml() . "\n";

			$html .= "</li>";

			return $html;
			#return $this->_showTemplateVars();
		}
	}
