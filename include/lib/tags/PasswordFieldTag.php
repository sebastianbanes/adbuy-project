<?php

	SmartyTag::get('FormFieldTag');

	class PasswordFieldTag extends FormFieldTag
	{
		const DEFAULT_CSS_CLASS = 'password-field';
		const DEFAULT_WITH_STRENGTH_CSS_CLASS = 'password-field-with-strength';

		const SCRIPT_URL = '/script/passwordstrength/pstrength.js';

		public $showstrength;

		// alte detalii: http://simplythebest.net/scripts/ajax/ajax_password_strength.html

		public function setParameters($params)
		{
			parent::setParameters($params);

			#show strength:
			$this->showstrength = empty($this->showstrength) ? false : ($this->showstrength != 'false');
		}

		public function getHtml()
		{
			if($this->showstrength) {
				$class = $this->_getComputedClass(self::DEFAULT_WITH_STRENGTH_CSS_CLASS);
			} else {
				$class = $this->_getComputedClass(self::DEFAULT_CSS_CLASS);
				
			}

			$html = "\n<li class=\"{$class}\">\n";

			$html .= "<label for=\"{$this->id}\"" . 
			($this->accesskey ? ' accesskey="' . $this->accesskey . '"' : '') . 
			'>' . $this->label . "</label>\n";

			$html .= '<input type="password" name="' . $this->name . '" id="' . $this->id . '" value=""' . 
							$this->_getExtraParametersString() . 
							$this->_getShortTagCloser() . "\n";

			#show strength:
			if($this->showstrength) {
				$html .= $this->_getJsBlockBegin();

				$html .= "$(document).ready(function(){ $('#{$this->id}').pstrength(); })";

				$html .= $this->_getJsBlockEnd();
			}

			$html .= $this->_getErrorHtml() . "\n" . $this->_getHintHtml() . "\n";

			$html .= "</li>";

			return $html;
			#return $this->_showTemplateVars();
		}

		public function getRequirements()
		{
			$script_url = Application::getMappedPath(self::SCRIPT_URL);

			$code = "<script type=\"text/javascript\" src=\"{$script_url}\"></script>\n";

			return $code;
		}

	}
