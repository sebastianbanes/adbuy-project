<?php

	SmartyTag::get('FormFieldTag');

	class TextareaTag extends FormFieldTag
	{
		const DEFAULT_CSS_CLASS = 'text-area';
		const DEFAULT_COLS = '80';
		const DEFAULT_ROWS = '10';

		public $cols;
		public $rows;

		public function setParameters($params)
		{
			parent::setParameters($params);

			#cols & rows:
			$this->cols = empty($this->cols) ? self::DEFAULT_COLS : $this->cols;
			$this->rows = empty($this->rows) ? self::DEFAULT_ROWS : $this->rows;
		}

		public function getHtml()
		{
			$class = $this->_getComputedClass(self::DEFAULT_CSS_CLASS);

			$html = "\n<li class=\"{$class}\">\n";

			$html .= "<label for=\"{$this->id}\"" . 
			($this->accesskey ? ' accesskey="' . $this->accesskey . '"' : '') . 
			'>' . $this->label . "</label>\n";

			$html .= '<textarea name="' . $this->name . '" id="' . $this->id . '" cols="' . $this->cols . '" rows="' . $this->rows .  '"' . 
						$this->_getExtraParametersString() . '>' . $this->value . "</textarea>\n"; 

			$html .= $this->_getErrorHtml() . "\n" . $this->_getHintHtml() . "\n";

			$html .= "</li>";

			return $html;
			#return $this->_showTemplateVars();
		}
	}
