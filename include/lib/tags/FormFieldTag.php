<?php

	class FormFieldTag extends SmartyTag
	{
		const REQUIRED_FIELD_CSS_CLASS = 'required-field';
		const ERROR_FIELD_CSS_CLASS = 'error-field';
		const ERROR_FIELD_MESSAGE_CSS_CLASS = 'error-field-message';
		const HINT_MESSAGE_CSS_CLASS = 'hint-message';
		const ACCESSKEY_CHAR_CSS_CLASS = 'access-key-char';

		const ERRORS_TEMPLATE_VAR = 'errors';

		public $name;
		public $label;
		public $value;
		public $required;
		public $accesskey;

		public $hint;
		public $error;

		public function isRequiredField()
		{
			if(empty($this->required)) {
				return false;
			}

			return ($this->required != 'false');
		}

		protected function _getParentFormName()
		{
			$form_name = '';

			$stack = $this->smarty->_tag_stack;
			foreach($stack as $tag) {
				$tag_name = strtolower($tag[0]);
				$tag_params = $tag[1];

				if($tag_name == 'form' && isset($tag_params['name'])) { // ar trebui sa fie in in $stack[0]...da daca folosim fieldset si buttonsgroup...
					$form_name = $tag_params['name'];
					break;
				}
			}

			return $form_name;
		}

		protected function _getHintHtml()
		{
			if($this->hint) {
				return '<p class="' . self::HINT_MESSAGE_CSS_CLASS . '">' . $this->hint . '</p>';
			}

			return '';
		}

		protected function _getErrorHtml()
		{
			if($this->error) {
				return '<p class="' . self::ERROR_FIELD_MESSAGE_CSS_CLASS . '">' . $this->error . '</p>';
			}

			return '';
		}

		public function setParameters($params)
		{
			parent::setParameters($params);

			#compute name:
			$name = empty($this->name) ? '' : $this->name;
			if(!$name) {
				$this->_showError('No name specified for ' . get_class($this)) . '.';
				return;
			}

			#if(preg_match('/[^[a-z][0-9]\.\-:_\[\]]/i', $name)) {
			#	$this->_showError('Invalid name specified for ' . get_class($this)) . '.';
			#	return;
			#}

			#nu mai trebe htmlspecialchars:
			// skip

			#compute id:
			if(empty($this->id)) { // always have id:
				$form_name = $this->_getParentFormName();
				$this->id = $form_name ? $form_name . '_' . $name : $name;
			}

			#clean up id (nu mai trebe htmlspecialchars):
			$this->id = $this->_cleanId($this->id);

			#compute value:
			$value = empty($this->value) ? (string)$this->_getTemplateVar($name) : $this->value;
			$this->value = htmlspecialchars($value);

			#accesskey:
			if(!empty($this->accesskey)) {
				$this->accesskey = (string)$this->accesskey;
				$this->accesskey = $this->accesskey{0};
			}

			#compute label:
			$this->label = empty($this->label) ? '&nbsp;' : htmlspecialchars($this->label);
			if($this->accesskey) {
				$pos = strpos(strtolower($this->label), strtolower($this->accesskey));
				if($pos !== false) {
					$replacement = '<span class="' . self::ACCESSKEY_CHAR_CSS_CLASS . '">' . $this->label{$pos} . '</span>';
					$this->label = substr_replace($this->label, $replacement, $pos, 1);
				}
			}
			if($this->isRequiredField()) {
				$this->label .= ' <em>*</em>';
			}

			#error message:
			if(!isset($params['error'])) { // daca nu e setata ca param., o luam din variabilele setate in smarty:
				$errors = $this->_getTemplateVar(self::ERRORS_TEMPLATE_VAR);
				if($errors && is_array($errors)) {
					$this->error = htmlspecialchars((string)$errors[$name]);
				}
			}

			#hint:
			#$this->hint = htmlspecialchars($this->hint); //trebe comentat!!! altfel, nu se pot baga linkuri & stuff...

			#compute class:
			$class = empty($this->class) ? '' : $this->class . ' ';
			if($this->isRequiredField()) {
				$class .= self::REQUIRED_FIELD_CSS_CLASS . ' ';
			}

			if($this->error) { // setat mai sus corespunzator...
				$class .= self::ERROR_FIELD_CSS_CLASS . ' ';
			}

			$this->class = htmlspecialchars($class);
		}

		protected function _getComputedClass($default_class = null)
		{
			return (str_word_count($this->class) < 3) ? ($default_class . ' ' . $this->class) : $this->class;
		}
	}
