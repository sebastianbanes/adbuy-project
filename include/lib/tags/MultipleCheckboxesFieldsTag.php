<?php

	SmartyTag::get('FormFieldTag');

	class MultipleCheckboxesFieldsTag extends FormFieldTag
	{
		const DEFAULT_CSS_CLASS = 'multiple-checkbox-fields';
		const DEFAULT_VALUE_PROPERTY = 'id';
		const DEFAULT_LABEL_PROPERTY = 'name';
		const BASE_OBJECT_PARENT_CLASS = 'BaseObject';
		const DEFAULT_COLUMN_NUMBER = 1;

		public $legend;

		public $options;
		public $checked;
		public $key;
		public $field;

		public $cols;

		// TODO: add some sort of check all/uncheck all???

		public function setParameters($params)
		{
			parent::setParameters($params);

			#name:
			$this->name = rtrim($this->name, '[]');

			#legend:
			if($this->legend) {
				$this->legend = htmlspecialchars($this->legend);
				#$this->label = null; //astea 2 sunt exclusive ???
			}

			#key & field (for BaseObject arrays):
			$this->key = empty($this->key) ? self::DEFAULT_VALUE_PROPERTY : $this->key;
			$this->field = empty($this->field) ? self::DEFAULT_LABEL_PROPERTY : $this->field;

			#options (play safe):
			$this->options = (array)$this->options;

			#checked options:
			if(empty($this->checked)) {
				$this->checked = $this->_getTemplateVar($this->name);
			}

			if(empty($this->checked)) {
				$this->checked = array();
			} else if(is_scalar($this->checked)) {
				$this->checked = preg_split("/[\s\.,;]+/", $this->checked);
			} else {
				$this->checked = (array)$this->checked;
			}

			#columns:
			$this->cols = intval($this->cols);
			if(empty($this->cols)) {
				$this->cols = self::DEFAULT_COLUMN_NUMBER;
			}
		}

		public function getHtml()
		{
			$class = $this->_getComputedClass(self::DEFAULT_CSS_CLASS);

			$html = "\n<li class=\"{$class}\">\n";

			#if($this->label) { // devine optional...
			#	$html .= "<label>" . $this->label . "</label>\n";
			#}

			if(!empty($this->options)) {
				$html .= "<div>\n<fieldset>\n";

				if($this->legend) {
					$html .= '<legend><span>' . $this->legend . "</span></legend>\n";
				}

				$html .= "<ul>\n";

				$end_tag = $this->_getShortTagCloser() . "\n";
				$checked_attribute = $xhtml ? ' checked="checked"' : ' checked';
				$extra_params = $this->_getExtraParametersString();

				$items_per_column = ceil(count($this->options) / $this->cols);
				$item_no = 0;

				if($this->_useBaseObjects()) {
					$key = $this->key;
					$field = $this->field;

					foreach($this->options as $option) {
						$option_value = isset($option->$key) ? htmlspecialchars($option->$key) : '';
						$option_label = isset($option->$field) ? htmlspecialchars($option->$field) : '&nbsp;';
						$option_checked = in_array($option_value, $this->checked) ? $checked_attribute : '';

						$option_name = $this->name . '[' . $option_value . ']';
						$option_id = $this->id . '_' . $this->_clearId($option_value);

						$html .= "<li><label for=\"{$option_id}\">\n";
						$html .= "<input type=\"checkbox\" name=\"{$option_name}\" id=\"{$option_id}\" value=\"{$option_label}\"" . 
									$option_checked . $extra_params . $end_tag;
						$html .= "<span>{$option_label}</span>\n";
						$html .= "</label></li>\n";

						$item_no++;
						if($item_no >= $items_per_column) {
							$item_no = 0;

							$html .= "</ul><ul>\n";
						}
					}
				} else {
					foreach($this->options as $option_value => $option_label) {
						$option_checked = in_array($option_value, $this->checked) ? $checked_attribute : '';

						$option_name = $this->name . '[' . $option_value . ']';
						$option_id = $this->id . '_' . $this->_cleanId($option_value);

						$html .= "<li><label for=\"{$option_id}\">\n";
						$html .= "<input type=\"checkbox\" name=\"{$option_name}\" id=\"{$option_id}\" value=\"{$option_label}\"" . 
									$option_checked . $extra_params . $end_tag;
						$html .= "<span>{$option_label}</span>\n";
						$html .= "</label></li>\n";

						$item_no++;
						if($item_no >= $items_per_column) {
							$item_no = 0;

							$html .= "</ul><ul>\n";
						}
					}
				}

				// stergem ultimele 5 caractere din coada:
				$html  = substr($html, 0, -5) . "\n" . 
						$this->_getErrorHtml() . "\n" . 
						$this->_getHintHtml() . "\n" . 
						"</fieldset>\n</div>\n";
			}

			$html .= "</li>";

			return $html;
			#return $this->_showTemplateVars();
		}

		private function _useBaseObjects()
		{
			$first_elem = reset($this->options);

			if(is_object($first_elem) && is_a($first_elem, self::BASE_OBJECT_PARENT_CLASS)) {
				return true;
			}

			return false;
		}
	}
