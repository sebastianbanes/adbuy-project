<?php

	SmartyTag::get('TextFieldTag');

	class ColorFieldTag extends TextFieldTag
	{
		const DEFAULT_CSS_CLASS = 'color-field';

		const SCRIPT_URL = '/script/colorpicker/farbtastic.js';
		const STYLESHEET_URL = '/script/colorpicker/farbtastic.css';

		const DEFAULT_VALUE = '#FFF';

		const DEFAULT_COLORWHEEL_ID_PREFIX = 'colorwheel_';

		public $wheelid;

		// vezi http://acko.net/dev/farbtastic pentru mai multe detalii

		public function setParameters($params)
		{
			parent::setParameters($params);

			#wheel id:
			if(!$this->wheelid) {
				$this->wheelid = self::DEFAULT_COLORWHEEL_ID_PREFIX . $this->id;
			}

			#color selected: (important!)
			if(!$this->value) {
				$this->value = self::DEFAULT_VALUE;
			}
		}

		public function getHtml()
		{
			$class = $this->_getComputedClass(self::DEFAULT_CSS_CLASS);
			$html = "\n<li class=\"{$class}\">\n";

			$html .= "<label for=\"{$this->id}\"" . 
			($this->accesskey ? ' accesskey="' . $this->accesskey . '"' : '') . 
			'>' . $this->label . "</label>\n";

			$html .= '<input type="text" name="' . $this->name . '" id="' . $this->id . '" value="' . $this->value . '"' . 
							$this->_getExtraParametersString() . 
							$this->_getShortTagCloser() . "\n";

			$html .= '<div id="' . $this->wheelid . '"></div>';

			$html .= $this->_getJsBlockBegin();

			$html .= "$(document).ready(function(){ $('#{$this->wheelid}').farbtastic('#{$this->id}')})";

			$html .= $this->_getJsBlockEnd();

			$html .= $this->_getErrorHtml() . "\n" . $this->_getHintHtml() . "\n";

			$html .= "</li>";

			return $html;
			#return $this->_showTemplateVars();
		}

		public function getRequirements()
		{
			$script_url = Application::getMappedPath(self::SCRIPT_URL);
			$css_url = Application::getMappedPath(self::STYLESHEET_URL);

			$code = "<style type=\"text/css\">@import url({$css_url});</style>\n" . 
					"<script type=\"text/javascript\" src=\"{$script_url}\"></script>\n";

			return $code;
		}
	}
