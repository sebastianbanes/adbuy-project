<?php

	class ErrorTag extends SmartyTag
	{
		const DEFAULT_TEMPLATE_VAR = 'error';
		const DEFAULT_CSS_CLASS = 'error-box';

		public $message;

		public function setParameters($params)
		{
			parent::setParameters($params);

			#message:
			if(!array_key_exists('message', $params)) { // !!!
				$this->message = $this->_getTemplateVar(self::DEFAULT_TEMPLATE_VAR);
			}

			#class:
			$this->class = $this->class ? $this->class : self::DEFAULT_CSS_CLASS;
			$this->class = htmlspecialchars($this->class);

			#id (nu mai trebe htmlspecialchars):
			$this->id = $this->_cleanId($this->id);
		}

		public function getHtml()
		{
			if (!$this->message) {
				return '';
		    }

		    $html  = '<p class="' . $this->class . '"';

			if($this->id) {
				$html .= ' id="' . $this->id . '"';
			}

			$html .= $this->_getExtraParametersString() . '>' . $this->message . '</p>';

			return $html;
		}
	}
