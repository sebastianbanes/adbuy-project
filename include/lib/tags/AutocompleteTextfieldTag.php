<?php

	SmartyTag::get('TextFieldTag');

	class AutocompleteTextfieldTag extends TextFieldTag
	{
		const DEFAULT_MIN_CHARS = 3;
		const DEFAULT_MAX_ITEMS = 10;
		const DEFAULT_DELAY = 10;

		const DEFAULT_CSS_CLASS = 'autocomplete-field';

		const SCRIPT_URL = '/script/autocomplete/autocomplete.js';
		const STYLESHEET_URL = '/script/autocomplete/autocomplete.css';

		public $options;		// astea 2 sunt exclusive: ori una, ori alta!!!
		public $url;

		public $delay;
		public $minchars;
		public $maxitems;
		public $autocomplete; // always off!!!!

		
		 // alte detalii: http://www.pengoworks.com/workshop/jquery/autocomplete_ajax.cfm.txt
		

		public function setParameters($params)
		{
			parent::setParameters($params);

			#url:
			$this->url = htmlspecialchars($this->url);

			#options:
			if(!empty($this->options)) {
				$this->options = is_scalar($this->options) ? 
								$this->options :
								'["' . implode('","', (array)$this->options) . '"]';
			}

			#minchars:
			$this->minchars = intval($this->minchars);
			if(!$this->minchars) {
				$this->minchars = self::DEFAULT_MIN_CHARS;
			}

			#maxitems:
			$this->maxitems = intval($this->maxitems);
			if(!$this->maxitems) {
				$this->maxitems = self::DEFAULT_MAX_ITEMS;
			}

			#delay:
			$this->delay = intval($this->delay);
			if(!$this->delay) {
				$this->delay = self::DEFAULT_DELAY;
			}

			#autocomplete:
			$this->autocomplete = "off";
		}

		public function getHtml()
		{
			$class = $this->_getComputedClass(self::DEFAULT_CSS_CLASS);

			$html = "\n<li class=\"{$class}\">\n";

			$html .= "<label for=\"{$this->id}\"" . 
			($this->accesskey ? ' accesskey="' . $this->accesskey . '"' : '') . 
			'>' . $this->label . "</label>\n";

			$html .= '<input type="text" autocomplete="off" name="' . $this->name . '" id="' . $this->id . '" value="' . $this->value . '"' . 
							$this->_getExtraParametersString() . 
							$this->_getShortTagCloser();

			#add js:
			$html .= $this->_getJsBlockBegin();

			$jsoptions = "{ delay:10, minChars:{$this->minchars}, maxItemsToShow:{$this->maxitems} }";

			$html .= '$(document).ready(function(){ ';
			if($this->options) {
				$html .= "$('#{$this->id}').autocompleteArray('{$this->url}', {$jsoptions});";
			} else {
				$html .= "$('#{$this->id}').autocomplete('{$this->url}', {$jsoptions});";
			}
			$html .= ' })';

			$html .= $this->_getJsBlockEnd();
			#end js

			$html .= $this->_getErrorHtml() . "\n" . $this->_getHintHtml() . "\n";

			$html .= "</li>";

			return $html;
			#return $this->_showTemplateVars();
		}

		public function getRequirements()
		{
			$script_url = Application::getMappedPath(self::SCRIPT_URL);
			$css_url = Application::getMappedPath(self::STYLESHEET_URL);

			$code = "<script type=\"text/javascript\" src=\"{$script_url}\"></script>\n" . 
					"<style type=\"text/css\">@import url({$css_url});</style>\n";

			return $code;
		}
	}
