<?php

	class ButtonsGroupTag extends SmartyTag
	{
		const DEFAULT_CSS_CLASS = 'buttons-group';

		public $content;

		public function getHtml()
		{
			return '<li class="' . self::DEFAULT_CSS_CLASS . "\">\n{$this->content}\n</li>";
		}
	}
