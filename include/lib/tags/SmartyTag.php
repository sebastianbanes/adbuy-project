<?php

	class SmartyTag
	{
		const TEMPLATE_ERROR_CSS_CLASS = 'error-tag';

		public static $tags_ref = array();

		protected $smarty;
		protected $index;

		public $xhtml;
		public $id;
		public $class;

		public function __construct()
		{
			if(func_num_args()) {
				$first_param = func_get_arg(0);
				$this->setSmarty($first_param);
			}
		}

		public function setParameters($params)
		{
			$params = (array)$params;

			foreach($params as $param_name => $param_value) {
				$this->$param_name = $param_value;
			}

			#compute boolean xhtml:
			if(empty($this->xhtml)) {
				global $config;
				$this->xhtml = (bool)$config->smarty->tags->xhtml_compliant;
			} else {
				$this->xhtml = ($this->xhtml != 'false');
			}

			#compute compliant id & class: // not required, for now...
			#$this->id = $this->_cleanId($this->id);
			#$this->class = $this->_cleanId($this->class);
		}

		public function getParameters()
		{
			$ret = array_intersect_key(
						get_object_vars($this), 
						get_class_vars(get_class($this))
					);
			unset($ret['smarty'], $ret['index']);

			return $ret;
		}

		public function getExtraParameters()
		{
			$ret = array_diff_key(
						get_object_vars($this), 
						get_class_vars(get_class($this))
					);
			unset($ret['smarty'], $ret['index']);

			return $ret;
		}

		public function getAllParameters()
		{
			return array_merge($this->getExtraParameters(), $this->getParameters());
		}

		protected function _getExtraParametersString()
		{
			$params = $this->getExtraParameters();

			$str = '';

			foreach($params as $param_name => $param_value) {
				$str .= ' ' . htmlspecialchars($param_name) . '="' . htmlspecialchars($param_value) . '"';
			}

			return $str;
		}

		public function setSmarty(&$smarty)
		{
			// ar trebui testat cumva sa fie obiect smarty...
			$this->smarty = $smarty;
		}

		public function &getSmarty($smarty)
		{
			$ret &= $this->smarty;
			return $ret;
		}

		public function getRequirements()
		{
			// default, no requirements
			return '';
		}

		public function getHtml()
		{
			trigger_error('You cannot use the SmartyTag directly.');
		}

		public static function get($name)
		{
			$dir_path = LIB_PATH . '/tags/';
			$path = $dir_path . $name;

			#testam daca s-a specificat extensia(.php):
			if(file_extension($name) != 'php') {
				$path .= '.php';
			}

			if(file_exists($path)) {
				include_once $path;
			} else {
				trigger_error("SmartyTag::get('{$name}') error: tag not found in '{$dir_path}'.", E_USER_ERROR);
			}
		}

		public static function newInstance($name, $params, &$smarty)
		{
			SmartyTag::get($name);

			$tag = new $name;
			$tag->setSmarty($smarty);
			$tag->setParameters($params);

			// inregistram tagul (grupate in functie de class name):
			self::$tags_ref[$name][] = &$tag;

			$tag->index = count(self::$tags_ref[$name]) - 1;

			return $tag->getHtml();
		}

		protected function _getShortTagCloser()
		{
			return $this->xhtml ? ' />' : '>';
		}

		protected function _getJsBlockBegin()
		{
			$html  = "\n<script type=\"text/javascript\">";
			$html .= $this->xhtml ? "\n/* <![CDATA[ */\n" : "\n";

			return $html;
		}

		protected function _getJsBlockEnd()
		{
			$html  = $this->xhtml ? "\n/* ]]> */\n" : '';
			$html .= "</script>\n";

			return $html;
		}

		protected function _cleanId($id)
		{
			return preg_replace('/[^[a-z][0-9]\.\-:_]/i', '_', $id);
		}

		protected function _showError($message)
		{
			echo '<span class="' . self::TEMPLATE_ERROR_CSS_CLASS . '">' . htmlentities($message) . '</span>' . 
					'<pre><b>Parameters given:</b><br>' . htmlentities(print_r($this->getAllParameters(), true)) . '</pre>';
		}

		protected function _getTemplateVar($name)
		{
			return $this->smarty->get_template_vars($name);
		}

		protected function _showTemplateVars()
		{
			return '<pre>' . htmlentities(print_r($this->smarty->get_template_vars(), true)) . '</pre>';
		}
	}
