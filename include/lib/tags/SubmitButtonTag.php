<?php

	class SubmitButtonTag extends SmartyTag
	{
		const DEFAULT_CSS_CLASS = 'button';

		public $type;

		public $name;
		public $value;

		public $image;
		public $alt;

		protected function _getParentFormName()
		{
			$form_name = '';

			$stack = $this->smarty->_tag_stack;
			foreach($stack as $tag) {
				$tag_name = strtolower($tag[0]);
				$tag_params = $tag[1];

				if($tag_name == 'form' && isset($tag_params['name'])) { // ar trebui sa fie in in $stack[0]...da daca folosim fieldset si buttonsgroup...
					$form_name = $tag_params['name'];
					break;
				}
			}

			return $form_name;
		}

		public function setParameters($params)
		{
			parent::setParameters($params); // nu putem folosi asta, pt. ca nu mai e obligatoriu numele

			#value (if input submit):
			$this->value = htmlspecialchars($this->value);

			#alternative text(if input image):
			$this->alt = htmlspecialchars($this->alt);

			#src path (pt. input image) - fara cache (cel putin deocamdata):
			if($this->image) {
				$this->image = Application::getMappedPath($this->image);
			} else if($this->src) { // alternativa...
				$this->image = Application::getMappedPath($this->src);
				unset($this->src);
			}

			#type(din motive de completitudine/whatever is called):
			$this->type = $this->image ? 'image' : 'submit';

			#css class:
			if(!$this->class) {
				$this->class = self::DEFAULT_CSS_CLASS;
			}
			$this->class = htmlspecialchars($this->class);

			#id:
			if(empty($this->id)) { // always have id:
				$form_name = $this->_getParentFormName();
				$this->id = ($form_name && $this->name)? $form_name . '_' . $this->name : $this->name;
			}
			$this->id = $this->_cleanId($this->id);
		}

		public function getHtml()
		{
			$html .= '<input type="' . $this->type . '"';

			if($this->value) {
				$html .= ' value="' . $this->value . '"';
			} else if($this->image) {
				$html .= ' src="' . $this->image . '" alt="' . $this->alt . '"';
			}

			if($this->name) {
				$html .= ' name="' . $this->name . '"';
			}

			if($this->id) {
				$html .= ' id="' . $this->id . '"';
			}

			if($this->class) {
				$html .= ' class="' . $this->class . '"';
			}

			$html .= $this->_getExtraParametersString() . $this->_getShortTagCloser() . "\n";

			return $html;
			#return $this->_showTemplateVars();
		}
	}
