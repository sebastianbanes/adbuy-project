<?php
	
	function urlParse($string) {
		global $config;
		$ret = array();
		$getParams = parse_url($string);
		$params = explode('/', trim($getParams['path'], '/'));
		
		$queryString = '';
		
		if (!empty($getParams['query'])){
			$queryParams = explode('&', $getParams['query']);
			$queryString = '?' . $getParams['query'];
		}
		
		$lang = isset($params[0]) ? $params[0] : null;
		
		if ($lang == Lang::DEFAULT_LANG) {
			Lang::setLang($lang);
			
			unset($params[0]);
			
			$link = implode('/', $params);
			redirect('/' . $link . $queryString);
		}
		
		/* home page without language */
		if ($lang == '') {
			$params[0] = Lang::DEFAULT_LANG;
		}
		
		if (!Lang::valid($params[0])) {
			Lang::setLang(Lang::DEFAULT_LANG);
/*			
			if ($params[0] != Lang::DEFAULT_LANG && Lang::DEFAULT_LANG != Lang::getLang()) {
				$redirectUrl = Lang::getLangLink() . $getParams['path'] . $queryString;
				$redirectCode = 301;
				redirect($redirectUrl, $redirectCode);
			}
*/
		}
		else {
			Lang::setLang($params[0]);
			array_shift($params);
		}
		
		if(empty($params)) {
			$_GET['page'] = 'home';
			
			if (!empty($queryParams)){
				foreach ($queryParams as $val) {
					$tval = explode('=', $val);
					$_GET[$tval[0]] = isset($tval[1]) ? urldecode($tval[1]) : null;
				}
			}
			return;
		}
		
		$ok = 1;
		foreach($params as $parameter) {
			$parts = explode('.', $parameter, 2);
			if(empty($parts[0])) {
				continue;
			}
			
			if ($ok === 1) {
				
				if (substr($parameter,-5) == '.html') {
					$_GET['page'] =  urldecode(substr($parameter, 0, -5));
				} else if (substr($parameter,-4) == '.xml') {
					$_GET['page'] = urldecode(substr($parameter, 0, -4));
				} else {
					$_GET['page'] = urldecode($parameter);
				}
				$_GET['page'] = safeFileName($_GET['page']);
				$ok = 2;
				continue;
			}

			$param_name  = $parts[0];

			$param_value = isset($parts[1]) ? $parts[1] : '';
			
			if ($param_value == 'html') {
				$_GET[] = $param_name;
				continue;
			}
			
			if (empty($param_value)) {
				if (substr($param_name,-5) == '.html'){
					$_GET[] = urldecode(substr($param_name, 0, -5));
				} else {
					$_GET[] = urldecode($param_name);
				}
			} else {
				if (substr($param_value,-5) == '.html'){
					$_GET[$param_name] = urldecode(substr($param_value, 0, -5));
				} else {
					$_GET[$param_name] = urldecode($param_value);
				}
			}
		}

		if (!empty($queryParams)){
			foreach ($queryParams as $val) {
				$tval = explode('=', $val);
				$_GET[urldecode($tval[0])] = isset($tval[1]) ? urldecode($tval[1]) : null;
			}
		}
	}
	
	urlParse($app->getRequestUri());