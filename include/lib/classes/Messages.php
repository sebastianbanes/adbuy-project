<?php

	class Messages 
	{

        const
            AD_TITLE_LENGHT_TO_HIGH     = 'Ad title should have a maximum of 140 chars',
            INVALID_EMAIL               = 'Please enter a valid email address',
            INVALID_USERNAME            = 'Username invalid',
            INVALID_PASSWORD            = 'Invalid password',
            NOT_LOGGED_IN               = 'You do not have access to this page. You should login first.',
            IMAGE_WRONG_FORMAT          = 'Wrong image file type',
            IMAGE_FILE_SIZE_BIG         = 'File size too big',
            CANNOT_POST_AD_NOT_LOGGED   = 'You can not post any ads. You should login first.',
            USER_HAS_NO_PAGES           = 'You can not post any ads. You do not have any pages from where to post!',
            AD_TITLE_EMPTY              = 'Ad title should not be empty!',
            AD_URL_EMPTY                = 'Ad URL should not be empty',
            AD_URL_NOT_VALID            = 'Ad URL is not valid',
            AD_PAGE_ID_NOT_SET          = 'Please select a page',
            AD_PAGE_ID_INVALID          = 'Page selected is invalid',
            AD_BUDGET_EMPTY             = 'Ad budget empty',
            AD_AGE_MIN_EMPTY            = 'Please select min age value',
            AD_AGE_MAX_EMPTY            = 'Please select max age value',
            AD_GEO_EMPTY                = 'Geo location empty',
            AD_TOTAL_BUDGET_EMPTY       = 'Ad budget empty',
            AD_DATERANGE_EMPTY          = 'Please select a date-range',
            AD_DATERANGE_WRONG          = 'Date-range wrong',
            END_DATE_SHOULD_BE_GREATER  = 'End date should be greater than start date',
            END_DATE_IS_PAST            = 'End date should be a date that is in the future',
            AD_BUDGET_SHOULD_BE_NUMERIC = 'Please enter a numeric value for budget',
            AD_BUDGET_SHOULD_BE_GREATER_THAN = 'Ad budget should be greater than',
            AD_BID_TOO_LOW              = 'Ad bit too low',
            AD_WRONG_BID_VALUE          = 'Ad bid value is wrong',
            AD_AGE_MIN_HIGHER_THAN_MAX  = 'Minimum age is higher than maximum age',
            AD_POSTED_SUCCESSFULLY      = 'Your ad was posted successfully.',
            PAYMENT_COMPLETE            = 'Thank you for your payment. Your transaction has been completed, and a receipt for your purchase has been emailed to you. You may log into your account at <a href="http://www.paypal.com" target="_blank">www.paypal.com</a> to view details of this transaction.',
            PAYMENT_HASH_INVALID        = 'Invalid payment hash.',
            PAYPAL_RETURN_URL_WRONG     = 'Wrong Paypal return URL',
            PAYMENT_CANCELLED           = 'Payment cancelled.';
//buyer
#andrew@cupew.com
#cupewcupew

//company
#cupew@cupew.com
#andrewandrew

        public static function getMessages()
		{
			$errors = isset($_SESSION['messages']['errors']) ? $_SESSION['messages']['errors'] : '';
			$notices = isset($_SESSION['messages']['notices']) ? $_SESSION['messages']['notices'] : '';
			unset($_SESSION['messages']);
			
			$ret = '';
			
			if (!empty($errors)){
				$ret .= '<div class="error">';
				
				if (count($errors) > 0){
					foreach ($errors as $value){
						$ret .= $value . '<br />';
					}
				}
				$ret .= '</div>';
			}
			
			if (!empty($notices)){
				$ret .= '<div class="notice">';
				
				if (count($notices) > 0){
					foreach ($notices as $value){
						$ret .= $value . '<br />';
					}
				}
				$ret .= '</div>';
			}
			return $ret;
		}
		
		public static function areMessages()
		{
			return empty($_SESSION['messages']);
		}
		
		public static function areErrors()
		{
			return !empty($_SESSION['messages']['errors']);
		}
		
		public static function areNotices()
		{
			return !empty($_SESSION['messages']['notices']);
		}
		
		public static function deleteErrors()
		{
			unset($_SESSION['messages']['errors']);
		}
		
		public static function deleteNotices()
		{
			unset($_SESSION['messages']['notices']);
		}
		
		public static function deleteMessages()
		{
			unset($_SESSION['messages']);
		}
		
		public static function addNotice($message)
		{
			$_SESSION['messages']['notices'][] = $message;
		}
		
		public static function addError($message)
		{
			if (is_array($message)){
				foreach ($message as $value){
					$_SESSION['messages']['errors'][] = $value;
				}
			} else {
				$_SESSION['messages']['errors'][] = $message;
			}
		}
		
	}