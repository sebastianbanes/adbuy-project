<?php

	class DbSqlManager
	{

		public $host;
		public $database;
		public $user;
		public $password;
		public $charset = 'utf8';

		public $debug = false;

		public $isConnected = false;

		private $record = array();
		private $row;

		private static $linkId  = array();
		private $queryId = null;
		private $className;

		function __construct($query = null)
		{
			$this->className = get_class($this);

			if ($this->className == 'DbSqlManager'){
				trigger_error('This class should be extended', E_USER_WARNING);
			}

			$this->connect();
			if ($this->isConnected)
				$this->query($query);
		}

		public function connect()
		{
			$this->isConnected = true;
			if (!isset(self::$linkId[$this->database . $this->className])) {

				$connection = @mysqli_connect($this->host, $this->user, $this->password, $this->database);

				self::$linkId[$this->database . $this->className] = $connection;

				if (!isset(self::$linkId[$this->database . $this->className])) {
					$this->fatalError('connect(' . $this->host . ', ' . $this->user . ', $Password) failed.');
				}


                mysqli_set_charset(self::$linkId[$this->database . $this->className], $this->charset);

				if (!mysqli_select_db(self::$linkId[$this->database . $this->className], $this->database)) {
					$this->fatalError('cannot use database ' . $this->database);
				}
			}

		}

		public function query($queryString = '')
		{
			if(empty($queryString)) {
				return;
			}

			$this->free();

			$this->queryId = mysqli_query(self::$linkId[$this->database . $this->className], $queryString);
			$this->row   = 0;

			if (!$this->queryId) {
				$this->fatalError('Invalid SQL: ' . $queryString);
			}
		}

		public function realEscape($string = '')
		{
			return mysqli_real_escape_string(self::$linkId[$this->database.$this->className], $string);
		}

		public function nextRecord()
		{
			$this->record = mysqli_fetch_assoc($this->queryId);
			$this->row   += 1;

			return is_array($this->record);
		}

		public function getRecord()
		{
			return $this->record;
		}

		public function insertId()
		{
			return mysqli_insert_id(self::$linkId[$this->database.$this->className]);
		}

		public function free()
		{
			if (is_resource($this->queryId)){
				mysql_free_result($this->queryId);
				$this->queryId = null;
			}
		}

		public function affectedRows()
		{
			return mysql_affected_rows(self::$linkId[$this->database.$this->className]);
		}

		public function numRows()
		{
			return mysql_num_rows(self::$linkId[$this->database.$this->className]);
		}

		public function numFields()
		{
			return mysql_num_fields(self::$linkId[$this->database.$this->className]);
		}

		public function nf()
		{
			return $this->numRows();
		}

		public function f($name)
		{
			return $this->record[$name];
		}

		public function lock($table, $mode = 'write')
		{
			$this->connect();

			$query = "lock tables ";

			if (is_array($table)) {
				while (list($key, $value) = each($table)) {
					if ($key == 'read' && $key != 0) {
						$query .= $value . ' read, ';
					} else {
						$query .= $value . ' ' . $mode . ', ';
					}
				}

				$query = substr($query, 0, -2);
			} else {
				$query .= $table . ' ' . $mode;
			}

			$res = mysql_query($query, self::$linkId[$this->database.$this->className]);

			if (!$res) {
				$this->error('lock(' . $table . ', ' . $mode . ') failed.');
			}
		}

		public function unlock()
		{
			$this->connect();

			$res = mysql_query("unlock tables", self::$linkId[$this->database.$this->className]);

			if (!$res) {
				$this->error('unlock() failed.');
			}
		}

		private function error($msg)
		{
			$error = mysql_error(self::$linkId[$this->database.$this->className]);
			$errno = mysql_errno(self::$linkId[$this->database.$this->className]);

			if($this->debug) {
				printf("<b>Database error:</b> %s<br>\n", $msg);
				printf("<b>MySQL Error</b>: %s (%s)<br>\n",  $errno, $error);
			}
		}

		private function fatalError($msg)
		{
			$error = mysqli_error(self::$linkId[$this->database.$this->className]);
			$errno = mysqli_errno(self::$linkId[$this->database.$this->className]);

			if($this->debug) {
				printf("<b>Database error:</b> %s<br>\n", $msg);
				printf("<b>MySQL Error</b>: %s (%s)<br>\n",  $errno, $error);
				die;
			}


			if ($this->database != $GLOBALS['config']->mysql['stats']->database) {
				Application::fatalError("An Internal Server Error Ocurred. Please report this to the webmaster.");
			} else {
				$this->isConnected = false;
			}

		}

		public function __clone()
		{
			trigger_error('Clone is not allowed.', E_USER_ERROR);
		}

		public function close()
		{
			return mysqli_close(self::$linkId[$this->database.$this->className]);
		}

	}
