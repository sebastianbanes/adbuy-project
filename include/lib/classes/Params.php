<?php

	class Params extends DbSqlManager 
	{

	function __construct($query = null, $db = 'params')
		{
			$conf = $GLOBALS['config']->mysql[$db];

			$this->host = $conf->host;
			$this->database = $conf->database;
			$this->user = $conf->user;
			$this->password = $conf->password;
			$this->debug = $GLOBALS['config']->debug_mode;
			  
			parent::__construct($query);
		}
		
		
		function getTableName()
		{
			return 'params';
		}
		
		function getFields()
		{
			return array('id', 'tpl_hash','name');
		}
	 	
 
	}