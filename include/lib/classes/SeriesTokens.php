<?php

class SeriesTokens extends BaseObjectGeneric {

    protected static $idField = 'email';

    public function getTableName() {
        return 'series_tokens';
    }

    public function getFields() {
        return array('email', 'series_id', 'token');
    }

    public static function add($email) {
        $s = new self($email);

        $token = md5(uniqid(rand(), true));
        $s->token = $token;
        if (!$s->isValid()) {
            $s->email = $email;
            $s->series_id = md5(uniqid(rand(), true));
            $s->token = $token;
            $s->insert();
        } else {
            $s->update();
        }

        return $s;
    }

    public function store() {
        $timestamp = time() + 60 * 60 * 24 * 365;

        setcookie('ua_e', $this->email, $timestamp, '/');
        setcookie('ua_sid', $this->series_id, $timestamp, '/');
        setcookie('ua_t', $this->token, $timestamp, '/');
    }

    public static function fromCookie() {
        if (!self::checkCookie()) {
            return false;
        }
        $s = new self();
        $db = new DbMySql();
        $q = sprintf("SELECT * FROM `%s` WHERE `email` = '%s' AND `series_id` = '%s'",
            $s->getTableName(), $db->realEscape($_COOKIE['ua_e']), $db->realEscape($_COOKIE['ua_sid']));

        $db->query($q);

        if ($db->nextRecord()) {
            if ($db->f('token') != $_COOKIE['ua_t']) {
                return false;
            }

            $s->setRecord($db->getRecord());
            $s->token = md5(uniqid(rand(), true));
            $s->update();
            $s->store();

            return $s->email;
        }
        return false;
    }

    public function cookieFormat() {
        return array(
            'e' => $this->email,
            'sid' => $this->series_id,
            't' => $this->token
        );
    }

    public static function clear() {
        if (!Users::isLogged()) {
            return false;
        }

        $s = new self();
        $q = sprintf("DELETE FROM `%s` WHERE `email` = '%s'", $s->getTableName(), Users::getLoggedUser()->email);
        new DbMySql($q);

        $time = time() - (60 * 60 * 20);
        setcookie('ua_e', '', $time, '/');
        setcookie('ua_sid', '', $time, '/');
        setcookie('ua_t', '', $time, '/');

        return true;
    }

    private static function checkCookie() {
        if (!isset($_COOKIE['ua_e']) || !isset($_COOKIE['ua_sid']) || !isset($_COOKIE['ua_t'])) {
            return false;
        }
        return true;
    }

}