<?php

class Ajax
{
	
	public static function & getSmarty()
	{
		static $smarty = null;
		if (is_null($smarty)){
			$smarty = Application::getSmarty('/ajax/templates/', '/ajax/templates_c/');
		}
		return $smarty;
	}
	
	
	public static function output ($response)
	{
		echo json_encode(array(	'code'	=> 200,
								'data'	=> $response));
		exit;
	}

    public static function outputJSON($data) {
        echo json_encode($data);
        exit;
    }

    public static function out($data, $code = 200)
    {
        if (isset($_GET['dataType']) && $_GET['dataType'] == 'json'){
            if (isset($_GET['callback']) && !empty($_GET['callback'])){
                $data = array('code' => $code, 'data' => $data);
                $data = $_GET['callback'] . '(' . json_encode($data) . ')';
            } else {
                $data = json_encode(array('code' => $code, 'data' => $data));
            }
        }
        echo $data;
        exit;
    }
	
	public static function internalServerError($error = null)
	{
		header('HTTP/1.0 500 Internal Server Error');
		echo json_encode($error);
		exit();
	}
	public static function outputError ($errorMessage, $code = 500)
	{
		echo json_encode(
			array(
				'code'	=> $code,
				'data'	=> $errorMessage
			)
		);
		exit;
	}


    public static function requireLoggedIn()
    {
        if (!Users::isLogged()){
            Ajax::outputError(Messages::NOT_LOGGED_IN);
        }
    }

    public static function requireLoggedInToPost(){
        if (!Users::isLogged()){
            Ajax::outputError(Messages::CANNOT_POST_AD_NOT_LOGGED);
        }
    }
}
