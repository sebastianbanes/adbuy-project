<?php

	class BaseObjectGeneric
	{
		/**
		 * Constructor.
		 * Daca este specificat un $id, se va citi din baza de date recordul asociat cu
		 * acel $id (daca exista unul).
		 *
		 * @param int $id
		 * @return BaseObject;
		 */
		public function __construct($id = null)
		{
			if (empty($id)) {
				$this->emptyRecord();
			} else {
				$this->readId($id);
			}
		}

		/**
		 * Returneaza identificatorul conexiunii setate in config.
		 * Trebuie suprascrisa corezpunzator.
		 *
		 * @return string - numele tabelei
		 */
		public function getDatabase()
		{
			return 'default';
		}

		/**
		 * Returneaza numele tabelei.
		 * Trebuie suprascrisa corezpunzator.
		 *
		 * @return string - numele tabelei
		 */
		public function getTableName()
		{
			return '';
		}

		/**
		 * Returneaza un array cu toate fieldurile folosite.
		 * Trebuie suprascrisa corezpunzator.
		 *
		 * @return array
		 */
		public function getFields()
		{
			return array();
		}

        protected static $idField;

		/**
		 * Seteaza recordul $record in obiectul curent.
		 *
		 * @param array $record
		 */
		public function setRecord($record)
		{
			$fields = $this->getFields();

			foreach ($record as $key => $value) {
				if(in_array($key, $fields)) {
					$this->$key = $value;
				}
			}
		}
		
		public function readField($field, $value)
		{
			$q = new DbMySql(null, $this->getDatabase());
			
			$field = $q->realEscape($field);
			$value = $q->realEscape($value);
			
			$fields = '`' . implode('`,`', $this->getFields()) . '`';

			$query = "SELECT {$fields} FROM `" . $this->getTableName() . "` WHERE `{$field}`  = '{$value}'";
			$q->query($query);
			
			if ($q->nextRecord()) {
				$this->setRecord($q->getRecord());
				return true;
			} else {
				$this->emptyRecord();
				return false;
			}
		}

		/**
		 * Returneaza un array asociativ cu toate fieldurile si valorile.
		 * Cu alte cuvinte, returneaza recordul curent.
		 *
		 * @return array
		 */
		public function getRecord()
		{
			$record = get_object_vars($this);

			$fields = $this->getFields();
			foreach($record as $k => $v) {
				if(!in_array($k, $fields)) {
					unset($record[$k]);
				}
			}

			return $record;
		}

		/**
		 * Seteaza ca null recordul curent (toate fieldurile devin null).
		 *
		 */
		public function emptyRecord()
		{
			$fields = $this->getFields();

			foreach ($fields as $value) {
				$this->$value = null;
			}
		}

		/**
		 * Seteaza in obiectul curent recordul corespunzator unui id din baza de date
		 *
		 * @param int $id
		 */
		public function readId($id)
		{
			$fields = '`' . implode('`,`', $this->getFields()) . '`';
            $idField = static::$idField;

			$query = "SELECT {$fields} FROM `" . $this->getTableName() . "` WHERE `{$idField}`  = '{$id}'";

			$q = new DbMySql($query, $this->getDatabase());

			if ($q->nextRecord()) {
				$this->setRecord($q->getRecord());
			} else {
				$this->emptyRecord();
			}
		}
		
		/**
		 * Seteaza in obiectul curent recordul corespunzator unui "append"
		 *
		 * @param int $id
		 */
		public function read($append)
		{
			$fields = '`' . implode('`,`', $this->getFields()) . '`';

			$query = "SELECT {$fields} FROM `" . $this->getTableName() . "` {$append} LIMIT 1";
			$q = new DbMySql($query, $this->getDatabase());

			if ($q->nextRecord()) {
				$this->setRecord($q->getRecord());
			} else {
				$this->emptyRecord();
			}
		}

		/**
		 * Returneaza valoarea unui camp din baza de date daca acesta exista.
		 * Altfel returneaza null. Unele fielduri vor avea in baza de date valoarea NULL, ceea ce
		 * inseamna ca unele fielduri vor exista, dar vor fi setate cu null.
		 *
		 * @param string $field
		 * @return field
		 */
		public function get($field)
		{
			if (isset($this->$field)) {
				return $this->$field;
			}

			return null;
		}

		/**
		 * Seteaza un field cu o valoare. Doar fieldurile valide (returnate de getFields) sunt setate.
		 *
		 * @param string $field
		 * @param mixed $value
		 */
		public function set($field, $value = null)
		{
			if (in_array($field, $this->getFields())) {
				$this->$field = $value;
			}
		}

		/**
		 * Insereaza o noua inregistrare in baza de date.
		 *
		 * Daca se specifica un $order_field, acesta va fi actualizat cu valoarea id-ului care tocmai a fost inserat.
		 * Scopul este pentru a folosi mai usor ordonarea in tabela, pe baza unui field de tip intreg,
		 * si eventual a unei conditii.
		 *
		 * Dupa executia functiei, $this->id primeste valoarea id-ului care tocmai a fost inserat.
		 *
		 * @param string $order_field
		 *
		 */
		public function insert()
		{
			$fields = $this->getRecord();

			$q = new DbMySql(null, $this->getDatabase());
			$querySet = '';
			foreach ($fields as $key => $value) {
				if(is_null($value)) {
					continue;
				}

				$querySet .= "`{$key}` = '" . $q->realEscape($value) . "', ";
			}
			$querySet = rtrim($querySet, ', ');

			if(!$querySet) {
				return;
			}

			$query = 'INSERT INTO `'. $this->getTableName() . '` SET ' . $querySet;
			$q->query($query);
		}

		/**
		 * Face update la obiectul curent in baza de date.
		 *
		 */
		public function update()
		{
			$fields = $this->getRecord();

			$q = new DbMySql(null, $this->getDatabase());
			$querySet = '';
			foreach ($fields as $key => $value) {
				if(is_null($value)) {
					continue;
				}

				$querySet .= "`{$key}` = '" . $q->realEscape($value) . "', ";
			}
			
			$querySet = rtrim($querySet, ', ');

			if(!$querySet) {
				return;
			}

			$query = "UPDATE `". $this->getTableName() . "` SET {$querySet} WHERE `" . static::$idField . "` = '{$this->{static::$idField}}'";
			$q->query($query);
		}

		/**
		 * Verifica daca exista un id in baza de date.
		 * Functia este statica, deci se poate apela direct: BaseObject::exists($id);
		 *
		 * @param int $id
		 * @return boolean
		 */
		public function exists($id)
		{
			$id = (int) $id;
            $idField = static::$idField;
			$query = "SELECT `{$idField}` FROM `" . $this->getTableName() . "` WHERE `" . static::$idField . "` = '{$this->{static::$idField}}'";
			$q = new DbMySql($query, $this->getDatabase());

			return (bool)$q->nextRecord();
		}

		/**
		 * Returneaza numarul inregistrarilor din baza de date.
		 * Se poate specifica o conditie pentru care se va face numararea.
		 *
		 * @param string $condition
		 * @return int
		 */
		public function getCount($condition = '')
		{
			$query = "SELECT count(*) as total FROM `" . $this->getTableName() . '` ' . $condition;
			$q = new DbMySql($query, $this->getDatabase());

			if ($q->nextRecord()) {
				return $q->f('total');
			}

			return 0;
		}

		/**
		 * Returneaza un array cu obiecte, pentru fiecare record care corespunde cu criteriul specificat.
		 * Se poate specifica un criteriu in parametrul $append, la fel cu alte clauze mysql. Ex:
		 * $all = $this->getAll('WHERE parent_id=2 ORDER BY id DESC LIMIT 3');
		 *
		 * Daca se specifica $fields ca array, obiectele extrase vor avea setate doar fieldurile specificate.
		 *
		 * @param string $condition
		 * @param string $fields
		 * @return array
		 */
		public function getAll($append = '', $fields = null)
		{
			$ret = array();
			if (empty($fields)) {
				$fields = '`' . implode('`, `', $this->getFields()) . '` ';
			} else {
				$fields = static::$idField . ', '. $fields;
			}

			$query = "SELECT $fields FROM " . $this->getTableName() . ' ' . $append;

			$q = new DbMySql($query, $this->getDatabase());
			$className = get_class($this);
			while ($q->nextRecord()) {
				$obj = new $className;
				$obj->setRecord($q->getRecord());
				$ret[] = $obj;
			}

			return $ret;
		}

		/**
		 * Testeaza daca obiectul are un id specificat.
		 *
		 * @return boolean
		 */
		public function isValid()
		{
			return !empty($this->{static::$idField});
		}
		
		public function delete()
		{
			if($this->{static::$idField}){
				$idField = static::$idField;
				$q = new DbMySql(null, $this->getDatabase());
                $id = $q->realEscape($this->{$idField});
                $query = "delete from `" . $this->getTableName() . "` where `{$idField}`='$id'";
                $q->query($query);
				$this->id = null;
			}
		}

		public function now()
		{
			return date("Y-m-d H:i:s");
		}
	
		public function query($query)
		{
			$q = new DbMySql($query, $this->getDatabase());
			$ret = array();
			$className = get_class($this);
			while ($q->nextRecord()){
				$record = $q->getRecord();
				$obj = new $className;
				foreach ($record as $fieldName => $fieldValue){
					$obj->$fieldName = $fieldValue;
				}
				$ret[] = $obj;
			}
			return $ret;
		}
		
		public function scalar($query)
		{
			$q = new DbMySql($query, $this->getDatabase());
			if ($q->nextRecord()) {
				return reset($q->getRecord());
			}
			return null;
		}
	}

