<?php

class SiteSessionHandler {

    /**
     * @var $db DbMySql
     */
    private static $db,
        $instance;

    public $expired = 0,
        $uid = 0;

    public static function init() {
        static::$instance = new self();

        session_set_save_handler(
            array(& static::$instance, 'open'),
            array(& static::$instance, 'close'),
            array(& static::$instance, 'read'),
            array(& static::$instance, 'write'),
            array(& static::$instance, 'destroy'),
            array(& static::$instance, 'gc')
        );
    }

    public function open($savePath, $sessionName) {
        static::$db = new DbMySql();
        return true;
    }

    public function close() {
        return static::$db->close();
    }

    public function read($sessionId) {

        $q = sprintf("SELECT * FROM `sessions` WHERE `session_id` = '%s'", static::$db->realEscape($sessionId));
        static::$db->query($q);
        if (static::$db->nextRecord()) {
            if (intval(static::$db->f('expired'))) {
                session_regenerate_id(true);
            }
            static::$instance->uid = static::$db->f('user_id');
            return static::$db->f('data');
        }
        return '';
    }

    public function write($sessionId, $data) {
        if ($data) {
            $q = sprintf("REPLACE INTO `sessions` (`session_id`, `user_id`, `data`, `expired`) VALUES ('%s', %u, '%s', %u)",
                static::$db->realEscape($sessionId), $this->uid, static::$db->realEscape($data), $this->expired);
        } else {
            $q = sprintf("DELETE FROM `sessions` WHERE `session_id` = '%s' AND `user_id` = %u",
                static::$db->realEscape($sessionId), $this->uid);
        }
        static::$db->query($q);

        return true;
    }

    public function destroy($sessionId) {
        static::$db->query(sprintf("DELETE FROM `sessions` WHERE `session_id` = '%s'", static::$db->realEscape($sessionId)));
        return true;
    }

    public function gc($max) {
        static::$db->query("DELETE FROM `sessions` WHERE `expired` = 1");
        return true;
    }

    public static function isExpired() {
        static::$db->query("SELECT `expired` FROM `sessions` WHERE `session_id` = '" . session_id() . "'");
        if (static::$db->nextRecord()) {
            return intval(static::$db->f('expired'));
        }
        return true;
    }

    public static function expireUserSessions($uid, $exclude = null, $currentSession = false) {
        $andExclude = '';

        self::$instance->expired = true;

        if ($exclude) {
            if ($exclude == session_id()) {
                self::$instance->expired = false;
            }
            $andExclude = sprintf(" AND `session_id` != '%s'", $exclude);
        }

        $andInclude = '';
        if ($currentSession) {
            $andInclude = " AND `session_id` = '" . session_id() . "'";
        }

        static::$db->query(sprintf("UPDATE `sessions` SET `expired` = 1 WHERE `user_id` = %u AND `expired` = 0%s%s",
            static::$instance->uid, $andExclude, $andInclude));
    }

    public static function setUserId($uid) {
        self::$instance->uid = $uid;
    }

}