<?php

	class Admin
	{
		var $id;
		var $email;
		var $password;
		var $last_login;

		/**
		 * Constructor. Daca se specifica un id, se va citi datele adminului cu id-ul respectiv.
		 *
		 * @return Admin
		 */
		function Admin($id = null)
		{
			if(!is_null($id)) {
				$this->readId($id);
			}
		}

		/**
		 * Citeste/seteaza param. de configurare ai clasei in functie de var. din $config-ul creat in init.php.
		 * Trebuie apelata static inaintea oricarei alte functii din clasa.
		 *
		 * @param StdObject $conf
		 * @return StdObject
		 */
		function config($conf = null)
		{
			static $config = null;

			if(!is_null($conf)) {
				$config = $conf;
			}

			return $config;
		}

		/**
		 * Se citeste datele adminului cu id-ul specificat.
		 * Daca nu se gaseste un astfel de admin, fieldurile vor deveni null.
		 *
		 * @param integer $id
		 */
		function readId($id)
		{
			$config = Admin::config();

			$id = intval($id);
			$sql = "SELECT * FROM `{$config->table}` WHERE id = $id";
			$db = new DbMySql($sql);
			$db->next_record();

			$this->id = $db->f('id');
			$this->email = $db->f('email');
			$this->password = $db->f('password');
		}


		/**
		 * Executa operatia de login pentru un username($email) si o parola($password).
		 * Daca operatia reuseste, adminul citit este salvat in sesiune si se returneaza true.
		 * Altfel returneaza false.
		 *
		 * In functie de setari, operatia de login poate actualiza field-ul last_login si chiar sa salveze incercarile de login
		 * intr-o alta tabela.
		 *
		 * Se poate apela static.
		 *
		 * @param string $email
		 * @param string $password
		 * @return boolean
		 */
		function login($email, $password)
		{
			$config = Admin::config();

			#filtrarea datelor!
			#1' OR 1 =1#
			$email = addslashes($email);
			$password = md5(addslashes($password));

			$sql = "SELECT * FROM `{$config->table}` WHERE `{$config->username_field}` = '$email' AND `{$config->password_field}` = '$password' ";
			#deg($sql); die;
			$db = new DbMySql($sql);

			$admin_id = 0;

			if($db->nextRecord())
			{
				// exista user, password
				$user = new Admin;
				$user->id = $db->f('id');
				$user->email = $db->f('email');
				$user->password = $db->f('password');
				if($config->save_last_login) {
					$user->last_login = $db->f('last_login');
				}

				Admin::setLoggedUser($user);

				if($config->save_last_login) {
					$sql = "UPDATE `{$config->table}` SET `{$config->last_login_field}` = now() WHERE id = " . $user->id;
					$db->query($sql);
				}

				$admin_id = $user->id;
			}

			if($config->track_login_attempts) {
				// admin login table
				$user_agent = $_SERVER["HTTP_USER_AGENT"];
				$ip = $_SERVER["REMOTE_ADDR"];

				$sql = "INSERT INTO `{$config->login_attempts_table}` (admin_id, user_agent, ip, login_date) VALUES ($admin_id, '$user_agent', '$ip', now())";
				$db->query($sql);
			}

			return Admin::isLogged();
		}

		/**
		 * Returneaza true daca exista un admin logat curent.
		 *
		 * Se poate apela static.
		 *
		 * @return boolean
		 */
		function isLogged()
		{
			$config = Admin::config();
			return !empty($_SESSION[$config->session_var]);
		}


		/**
		 * Returneaza adminul logat, daca exista unul.
		 * Se poate apela static.
		 *
		 * @return Admin
		 */
		function getLoggedUser()
		{
			$config = Admin::config();

			if(empty($_SESSION[$config->session_var])) {
				return null;
			}

			return $_SESSION[$config->session_var];
		}

		/**
		 * Folosit pentru a seta un admin ca fiind cel logat curent.
		 * Este apelata automat in functia login().
		 *
		 * Poate fi apelata pentru a re-actualiza modificarile facute unui admin, si
		 * a se reflecta si in sesiune.
		 *
		 * @param Admin $user
		 */
		function setLoggedUser($user)
		{
			$config = Admin::config();
			$_SESSION[$config->session_var] = $user;
		}

		/**
		 * Executa operatia de logout pentru adminul curent logat,
		 * daca exista unul.
		 *
		 */
		function logout()
		{
			$config = Admin::config();
			unset($_SESSION[$config->session_var]);
		}
	}
