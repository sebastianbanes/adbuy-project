<?php

    /** scoate ./\ din fata stringului
     *
     * @param $fname string
     * @return string
     */
    function safeFileName($fname)
    {
        return ltrim($fname, './\\');
    }


/**
	 * Face redirect la locatia specificata in $url.
	 * Daca este specificat un status redirectul se va face corespunzator.
	 * Doar 301 si 302 sunt permise pt status.
	 *
	 * @param string $url
	 * @param integer $status
	 */
	function redirect($url, $status = null)
	{
		if(!is_null($status)) {
			if($status == 301) {
				header("HTTP/1.1 301 Moved Permanently");
			} else if($status == 302){
				header("HTTP/1.1 302 Moved Temporarily");
			}
		}

		header("Location: {$url}");
		die;
	}

    /**
     * Extrage extensia unui fisier din numele(sau path-ul)acestuia.
     *
     * @param string $filename
     * @return string
     */
    function file_extension($filename)
    {
        return strtolower(substr(strrchr($filename, '.'), 1));
    }

	/**
	 * Forteaza un redirect la adresa specificata, prin diverse metode.
	 * Daca nu s-a facut inca output, se face redirect HTTP.
	 * Altfel se face redirect prin meta-refresh si/sau javascript.
	 *
	 * E doar o metoda de a te impusca singur in picior.
	 *
	 * @deprecated
	 *
	 * @param string $url
	 */
	function force_redirect($url)
	{
		if(headers_sent()) {
			echo '<META HTTP-EQUIV="refresh" content="0;URL='.htmlentities(urlencode($url)).'">';
			echo '<SCRIPT TYPE="text/javascript"> location.href="'.urlencode($url).'"; </SCRIPT>';
			die;
		} else {
			header("Location: {$url}");
			die;
		}
	}

	/**
	 * Face un redirect HTTP la url-ul indicat.
	 * Este metoda recomandata de a face redirecturi.
	 *
	 * Daca s-a facut output deja, se va afisa un warning.
	 *
	 * @param string $link
	 */
	function jump($link)
	{
		header('Location: '.$link);
		session_write_close();
		die;
	}

	/**
	 * Face output la o variabila intr-un tag html PRE,
	 * pentru a fi mai lizibil outputul.
	 *
	 * @param mixed $var
	 */
	function deg($var)
	{
		if(func_num_args() > 1) $var = func_get_args();

		$trace = array_shift(debug_backtrace());
		$file = $trace['file'];
		$line = $trace['line'];
		$rows = file($file);

		#meta:
		$output = "<b>Debugging <font color='red'>" . basename($file) . 
					"</font> on line <font color='red'>{$line}</font></b>:\r\n";
		$output .= "<div style='background:#f5f5f5;padding:0.2em 0em'>" . 
					htmlspecialchars($rows[$line-1]) . "</div>\r\n";

		#type:
		$output .= '<b>Type</b>: ' . gettype($var) . "\r\n";
		if(is_string($var)) {
			$output .= "<b>Length</b>: " . strlen($var) . "\r\n";
		} else if(is_array($var)) {
			$output .= "<b>Length</b>: " . count($var) . "\r\n";
		}

		#value:
		$output .= '<b>Value</b>: ';
	    if (is_scalar($var)) {
			$output .= htmlspecialchars($var);
		} else {
			$print_r = print_r($var, true);
			$print_r = str_replace(array('<', '>'), array('&lt;', '&gt;'), $print_r);

	        $output .= '<pre>' . $print_r . "</pre>\n";
	    }

		echo $output;
	}

	/**
	 * Face output la o variabila intr-un tag html PRE,
	 * apoi opreste executia scriptului.
	 *
	 * @param mixed $var
	 */
	function out($var)
	{
		deg($var);
		die;
	}

	function getFacebookLoginUrl() {

		global $config;

		require_once APPLICATION_PATH . '/include/lib/faceBook/autoload.php';

		$fb = new Facebook\Facebook(array(
				'app_id' => $config->facebook->appId,
				'app_secret' => $config->facebook->secret,
				'default_graph_version' => $config->facebook->graphVersion,
		));

		$helper = $fb->getRedirectLoginHelper();
		$permissions = array('email'); // Optional permissions
		return $helper->getLoginUrl('http://' . $_SERVER['HTTP_HOST'] . '/fb-callback', $permissions);

	}

    function getMySqlDateFromDatePickerFormat($datePickerDate){
        return date('Y-m-d', strtotime(str_replace('-', '/', $datePickerDate)));
    }


	function getDateMysql($timestamp = null) {

		if (empty($timestamp)) {
			$timestamp = time();
		}

		return date('Y-m-d H:i:s', $timestamp);
	}

	function pageNotFound() {
		global $smarty;

		if (empty($smarty)) {
			$smarty = Application::getSmarty('/templates/', '/templates_c/');
		}

		header("HTTP/1.1 404 Not Found");
		$smarty->assign('CONTENT', 'error_not_found.tpl');
		$smarty->display('index.tpl');
	}


    function valid_url($website)
    {
        if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
           return false;
        }

        return true;
    }
//
//    function shutDownFunction() {
//        $error = error_get_last();
//        // fatal error, E_ERROR === 1
//        if ($error['type'] === E_ERROR) {
//            jump('/error.html');
//            global $smarty;
//            $smarty->assign('fullError', $error['message']);
//            $smarty->assign('CONTENT', 'error.tpl');
//            $smarty->display('index.tpl');
//        }
//    }
//    register_shutdown_function('shutDownFunction');