<?php

	class ErrorHandler 
	{
		var $debugMode = false;

		var $errorLogging;
		var $logFilename;

		var $logBuffer = '';

		function ErrorHandler($config = null)
		{
			if(is_null($config) && !is_object($config)) {
				return;
			}

			$this->debugMode = $config->debug_mode;

			if(isset($config->error_log)) {
				$this->logFilename = $config->absolute_path . '/' . $config->error_log;

				if(isset($config->error_levels)) {
					$this->errorLogging = $config->error_levels;
				} else {
					$this->errorLogging = E_ALL;
				}
			}
		}

		function register()
		{
			//ini_set('html_errors', 0);
			set_error_handler(array(&$this, 'onError'));

			if($this->logFilename) {
				register_shutdown_function(array(&$this, 'saveLog'));
			}
		}

		function onError($errno, $errmsg, $filename, $linenum, $vars)
		{
			#erorile stricte le ignoram:
			$is_strict = ($errno == 2048);  // 2048 = E_STRICT, PHP5 specific
			if($is_strict) {
				return;
			}

			#afisam separat erorile fatale de cele obisnuite:
			$is_fatal = in_array($errno, array(E_ERROR, E_PARSE, E_CORE_ERROR, E_COMPILE_ERROR));
			if($is_fatal) {
				$this->_showFatalError($errno, $errmsg, $filename, $linenum, $vars);
			} else {
				$this->_showRegularError($errno, $errmsg, $filename, $linenum);
			}

			#logam erorile, daca este cazul:
			$this->_log($errno, $errmsg, $filename, $linenum);

			//return false;
		}

		function saveLog()
		{
			if(!empty($this->logBuffer) && !empty($this->logFilename)) {
				$errfile = fopen($this->logFilename, 'a');
				if($errfile) {
					fwrite($errfile, $this->logBuffer);
					fclose($errfile);
					chmod($this->logFilename, 0777);
				}
			}
		}

		function parseLog()
		{
			ini_set('auto_detect_line_endings', 'on'); //mac compatibility...

			$pattern = "@\[(.+)\]\[(.+): (\d+)\] - (.+)@";
			#$pattern = "@\s?\[(.+)\]\[(.+): (\d+)\] - ([^\]]+)@";

			$lines = file($this->logFilename);
			$errors = array();

			foreach($lines as $line) {
				preg_match($pattern, $line, $matches);

				$error['time'] = $matches[1];
				$error['file'] = $matches[2];
				$error['line'] = $matches[3];
				$error['msg'] = $matches[4];

				$errors[] = $error;
				unset($matches);
			}

			return $errors;
		}

		function _showFatalError($errno, $errmsg, $filename, $linenum, $vars)
		{
			if($this->debugMode) {

				global $config;
				$relative_filename = str_replace('\\', '/', $filename);
				$relative_filename = str_replace($config->absolute_path, '', $relative_filename);

				$html  = "<h1>A fatal error occured</h1>\n";
				$html .= '<div style="padding:3px;border:1px solid;margin:5px">' . 
					'<div style="padding:5px;border-bottom:1px solid"><b>' . $errmsg . '</b> in ' . 
					$relative_filename . ' (' . $linenum . ')</div>' .
					$this->_getSource($filename, $linenum) . 
					'</div>';

				$html .= '<h3>Bad Code:</h3><div>' . $this->_getSource($filename, $linenum) . '</div>';

				$html .= '<p>&nbsp;</p>';
				$html .= '<b>Scope vars:</b>:<pre>' . print_r($vars, true) . '</pre>';
				$html .= '<p>&nbsp;</p>';
			} else {
				$html = defined('MSG_FATAL_ERROR') ? MSG_FATAL_ERROR :
					'A fatal error has occured. Script execution has been aborted.';
			}

			Application::fatalError($html);
		}

		function _showRegularError($errno, $errmsg, $filename, $linenum)
		{
			if(!$this->debugMode) {
				return;
			}

			$error_reporting = error_reporting() & $errno;
			if(!$error_reporting) {
				return;
			}

			//$time = isset($_SERVER['REQUEST_TIME']) ? $_SERVER['REQUEST_TIME'] : time();

			global $config;
			$relative_filename = str_replace('\\', '/', $filename);
			$relative_filename = str_replace($config->absolute_path, '', $relative_filename);

			$html = '<div style="padding:3px;border:1px solid;margin:5px">' . 
					'<div style="padding:5px;border-bottom:1px solid"><b>' . $errmsg . '</b> in ' . 
					$relative_filename . ' (' . $linenum . ')</div>' .
					$this->_getSource($filename, $linenum) . 
					'</div>';

			echo $html;
		}

		function _getSource($file, $line, $next = 5, $prev = 5)
		{
			/*
			if (!(file_exists($file) && is_file($file))) {
				trigger_error("showSource() failed, file does not exist `$file`", E_USER_ERROR);
				return false;
			}
			*/

		    //read code
		    $data = highlight_string(file_get_contents($file), true);

		    //seperate lines
		    $data  = explode('<br />', $data);
		    $count = count($data) - 1;

		    //count which lines to display
		    $start = $line - $prev;
		    if ($start < 1) {
		        $start = 1;
		    }
		    $end = $line + $next;
		    if ($end > $count) {
		        $end = $count + 1;
		    }

			// result:
			$ret  = '<table cellspacing="0" cellpadding="0"><tr>';
			$ret .= '<td style="vertical-align: top;"><code style="background-color: #FFFFCC; color: #666666;">';

		    for ($x = $start; $x <= $end; $x++) {
		        $ret .= '<a name="'.$x.'"></a>';
		        $ret .= ($line == $x ? '<font style="background-color:red; color: white;">' : '');
		        $ret .= str_repeat('&nbsp;', (strlen($end) - strlen($x)) + 1);
		        $ret .= $x;
		        $ret .= '&nbsp;';
		        $ret .= ($line == $x ? '</font>' : '');
		        $ret .= '<br />';
		    }
			$ret .= '</code></td><td style="vertical-align: top;"><code>';

		    while ($start <= $end) {
		        $ret .= '&nbsp;' . $data[$start - 1] . '<br />';
		        ++$start;
		    }
			$ret .= '</code></td>';
			$ret .= '</tr></table>';

			return $ret;
    	}

		function _log($errno, $errmsg, $filename, $linenum) 
		{
			if($this->errorLogging & $errno){ // operatori pe bitzi!!!
				$time = isset($_SERVER['REQUEST_TIME']) ? $_SERVER['REQUEST_TIME'] : date("Y-m-d H:i:s");
				$this->logBuffer .= "[$time][$filename: $linenum] - $errmsg\n";
			}
		}
	}
