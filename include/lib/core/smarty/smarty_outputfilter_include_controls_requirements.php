<?php

	function smarty_outputfilter_include_controls_requirements($tpl_output, &$smarty)
	{
		$code = "\n<!-- auto-included resources: -->";

		$tags = SmartyTag::$tags_ref;
		foreach($tags as $class_name => $tag_list) {
			$tag = $tag_list[0]; // chemam requirements doar pentru primul field...

			$requirements = $tag->getRequirements();
			if($requirements) {
				$code .= "\n{$requirements}\n";
			}
		}

		$code .= "<!-- end auto-included resources: -->\n";

		# insert code in HTML:
		$parts = preg_split('#</head>#i', $tpl_output, 2);
		if(!isset($parts[1])) {
			# nu are 2 elem, deci nu contine </head>...(template inclus? returnam outputu direct)
			return $tpl_output;
		}

		list($html_before, $html_after) = $parts;

		$tpl_output = $html_before . $code . "</head>" . $html_after;

		return $tpl_output;
	}
