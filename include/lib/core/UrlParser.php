<?php

	class UrlParser {

		var $varSeparator = '/';
		var $varEqual = '.';

		function parse($string) {
			$ret = array();

			$params = explode($this->varSeparator, $string);

			if(empty($params)) {
				return array();
			}

			foreach($params as $parameter) {
				$parts = explode($this->varEqual, $parameter, 2);

				if(empty($parts[0])) {
					continue;
				}

				$param_name  = $parts[0];
				if(isset($param_name{0}) && $param_name{0} == '?') {
					// nu extrage var. din adevaratul query string, daca este setat...
					continue;
				}

				$param_value = isset($parts[1]) ? $parts[1] : '';

				$ret[$param_name] = $param_value;
			}

			return $ret;
		}
	}
