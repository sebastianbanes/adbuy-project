<?php

	class DB_Sql 
	{

		var $Host;
		var $Database;
		var $User;
		var $Password;

		var $Debug = false;

		var $Record = array();
		var $Row;

		var $Link_ID  = null;
		var $Query_ID = null;

		function DB_Sql($query = '')
		{
			$conn = $GLOBALS['config']->mysql;
			$this->Host = $conn->host;
			$this->Database = $conn->database;
			$this->User = $conn->user;
			$this->Password = $conn->password;

			$this->Debug = (bool)(@$GLOBALS['config']->debug_mode);

			$this->query($query);
		}

		function connect($Database = '', $Host = '', $User = '', $Password = '') 
		{

			if ('' == $Database) {
			  $Database = $this->Database;
			}

			if ('' == $Host) {
			  $Host = $this->Host;
			}

			if ('' == $User) {
			  $User = $this->User;
			}

			if ('' == $Password) {
			  $Password = $this->Password;
			}

			if (is_null($this->Link_ID)) {
				$this->Link_ID = mysql_connect($Host, $User, $Password);
				if (!$this->Link_ID) {
					$this->halt("connect($Host, $User, \$Password) failed.");
				}

				if (!@mysql_select_db($Database, $this->Link_ID)) {
					$this->halt("cannot use database ".$this->Database);
				}
			}
		}

		function free()
		{
		  @mysql_free_result($this->Query_ID);
		  $this->Query_ID = null;
		}

		function query($Query_String)
		{
			#echo "<br>$Query_String\n";

			if($Query_String == '') {
				/* No empty queries, please, since PHP4 chokes on them. */
				return;
			}

			$this->connect();

			# New query, discard previous result.
			if ($this->Query_ID) {
				$this->free();
			}

			$this->Query_ID = @mysql_query($Query_String, $this->Link_ID);
			$this->Row   = 0;

			if (!$this->Query_ID) {
				$this->halt("Invalid SQL: ".$Query_String);
			}
		}

		function next_record() 
		{
			if (!$this->Query_ID) {
			  $this->halt("next_record called with no query pending.");
			}

			$this->Record = @mysql_fetch_assoc($this->Query_ID);
			$this->Row   += 1;

			return is_array($this->Record);
		}

		function getRecord()
		{
			return $this->Record;
		}

		function lock($table, $mode = 'write') {
			$this->connect();

			$query = "lock tables ";

			if (is_array($table)) {
				while (list($key, $value) = each($table)) {
					if ($key == "read" && $key != 0) {
						$query .= "$value read, ";
					} else {
						$query .= "$value $mode, ";
					}
				}

				$query = substr($query, 0, -2);
			} else {
				$query .= "$table $mode";
			}

			$res = @mysql_query($query, $this->Link_ID);

			if (!$res) {
				$this->halt("lock($table, $mode) failed.");
			}
		}

		function unlock() {
			$this->connect();

			$res = @mysql_query("unlock tables", $this->Link_ID);

			if (!$res) {
				$this->halt("unlock() failed.");
			}
		}

		function insert_id()
		{
			return mysql_insert_id($this->Link_ID);
		}

		function affected_rows()
		{
			return @mysql_affected_rows($this->Link_ID);
		}

		function num_rows()
		{
			return @mysql_num_rows($this->Query_ID);
		}

		function num_fields() 
		{
			return @mysql_num_fields($this->Query_ID);
		}

		function nf() 
		{
			return $this->num_rows();
		}

		function np() 
		{
			print $this->num_rows();
		}

		function f($Name) 
		{
			return @$this->Record[$Name];
		}

		function p($Name) 
		{
			print $this->Record[$Name];
		}

		function halt($msg) 
		{
			$Error = @mysql_error($this->Link_ID);
			$Errno = @mysql_errno($this->Link_ID);

			if($this->Debug) {
				printf("</td></tr></table><b>Database error:</b> %s<br>\n", $msg);
				printf("<b>MySQL Error</b>: %s (%s)<br>\n",  $Errno, $Error);

				die;
			}

			Application::fatalError("An Internal Server Error Ocurred. Please report this to the webmaster.");
		}
	}
