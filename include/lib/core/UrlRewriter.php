<?php

	/**
	 * Clasa se ocupa de rescrierea linkurilor, si este capabila sa parseze
	 * outputul pt. a rescrie linkurile existente;
	 *
	 * De asemenea stie sa extraga corespunzator variabilele din url-urile rescrise,
	 * facand transparent procesul de rescriere.
	 *
	 */
	class UrlRewriter
	{
		var $part_separator = '/';
		var $var_separator = '.';

		var $url_page_var;
		var $url_action_var;
		var $url_language_var;

		var $default_page;
		var $default_action;
		var $default_lang;

		var $use_language_urls;

		var $absolute_url;

		/**
		 * Constructorul, citeste din configul principal parametrii relevanti pt rescrierea linkurilor.
		 *
		 * @param Config $config
		 * @return UrlRewriter
		 *
		 */
		function UrlRewriter($config)
		{

			$this->url_page_var = $config->url_page_var;
			$this->url_action_var = $config->url_action_var;
			$this->url_language_var = $config->language_var;

			$this->default_page = $config->default_page;
			$this->default_action = $config->default_action;
			$this->default_lang = empty($config->default_language) ? 
									reset($config->languages) :
									$config->default_language;

			$this->absolute_url = rtrim($config->absolute_url, '/').'/';

			$this->use_language_urls = (count($config->languages) > 1);
		}

		/**
		 * Extrage variabilele parsate din url-ul primit ca parametru in $_GET si $_REQUEST.
		 * Daca variabila exista deja, NU va fi suprascrisa.
		 *
		 * @param string $url
		 */
		function extractVars($url)
		{
			$url = parse_url($url);

			if(empty($url['path']) && empty($url['query'])) {
				#no path:
				UrlRewriter::_registerGetVariable($this->url_page_var, $this->default_page);
				UrlRewriter::_registerGetVariable($this->url_action_var, $this->default_action);
				if($this->use_language_urls) {
					UrlRewriter::_registerGetVariable($this->url_language_var, $this->default_lang);
				}

				return;
			}

			if(isset($url['query'])) {
				$qs = $url['query'];
			}

			#extrage partile componente:
			#$pattern = '#^/([^/]*)/?(.*)$#i';
			#preg_match($pattern, $qs, $matches);
			#$page = $matches[1];
			#$rest = $matches[2];

			#sterge part separatorul de la sfarsit:
			$qs = rtrim($url['path'], $this->part_separator);

			# '/' -> face parte din path, asa ca il stergem daca exista(primul caracter):

			if($qs && $qs[0] == '/') {
				$qs = substr($qs, 1);
			}

			$parts = explode($this->part_separator, $qs);

			if($parts === false) {
				#empty string:
				UrlRewriter::_registerGetVariable($this->url_page_var, $this->default_page);
				UrlRewriter::_registerGetVariable($this->url_action_var, $this->default_action);
				if($this->use_language_urls) {
					UrlRewriter::_registerGetVariable($this->url_language_var, $this->default_lang);
				}

				return;
			}

			$lang = null;
			$page = null;
			$action = null;

			if($this->use_language_urls) {
				if(isset($parts[0])) {
					$lang = $parts[0];
				}

				if(isset($parts[1])) {
					$page = $parts[1];
				}

				if(isset($parts[2])) {
					$action = $parts[2];
				}
			} else {
				if(isset($parts[0])) {
					$page = $parts[0];
				}

				if(isset($parts[1])) {
					$action = $parts[1];
				}
			}

			if(empty($lang)) {
				$lang = $this->default_lang;
			}

			if(empty($page)) {
				$page = $this->default_page;
			}

			if(empty($action)) {
				$action = $this->default_action;
			}

			UrlRewriter::_registerGetVariable($this->url_page_var, $page);
			UrlRewriter::_registerGetVariable($this->url_action_var, $action);
			if($this->use_language_urls) {
				UrlRewriter::_registerGetVariable($this->url_language_var, $lang);
			}

			#stergem lang, page si action -urile (primele 2-3 var):
			unset($parts[1], $parts[0]);
			if($this->use_language_urls) {
				unset($parts[2]);
			}

			#extragem variabilele:
			foreach($parts as $var) {
				@list($name, $value) = explode($this->var_separator, $var);
				if($name) {
					UrlRewriter::_registerGetVariable($name, $value);
				}
			}
		}

		/**
		 * Functie ajutatoare pt extragerea variabilelor: extrage o singura variabila.
		 *
		 * @access private
		 * @param string $name
		 * @param mixed $value
		 *
		 */
		function _registerGetVariable($name, $value)
		{
			//stop();
			if (!empty($value) && $value != 'html'){
				$_GET[$name] = $value;
			} else {
				$_GET[] = $name;
			}
			if ($value == 'html') {
				$_GET['htmlInPath'] = true;
			}
		}
	}
