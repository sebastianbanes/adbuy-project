<?php

	class UrlMapper 
	{
		var $patterns;

		var $parametersVariableName = 'PARAMS';
		var $urlParserFunction = null;

		function UrlMapper()
		{
			$this->resetPatterns();
		}

		function resetPatterns()
		{
			$this->patterns = array();
		}

		function getPatterns()
		{
			return $this->patterns;
		}

		function set($pattern, $params)
		{
			$map['pattern'] = $pattern;
			$map['params'] = $params;

			$this->patterns[] = $map;
		}

		function setParametersVariableName($name) {
			$this->parametersVariableName = (string)$name;
		}

		function getParametersVariableName() {
			return $this->parametersVariableName;
		}

		function setUrlParserFunction($function) {
			$this->urlParserFunction = $function;
		}

		function getUrlParserFunction() {
			return $this->urlParserFunction;
		}

		function execute($request_uri)
		{
			$matches = array();
			$url = '';

			foreach($this->patterns as $map) {
				$pattern = '%^' . $map['pattern'] . '$%i';

				if(preg_match($pattern, $request_uri, $matches)) {
					#deg($matches);
					$url = $map['params'];
					break;
				}
			}

			# stergem prima inregistrare, pt. ca nu e palpitanta:
			unset($matches[0]);

			# daca nu exista match-uri sau parametrii ca sa ii inlocuim, iesm instant:
			if(empty($matches) || empty($url)) {
				return;
			}

			#parsam url-ul:
			$url_parts = parse_url($url);

			# daca nu exista parametrii de 'extras', iesim brusc instant si dintro data:
			$query_string = '';
			if(empty($url_parts['query'])) {
				if(empty($url_parts['path'])) {
					return;
				}

				#daca nu se specifica query string, parsam pathul:
				$query_string = $url_parts['path'];
			} else {
				$query_string = $url_parts['query'];
			}

			#parsam query stringul:
			parse_str($query_string, $params);

			#daca nu avem paramerii, iesim:
			if(empty($params)) {
				return;
			}

			#parcurgem parametrii si luam valorile echivalente din $matches:
			foreach($params as $param_name => $param_nr) {
				$param_nr = trim($param_nr, "{}");
				$param_value = isset($matches[$param_nr]) ? $matches[$param_nr] : null;

				$params[$param_name] = $param_value;
			}

			# parametrii speciali (daca exista, ii scoatem, si ii extragem ca string):
			$unspecified_params_str = '';
			$unspecified_params_name = '{' . $this->parametersVariableName . '}';

			if(isset($params[$unspecified_params_name])) {
				$unspecified_params_str = $params[$unspecified_params_name];
				unset($params[$unspecified_params_name]);
			}

			# parsam stringu de parametrii speciali, folosind parseur-ul setat, daca exista unul:
			$unspecified_params = array();
			if(!empty($this->urlParserFunction) && is_callable($this->urlParserFunction)) {
				$unspecified_params = (array) call_user_func($this->urlParserFunction, $unspecified_params_str);
			}

			# unim array-urile intr-un singur array, astfel incat parametrii nespecificati sa fie 
			# suprascrisi de cei specificati, daca apar conflicte:
			$params = array_merge($unspecified_params, $params);

			# finally extragem in super-globale toti parametrii parsati (specificati sau nu in regexpuri)
			foreach($params as $param_name => $param_value) {
				UrlMapper::_registerVar($param_name, $param_value);
			}
		}

		function _registerVar($name, $value = null)
		{
			$_GET[$name] = $value;

			if(!isset($_POST[$name]) && !isset($_COOKIE[$name])) {
				$_REQUEST[$name] = $value;
			}
		}
	}
