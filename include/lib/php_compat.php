<?php

	#constante:
	if (!defined('DIRECTORY_SEPARATOR')) {
		define('DIRECTORY_SEPARATOR',
			strtoupper(substr(PHP_OS, 0, 3) == 'WIN') ? '\\' : '/'
		);
	}
	
	if (!defined('PATH_SEPARATOR')) {
		define('PATH_SEPARATOR',
			strtoupper(substr(PHP_OS, 0, 3) == 'WIN') ? ';' : ':'
		);
	}

	if (!defined('PHP_EOL')) {
		switch (strtoupper(substr(PHP_OS, 0, 3))) {
			// Windows
			case 'WIN':
				define('PHP_EOL', "\r\n");
				break;

			// Mac
			case 'DAR':
				define('PHP_EOL', "\r");
				break;

			// Unix
			default:
				define('PHP_EOL', "\n");
		}
	}

	if (!defined('UPLOAD_ERR_OK')) {
		define('UPLOAD_ERR_OK', 0);
	}

	if (!defined('UPLOAD_ERR_INI_SIZE')) {
		define('UPLOAD_ERR_INI_SIZE', 1);
	}

	if (!defined('UPLOAD_ERR_FORM_SIZE')) {
		define('UPLOAD_ERR_FORM_SIZE', 2);
	}

	if (!defined('UPLOAD_ERR_PARTIAL')) {
		define('UPLOAD_ERR_PARTIAL', 3);
	}

	if (!defined('UPLOAD_ERR_NO_FILE')) {
		define('UPLOAD_ERR_NO_FILE', 4);
	}


	if (!defined('ENT_NOQUOTES')) {
		define('ENT_NOQUOTES', 0);
	}

	if (!defined('ENT_COMPAT')) {
		define('ENT_COMPAT', 2);
	}

	if (!defined('ENT_QUOTES')) {
		define('ENT_QUOTES', 3);
	}

	#functii:
	if (!function_exists('array_diff_key')) {

		function array_diff_key() 
		{
			$argCount   = func_num_args();
			$argValues  = func_get_args();
			$valuesDiff = array();

			if ($argCount < 2) {
				return false;
			}

			foreach ($argValues as $argParam) {
				if (!is_array($argParam)) {
					return false;
				}
			}

			foreach ($argValues[0] as $valueKey => $valueData) {
				for ($i = 1; $i < $argCount; $i++) {
					if (isset($argValues[$i][$valueKey])) {
						continue 2;
					}
				}

				$valuesDiff[$valueKey] = $valueData;
			}

			return $valuesDiff;
		}
	}

	if (!function_exists('checkdnsrr')) {
    	function checkdnsrr($host, $type = '') {
    		if(!empty($host)) {
    			if($type == '') $type = "MX";
    			@exec("nslookup -type=$type $host", $output);
    			while(list($k, $line) = each($output)) {
    				if(eregi("^$host", $line)) {
    					return true;
    				}
    			}
    			return false;
    		}
    	}
	 }

	if (!function_exists('html_entity_decode')) {
		function html_entity_decode($string, $quote_style = ENT_COMPAT, $charset = null)
		{
			if (!is_int($quote_style)) {
				user_error('html_entity_decode() expects parameter 2 to be long, ' .
					gettype($quote_style) . ' given', E_USER_WARNING);
				return;
			}

			$trans_tbl = get_html_translation_table(HTML_ENTITIES);
			$trans_tbl = array_flip($trans_tbl);

			// Add single quote to translation table;
			$trans_tbl['&#039;'] = '\'';

			// Not translating double quotes
			if ($quote_style & ENT_NOQUOTES) {
				// Remove double quote from translation table
				unset($trans_tbl['&quot;']);
			}

			return strtr($string, $trans_tbl);
		}
	}

	if(!function_exists('http_build_query')) {
		function http_build_query($data, $prefix = null, $sep = '&', $key = '')
		{
			$ret = array();

			foreach($data as $k => $v) {
				$k = urlencode($k);

				if(is_int($k) && $prefix != null) {
					$k = $prefix.$k;
				}

				if(!empty($key)) {
					$k = $key."[".$k."]";
				}

				if(is_array($v) || is_object($v)) {
					array_push($ret, http_build_query($v, '', $sep, $k));
				} else {
					array_push($ret, $k."=".urlencode($v));
				}
			}

			if(empty($sep)) {
				$sep = ini_get("arg_separator.output");
			}

			return implode($sep, $ret);
		}
	}
