<?php

    global $config;

    #init:
    require_once "init.php";

    session_start();
    Users::autoLogin();

    #extract vars from rewrited urls:
    $rewriter = new UrlRewriter($config);
    $rewriter->extractVars($config->getCurrentUrl());

    #smarty:
    $smarty = Application::getSmarty('/templates/', '/templates_c/');

    $smarty->template_dir = $config->absolute_path . '/templates/';
    $smarty->compile_dir = $config->absolute_path . '/templates_c/';

    #get the page::
    $page = empty($_GET['page']) ? 'home' : $_GET['page'];
    if(file_extension($page) == 'html') {
        $page = substr($page, 0, -5);
    }

    require_once "config/mapping.php";

    #map the page to another one:
    $mapped_controller = isset($mapping[$page]) ? $mapping[$page] : $page;

    $controller = $config->absolute_path . '/pages/' . $mapped_controller . '.php';
    $template = $config->absolute_path . '/templates/' . $mapped_controller . '.tpl';

//deg($controller, $template);

    if(file_exists($controller)) {

        require_once $controller;

        $content_var = $smarty->getTemplateVars('CONTENT');
        if(empty($content_var)) {
            $content_var = $mapped_controller;
        }

    } else if(file_exists($template)) {
        $content_var = $mapped_controller;
    } else {
        header("HTTP/1.1 404 Not Found");
        $content_var = 'error_not_found';
    }

    $content_var .= '.tpl';

    $smarty->assign('CONTENT', $content_var);

    #index:
    $index = $smarty->getGlobal('INDEX');
    if(empty($index)) {
        if(empty($_GET['indexul'])) {
            $index = 'index.tpl';
        } else {
            $index = $_GET['index'] . '.tpl';
        }
    }

    #general vars:
    $smarty->assign('config', $config);
    $smarty->assign('page_meta', $config->page_meta);
    $smarty->assign('page', $page);
    $smarty->assign('controller', $mapped_controller);

    if (Users::isLogged()) {
        $logged = Users::getLoggedUser();
        unset($logged->password);
        $smarty->assign('loggedUser', $logged);
    }

    if(Users::isFacebookLogged()){
        $facebookLogged = Users::getFacebookLoggedUser();
        $smarty->assign('facebookLogged', $facebookLogged);
    }

    if(Users::isInstagramLogged()){
        $instagramLogged = Users::getInstagramLoggedUser();
        $smarty->assign('instagramLogged', $instagramLogged);
    }

    if(Users::isTwitterLogged()){
        $twitterLogged = Users::getTwitterLoggedUser();
        $smarty->assign('twitterLogged', $twitterLogged);
    }
    #output:
    $smarty->display($index);