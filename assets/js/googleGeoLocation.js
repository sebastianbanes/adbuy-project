function initialize() {
    var options = {
        types: ['geocode']
    };
    var address = (document.getElementById('my-address'));
    var autocomplete = new google.maps.places.Autocomplete(address, options);
    autocomplete.setTypes(['geocode']);
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            return;
        }

        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }
    });
}
function codeAddress() {
    geocoder = new google.maps.Geocoder();
    var address = document.getElementById("my-address").value;
    geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            $type = results[0].address_components[0].types[0];
            if ($.inArray($type, ['country','locality']) !== -1){
                $locationToAdd      = results[0].formatted_address;
                $locationShortName  = results[0].address_components[0].short_name;
                $locationType       = results[0].address_components[0].types[0];
                adElement(results);
            } else {
                alert("You can only select a country, or a city");
            }
        }
        else {
            alert("Please select a location first!");
        }
    });
}

var list = document.getElementById('geoList');

function adElement(results){
    var locationToAdd       = results[0].formatted_address;
    var locationId          = results[0].address_components[0].short_name;
    var locationType        = results[0].address_components[0].types[0];

    var elementExists           = document.getElementById(locationId);
    var cityWithinElementExists = document.getElementById('cityIn' +  locationId);
    if(elementExists){
        alert("Location already added");
        return false;
    }

    if (locationType == 'country' && cityWithinElementExists){
        alert("You already have a city within this country");
        return false;
    }

    if (locationType == 'locality'){
        cityCountryNameToCheck = results[0].address_components[3].short_name;
        if (document.getElementById(cityCountryNameToCheck)){
            alert("You have already selected the whole country");
            return false;
        }
    }

    var entry   = document.createElement('li');
    entry.id    = locationId;
    entry.appendChild(document.createTextNode(locationToAdd));
    list.appendChild(entry);

    var locationField    = document.createElement('input');
    locationField.type   = "hidden";
    if (locationType == 'locality'){
        cityCountryName = results[0].address_components[3].short_name;
        locationField.name   = "locationCity[]";

        var locationFieldCityCountry    = document.createElement('input');
        locationFieldCityCountry.type   = "hidden";
        locationFieldCityCountry.id     = 'cityIn' + cityCountryName;
        entry.appendChild(locationFieldCityCountry);
    } else {
        locationField.name   = "location[]";
    }
    locationField.value  = locationId;
    entry.appendChild(locationField);


    var removeLink = document.createElement('a');
    removeLink.id = "remove" + locationId;
    linkText = document.createTextNode("Remove");
    removeLink.appendChild(linkText);
    removeLink.onclick = function(){removeElement(locationId)};
    entry.appendChild(removeLink);

    document.getElementById("my-address").value = "";

}

function removeElement(elementToRemove){
    document.getElementById(elementToRemove).remove();
}

$(document).ready( function() {
    google.maps.event.addDomListener(window, 'load', initialize);
});