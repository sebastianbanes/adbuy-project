var instagramLogin = false;
var faceBookLogin = false;

(function() {

    $(function() {
        if (isFacebookLogged){
            $('#btn-facebook').click(function() {
                logoutSocialMedia('facebook')
            });
        } else {
            $('#btn-facebook').click(loginWithFacebook);
        }
    });

    $(function() {
        if (isTwitterLogged){
            $('#btn-twitter').click(function() {
                logoutSocialMedia('twitter')
            });
        } else {
            $('#btn-twitter').click(loginWithTwitter);
        }
    });

    $(function() {
        if (isInstagramLogged){
            $('#btn-instagram').click(function() {
             logoutSocialMedia('instagram')
         });
        } else {
            $('#btn-instagram').click(loginWithInstagram);
        }
    })


    function logoutSocialMedia(socialMedia){
        $.ajax({
            data: {
                page: 'logout-media',
                socialMedia: socialMedia
            },
            type: 'post',
            success: function(d) {
                if (d.code == 200) {
                    $('.' + socialMedia + '-connect').removeClass('active');

                    switch(socialMedia) {
                        case 'instagram':
                        isInstagramLogged = false;
                        $('#btn-instagram').unbind("click");
                        $('#btn-instagram').click(loginWithInstagram);
                        isLogged =  false;
                        if(isFacebookLogged && !isInstagramLogged) {
                            console.log('A ramas logat pe facebook');
                        } else {
                            $('#userPageList').empty();
                        }
                        break;
                        case 'facebook':
                        isFacebookLogged = false;
                        $('#btn-facebook').unbind("click");
                        $('#btn-facebook').click(loginWithFacebook);
                        isLogged =  false;
                        if(!isFacebookLogged && isInstagramLogged) {
                            console.log('A ramas logat pe instagram');
                        } else {
                            $('#userPageList').empty();
                        }
                        break;
                        case 'twitter':
                        isTwitterLogged = false;
                        $('#btn-twitter').unbind("click");
                        $('#btn-twitter').click(loginWithFacebook);
                        break;
                    }

                    if (!isInstagramLogged && !isFacebookLogged && !isTwitterLogged){
                        isLogged = false;
                        $('.right-side input').attr('disabled', 'disabled');
                        $('.right-side select').attr('disabled', 'disabled');
                        $('.right-side label').addClass('is_disabled');
                        $('.right-side h3').addClass('is_disabled');
                        $('#submitButton').attr('disabled', 'disabled');
                    }

                } else {
                    console.log(d.data);
                }
            }
        });
    }

    function loginWithFacebook(e){
        faceBookLogin = true;
        loginWithFacebookOrInstagram(e);
    }

    function loginWithInstagram(e){
        instagramLogin = true;
        loginWithFacebookOrInstagram(e);
    }

    function loginWithFacebookOrInstagram(e) {

        e.preventDefault();

        FB.login(function(response) {

            if (response.status === 'connected') {
                $.ajax({
                    data: {
                        page: 'login-fb',
                        accessToken: response.authResponse.accessToken,
                        expiresIn: response.authResponse.expiresIn,
                        doFaceBookLogin: faceBookLogin ? 1 : 0,
                        doInstagramLogin: instagramLogin ? 1 : 0
                    },
                    type: 'post',
                    success: function(d) {

                        if (d.code == 200) {
                            // location.href = d.data;
                            
                            if(d.data == 'loggedIn') {
                                console.log('erai deja logat');
                            } else {
                                populateUserPAgesDropDown(d.data);
                                $('.right-side input').removeAttr('disabled');
                                $('.right-side select').removeAttr('disabled');
                                $('.btn-place-order').removeAttr('disabled');
                                $('.right-side label').removeClass('is_disabled');
                                $('.right-side h3').removeClass('is_disabled');
                            }


                            if (faceBookLogin){
                                $('.facebook-connect').addClass('active');
                                isFacebookLogged = true;
                                faceBookLogin = false;

                                $('#btn-facebook').unbind("click");
                                $('#btn-facebook').click(function() {
                                    logoutSocialMedia('facebook');
                                });
                            }

                            if (instagramLogin){
                                $('.instagram-connect').addClass('active');
                                isInstagramLogged = true;
                                instagramLogin = false;
                                $('#btn-instagram').unbind("click");
                                $('#btn-instagram').click(function() {
                                    logoutSocialMedia('instagram');
                                });
                            }

                        } else {
                            console.log(d.data);
                        }
                    }
                });
            }

        }, {scope: 'public_profile,email,manage_pages,publish_pages,ads_management'});
    }

    window.fbAsyncInit = function() {
        FB.init({
            appId      : __FB_APP_ID,
            cookie     : true,  // enable cookies to allow the server to access
                                // the session
            version    : 'v2.5' // use version 2.5
        });
    };


    function loginWithTwitter(e) {

        $.ajax({
            data: {
                page: 'login-twitter'
            },
            type: 'post',
            success: function(d) {
                if (d.code == 200) {
                	console.log(d.data);
                	window.open(d.data, "Twitter", "width=725,height=685,resizable=no,toolbar=no,menubar=no,location=no,status=no");
                } else {
                    console.log(d.data);
                }
            }
        });
    }

})();