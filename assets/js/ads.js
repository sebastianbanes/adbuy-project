$(document).ready( function() {

    var form = $('#facebookCreateAd');

    form.submit(function(e){

        e.preventDefault();

        var data = new FormData(this),
        inputs = form.find('input'),
        submit = form.find('button[type=submit]');
        selects = form.find('select');

        inputs.prop('disabled', true);
        selects.prop('disabled', true);
        submit.button('loading');

        $.ajax({
            type: 'post',
            data: data,
            cache: false,
            contentType: false,
            processData: false,

            success: function(d) {
                if (d.code == 200) {

                    location.href = '/paypal-payment.html?hashId=' + d.data;
                } else {
                    submit.button('reset');
                    inputs.prop('disabled', false);
                    selects.prop('disabled', false);

                    alert(d.data);
                }
            },

            complete: function() {
            }
        });
    });

    $('#buyDetailsCampaignBudget').keyup(calculateCupewFee);
    $('#buyDetailsDateRange').change(validateAndCalculateCPM);
});


function calculateCupewFee(){
    $('#buyDetailsOurFee').html('$' + ($('#buyDetailsCampaignBudget').val() * 0.5 / 100));
}


function validateAndCalculateCPM(){

    if ($('#buyDetailsCampaignBudget').val() > 0){
        $.ajax({
            type: 'post',
            data: {
                page: 'calculate-cpm',
                budget: $('#buyDetailsCampaignBudget').val(),
                timeSpan:  $('#buyDetailsDateRange').val()
            },
            success: function(d) {
                if (d.code == 200) {
                    calculateCupewFee();

                    var cpmList =  d.data;
                    var parsedJson = JSON.parse(cpmList);
                    var arr = [];

                    for(var x in parsedJson){
                        arr.push(parsedJson[x]);
                    }

                    $('.cpmSelect').empty();
                    
                    $.each(arr, function(index, value) {
                        var str = value;

                        var getCPMvalue = str.replace('$','');

                        $('.cpmSelect').append('<option value="' + getCPMvalue + '">' + value + '</option>');
                    });


                } else {
                    alert(d.data);
                }
            }
        });
    }
}

function getCupewFee(){

}

