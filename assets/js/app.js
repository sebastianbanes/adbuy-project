$(document).ready( function() {
	$.ajaxSetup({
		url: "/ajax/",
		global: false,
		type: "get",
		dataType: "json"
	});

    // Date pickers

     var dateNow = new Date();
     var tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);

    $('#start_date').datetimepicker({
    	format: 'MM/DD/YYYY',
    	defaultDate: dateNow
    });
    $('#end_date').datetimepicker({
    	format: 'MM/DD/YYYY',
    	defaultDate: tomorrow
    });

    // Facebook data generator
    function generateAgeRangeSelector(generalId, elementId , minAge, maxAge, optionText) {

    	var myDiv = document.getElementById(generalId);

	    var lowEnd = minAge; // Minimum age range
	    var highEnd = maxAge; // Maximum age range
	    var arr = [];
	    while(lowEnd <= highEnd){
	    	arr.push(lowEnd++);
	    }

	    //Create and append select list
	    var selectList = document.createElement("select");
	    selectList.className = 'form-control ageSelector';
	    selectList.id = elementId;
	    selectList.name = elementId;
	    selectList.setAttribute("required", "");

	    myDiv.appendChild(selectList);
	    var thisElement = document.getElementById(elementId);
	    $(thisElement).append('<option value="" selected disabled>'+ optionText +'</option>');

	    for (var i = 0; i < arr.length; i++) {
	    	var option = document.createElement("option");
	    	option.value = arr[i];
	    	option.text = arr[i];
	    	selectList.appendChild(option);
	    }


	}

	generateAgeRangeSelector('ageRange', 'ageMin', 13, 65, 'Minimum...');
	generateAgeRangeSelector('ageRange', 'ageMax', 13, 65, 'Maximum...');

	// Drag and drop image upload


$("input[name='form-ad-photo']").on('drop', function(e){

	var myImage = e.originalEvent.dataTransfer.files;

	var fReader = new FileReader();

	fReader.readAsDataURL(myImage[0]);

	fReader.onloadend =  function(event) {
		imgSrc = event.target.result;

		$('#image_upload_preview').attr('src', imgSrc);

		var elHeight = $('.preview_image').height();

		$("input[name='form-ad-photo']").height(elHeight + 85);
	}

});

$('#form-ad-photo').change(function(event){
	var files = event.target.files;

	var reader = new FileReader();

	reader.readAsDataURL(files[0]);

	reader.onloadend = function(event) {
		myImgUrl = event.target.result;

		$('#image_upload_preview').attr('src', myImgUrl);

		var elHeight = $('.preview_image').height();

		$("input[name='form-ad-photo']").height(elHeight + 85);
	}
});

	// Enable / disable buy details on ad create form

	var isLogged = false;

	if(isFacebookLogged){
		$('.facebook-connect').addClass('active');
		isLogged = true;
	}
	if(isInstagramLogged){
		$('.instagram-connect').addClass('active');
		isLogged = true;
	}
	if(isTwitterLogged){
		$('.twitter-connect').addClass('active');
	}

	if (!isFacebookLogged && !isInstagramLogged && !isTwitterLogged){
		$('.right-side input').attr('disabled', 'disabled');
		$('.right-side select').attr('disabled', 'disabled');
		$('.right-side label').addClass('is_disabled');
		$('.right-side h3').addClass('is_disabled');
		$('#submitButton').attr('disabled', 'disabled');
		$('#getCords').attr('disabled', 'disabled');
	}

	if (isLogged){
		$.ajax({
			data: {
				page: 'get-pages'
			},
			type: 'post',
			success: function(d) {
				if (d.code == 200) {
                        // location.href = d.data;
                        populateUserPAgesDropDown(d.data);
                    } else {
                    	alert(d.data);
                    }
                }
            });
	}

});


function populateUserPAgesDropDown(pagesJQuery){
	$('#userPageList').append('<option value="" selected disabled>Select a page...</option>');
	$.each(pagesJQuery, function(i, option) {
		// $('#userPageList').append($('<option/>').attr("value", option.id).text(option.name));
		$('#userPageList').append('<option value="' + option.id + '">' + option.name + '</option>');
	});
}