<?php

	$config->debug_mode = true;

    $config->mysql['default'] = new stdClass();
    $config->mysql['default']->host = 'localhost';
    $config->mysql['default']->database = 'adbuy';
    $config->mysql['default']->user = 'root';
    $config->mysql['default']->password = '';

    $config->facebook = new stdClass();
    $config->facebook->appId = '';
    $config->facebook->secret = '';
    $config->facebook->graphVersion = 'v2.5';
