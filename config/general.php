<?php

	#operating modes:
	$config->maintenance_mode = false;
	$config->debug_mode = true;

	#default timezone:
	$config->default_timezone = 'Europe/Bucharest';

	#auto include directories:
	$config->auto_include_directories = array(
			'include/lib/classes/base/', 
			'include/lib/classes/',
            'include/lib/siteclasses/',
            'include/classes/',
            'include/'
	);

	#error logging:
	$config->error_log = '/files/logs/errors.txt';
	$config->error_levels = E_ALL ^ E_STRICT;

	#default meta tags:
	$config->page_meta = new stdClass();
	$config->page_meta->title = 'Cupew Consulting improve marketing';
	$config->page_meta->description = 'Cupew Consulting improve marketing';
	$config->page_meta->keywords = '';
	$config->page_meta->language = 'EN';

    #files settings
    $config->image_file = new stdClass();
    $config->image_file->max_file_size = 5;


	#controllers:
	$config->url_page_var = 'page';
	$config->url_action_var = 'action';
	$config->default_page = 'home';
	$config->default_action = 'index';

	#languages:
	$config->languages = array('en');
	//$config->default_language = 'en';
	$config->language_var = 'lang';

    #ads
    $config->ad_Settings = new stdClass();
    $config->ad_Settings->min_budget_value = 100;

	#mailer:
	$config->mail = new stdClass();

    $config->mail->payment = 'ovidiu.maritan-facilitator-1@qubiz.com';
	$config->mail->admin = 'contact@qubiz.com';
	$config->mail->host = 'localhost';
	$config->mail->from = 'contact@qubiz.com';
	$config->mail->from_name = @$config->page_meta->title;
	$config->mail->charset = 'iso-8859-1';
	$config->mail->content_type = 'text/html';
	$config->mail->encoding = '8bit';
	$config->mail->line_ending = "\n";
    $config->mail->mesajContact = 'qubiz.com';

	#admin auth:
	$config->admin_auth = new stdClass();
	$config->admin_auth->table = 'admin';
	$config->admin_auth->login_attempts_table = 'admin_login';
	$config->admin_auth->track_login_attempts = true;
	$config->admin_auth->save_last_login = true;
	$config->admin_auth->last_login_field = 'last_login';
	$config->admin_auth->username_field = 'email';
	$config->admin_auth->password_field = 'password';
	$config->admin_auth->session_var = 'user_admin';

    #user auth:
    $config->user_auth = new stdClass();
    $config->user_auth->table = 'users';
    $config->user_auth->username_field = 'email';
    $config->user_auth->password_field = 'password';
    $config->user_auth->session_var = 'user_front';
    $config->user_auth->facebook_session_var = 'facebook_user_front';
    $config->user_auth->instagram_session_var = 'instagram_user_front';
    $config->user_auth->twitter_session_var = 'twitter_user_front';
    $config->user_auth->last_login_field = 'last_login';
    $config->user_auth->track_login_attempts = true;
    $config->user_auth->save_last_login = true;

    define('ADDR_CONTACT', 'contact@qubiz.com');

	// custom settings
	$config->version = '1.0.0';
